<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInscribesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('inscribes', 'unity')) {

        }
        Schema::table('inscribes', function (Blueprint $table) {
            $table->string('unity')->nullable();
            $table->string('course')->nullable();
            $table->string('shift')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inscribes', function (Blueprint $table) {
            //
        });
    }
}
