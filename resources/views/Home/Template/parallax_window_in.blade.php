<section class="parallax_window_in" data-parallax="scroll" data-image-src="{{ $cms_page->session_1_img_background!=null && $cms_page->session_1_img_background!='default' ? asset("HomeP/img/CmsPages/$cms_page->session_1_img_background") : asset('HomeP/img/sub_header_about.jpg') }}" data-natural-width="1400" data-natural-height="800">
    <div id="sub_content_in">
        <h1>{{ $cms_page->session_1_title ? $cms_page->session_1_title : 'Sem Título' }}</h1>
        <p>"{{ $cms_page->session_1_subtitle ? $cms_page->session_1_subtitle : 'Sem Sub-Título' }}"</p>
    </div>
</section>
<!-- /section -->
