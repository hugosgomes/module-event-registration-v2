<!--Tag id="wrapped" retirada -->
<div id="middle-wizard">
    <div class="step">
        <h3 class="main_question">Consulta Para Evento {{ $model['name'] }}</h3>


        @if ($model['type']['name'] == 'Vestibular' || $model['fixed_subscribers'] ==1)
            <p>Digite seu CPF para consultar inscrição!</p>
        @else
            <p>Digite seu CPF ou Matrícula para consultar inscrição!</p>
        @endif

        <div class="col-12">
            <div class="form-group">
                <input type="text" id="search_cpf" name="cpf" value="" class="col-12 form-control cpf" placeholder="Digite o CPF...">
            </div>
        </div>

        @if ($model['type']['name'] != 'Vestibular' && $model['fixed_subscribers'] !=1)
            <div class="col-12">
                <div class="form-group">
                    <input type="text" id="search_number_registration" name="number_registration" value="" class="col-12 form-control" placeholder="Digite a Matrícula...">
                </div>
            </div>
        @endif
    </div>
    <div class="col-12">
        <button type="button" id="search_2" class="forward_error">Consultar</button>
        <div id="loading" style="margin-bottom: 10px; display:none">
            <span style="color: darkblue;"><img src="{{ asset('HomeP/img/loading.gif') }}" style="width: 30px;height: 30px;margin-bottom: 10px;"> | Verificando inscrição existente...</span>
        </div>
    </div>
    <hr>
</div>

<a id="search_result" href="#search_result" style="display:none">Link</a>
<p class="infor_person" style="font-size: large;"></p>
<p class="inscribe-data" style="font-size: large; display:none">
    Dados da Inscrição: <br>
    Nome Completo: <span class="full-name"></span>
</p>
<P class="unity-course" style="font-size: large; display:none">
    Campus: <span class="unity"></span><br>
    Curso: <span class="course"></span><br>
</P>
<p class="infor_status" style="font-size: large;"></p>
<p class="infor_inscribe" style="font-size: large;"></p>
<p name="search_result" class="infor_payment"></p>

<div class="infor-vestibular" style="font-size: large; display:none">
    @if ($model['type']['name'] == 'Vestibular')
        <p>O Vestibular 2020.1 ocorrerá no dia 30/11/2019, clique <a href="https://unig.br/conheca-os-campi-nova-iguacu-e-itaperuna/" target="_blank">aqui</a> para conhecer o seu campus </p>

        <p>
            Informações úteis:<br>
            + Acesse <a href="https://unig.br/vestibular/" target="_blank">https://unig.br/vestibular/</a> para informações gerais e Edital e Manual do Aluno<br>
            + Caso seu boleto esteja vencido clique no botão abaixo para emitir um novo boleto.<br>
            + Qualquer dúvida adicional, entre em contato através de nosso chat de atendimento.<br>
        </p>
    @endif
</div>

<form action="{{route('Home.PagSeguro.boleto')}}" method="post" class="form-boleto">
    {{ csrf_field() }}
    <input type="hidden" name="price" value="{{$model['type']['name'] == 'Vestibular' ? '30.00' : ''}}">
    <input type="hidden" name="unity">
    <input type="hidden" name="course">
    <input type="hidden" name="cell_phone">
    <input type="hidden" name="cpf">
    <input type="hidden" name="event_id">
    <input type="hidden" name="event_name">
    <input type="hidden" name="inscribe_id">
    <input type="hidden" name="created_at">
    <input type="hidden" name="first_name">
    <input type="hidden" name="last_name">
    <input type="hidden" name="email">
    <input type="hidden" name="street">
    <input type="hidden" name="number">
    <input type="hidden" name="district">
    <input type="hidden" name="cep">
    <input type="hidden" name="city">
    <input type="hidden" name="state">
    <input type="hidden" name="hashInput" id="hashInput">
    <p class="infor_boleto"></p>
</form>

<div class="infor-certified" style="display:none">
    <button type="button" id="btn-certified" class="forward_error">Gerar Certificado</button>
</div>
