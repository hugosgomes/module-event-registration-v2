@extends("$route[0].TemplateShow.index")

@section('content')

    <div class="container-fluid full-height">
        <div class="row row-height">
            <div class="col-lg-6 content-left">
                <div class="content-left-wrapper">
                    <a href="{{ route("Home.Main.index") }}" id="logo"><img src="{{ $cms_page->logo2!=null && $cms_page->logo2!='default' ? asset("HomeP/img/CmsPages/$cms_page->logo2") : asset("HomeP/img/CmsPages/logo2.png") }}" alt="" width="35" height="35"></a>
                    <div id="social">
                        <ul>
                            @if($cms_page->facebook!=null)
                                <li><a href="{{ $cms_page->facebook }}"><i class="icon-facebook"></i></a></li>
                            @endif
                            @if($cms_page->twitter!=null)
                                <li><a href="{{ $cms_page->twitter }}"><i class="icon-twitter"></i></a></li>
                            @endif
                            @if($cms_page->google!=null)
                                <li><a href="{{ $cms_page->google }}"><i class="icon-google"></i></a></li>
                            @endif
                            @if($cms_page->linkedin!=null)
                                <li><a href="{{ $cms_page->linkedin }}"><i class="icon-linkedin"></i></a></li>
                            @endif

                            @if($cms_page->youtube!=null)
                                <li><a href="{{ $cms_page->youtube }}"><i class="icon-youtube"></i></a></li>
                            @endif
                            @if($cms_page->instagram!=null)
                                <li><a href="{{ $cms_page->instagram }}"><i class="icon-instagram"></i></a></li>
                            @endif
                        </ul>
                    </div>
                    <!-- /social -->
                    <div>
                        <figure><img src="{{ asset('HomeP/img/errors/error3.png') }}" alt="" class="img-fluid" width="200" height="200"></figure>
                        <h2>Houve um erro!</h2>
                        <p>{{ $msgError }}!</p>
                        <a href="{{ route("$route[0].$route[1].$route[2]") }}" class="btn_1 rounded">Atualizar Página</a>
                        <a href="{{ route("$route[0].$route[1].$route[2]") }}" class="btn_1 rounded mobile_btn">Atualizar Página</a>
                    </div>
                    <div class="copy">© {{ date('Y') }} Unig Digital</div>
                </div>
                <!-- /content-left-wrapper -->
            </div>
            <!-- /content-left -->

            <div class="col-lg-6 content-right" id="start">
                <div id="wizard_container">
                    <p class="alert alert-danger">O sistema se encontra com problemas, você pode clicar no botão ATUALIZAR A PÁGINA, para tentar resolver o problema, ou informar o problema no e-mail: ( )</p>
                </div>
                <!-- /Wizard container -->
            </div>
            <!-- /content-right-->
        </div>
        <!-- /row-->
    </div>
    <!-- /container-fluid -->

@endsection
