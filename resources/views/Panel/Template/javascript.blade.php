<script src="{{ asset('PanelP/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('PanelP/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<script src="{{ asset('PanelP/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('PanelP/plugins/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('PanelP/plugins/sparklines/sparkline.js') }}"></script>
<script src="{{ asset('PanelP/plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('PanelP/plugins/jqvmap/maps/jquery.vmap.world.js') }}"></script>
<script src="{{ asset('PanelP/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<script src="{{ asset('PanelP/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('PanelP/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('PanelP/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<script src="{{ asset('PanelP/plugins/summernote/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('PanelP/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<script src="{{ asset('PanelP/plugins/fastclick/fastclick.js') }}"></script>
<script src="{{ asset('PanelP/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('PanelP/plugins/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('PanelP/dist/js/adminlte.js') }}"></script>
<script src="{{ asset('PanelP/dist/js/pages/dashboard.js') }}"></script>
<!--<script src="{{ asset('PanelP/dist/js/demo.js') }}"></script>-->
<script type="text/javascript">
    $(function() {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        $('.swalDefaultSuccess').click(function() {
            Toast.fire({
                type: 'success',
                title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
            })
        });
        $('.swalDefaultInfo').click(function() {
            Toast.fire({
                type: 'info',
                title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
            })
        });
        $('.swalDefaultError').click(function() {
            Toast.fire({
                type: 'error',
                title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
            })
        });
        $('.swalDefaultWarning').click(function() {
            Toast.fire({
                type: 'warning',
                title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
            })
        });
        $('.swalDefaultQuestion').click(function() {
            Toast.fire({
                type: 'question',
                title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
            })
        });

        $('.toastrDefaultSuccess').click(function() {
            toastr.success('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
        });
        $('.toastrDefaultInfo').click(function() {
            toastr.info('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
        });
        $('.toastrDefaultError').click(function() {
            toastr.error('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
        });
        $('.toastrDefaultWarning').click(function() {
            toastr.warning('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
        });

        $(document).ready(function () {
            @if(session('error'))
                toastr.error("{{ session('error') }}");
            @endif
            @if(session('success'))
                toastr.success("{{ session('success') }}");
            @endif
            @if(session('info'))
                toastr.info("{{ session('info') }}");
            @endif
            @if(session('warning'))
                toastr.warning("{{ session('warning') }}");
            @endif
        });
    });

</script>
<!-- ================== PAGE LEVEL SCRIPTS ==================-->
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
<script>
    //$('textarea').ckeditor();
    //$('.textarea').ckeditor(); // if class is prefered.
    //$('#textarea').ckeditor(); // if id is prefered.
</script>
<script src="{{ asset('PanelP/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>
<script src="{{ asset('PanelP/plugins/select2/js/select2.full.min.js') }}"></script>
<script>//Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox();
    //Initialize Select2 Elements
    $('.select2').select2();
</script>

<script>

    $('.click_dependence').click(function () {
        if(this.href.split('#')[1]==='campus')
        {
            toastr.error("Para criar campus você precisa criar Unidade primeiro!");
        }
        if(this.href.split('#')[1]==='departments')
        {
            toastr.error("Para criar departamentos você precisa criar campus e centro de custo primeiro!");
        }
        if(this.href.split('#')[1]==='groups')
        {
            toastr.error("Para criar grupos você precisa criar campus primeiro!");
        }
        if(this.href.split('#')[1]==='tickets')
        {
            toastr.error("Para criar tickets você precisa criar departamentos primeiro!");
        }
        if(this.href.split('#')[1]==='trainingsClasses')
        {
            toastr.error("Para criar Aulas você precisa criar Treinamentos primeiro!");
        }
        if(this.href.split('#')[1]==='shifts')
        {
            toastr.error("Para criar turnos você precisa criar Cursos primeiro!");
        }
        if(this.href.split('#')[1]==='courses')
        {
            toastr.error("Para criar Cursos você precisa criar Tipos de Cursos, Turnos e Categorias de Cursos primeiro!");
        }
        if(this.href.split('#')[1]==='events')
        {
            toastr.error("Para criar Eventos você precisa criar Tipos de Eventos e Cursos primeiro!");
        }


        //Permissão não aceita
        if(this.href.split('#')[1]==='institutions_ban')
        {
            toastr.error("Você não tem permissão para nesta área!");
        }
        if(this.href.split('#')[1]==='units_ban')
        {
            toastr.error("Você não tem permissão para nesta área!");
        }
        if(this.href.split('#')[1]==='campus_ban')
        {
            toastr.error("Você não tem permissão para nesta área!");
        }
        if(this.href.split('#')[1]==='cost_center_ban')
        {
            toastr.error("Você não tem permissão para nesta área!");
        }
        if(this.href.split('#')[1]==='departments_ban')
        {
            toastr.error("Você não tem permissão para nesta área!");
        }
        if(this.href.split('#')[1]==='groups_ban')
        {
            toastr.error("Você não tem permissão para nesta área!");
        }
        if(this.href.split('#')[1]==='tickets_ban')
        {
            toastr.error("Você não tem permissão para nesta área!");
        }
        if(this.href.split('#')[1]==='cdis_ban')
        {
            toastr.error("Você não tem permissão para nesta área!");
        }
        if(this.href.split('#')[1]==='trainingsCourses_ban')
        {
            toastr.error("Você não tem permissão para nesta área!");
        }
    });

</script>


@includeIf("$route[0].$route[1].Organizar.javascriptLocal")
