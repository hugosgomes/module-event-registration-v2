<!--Tag id="wrapped" retirada -->
<form>
    <div id="middle-wizard">
        <div class="step">
            <h3 class="main_question">Boleto : {{ $model['name'] ? $model['name'] : 'Sem título' }}</h3>
            <a href="{{ session('boleto')!='error' ? session('boleto')->link_boleto : route('Home.Main.search', ['id' => $model['id'], 'tag' => $model['tag']]) }}"><img src="{{ asset('HomeP/img/boleto.png') }}" class="img-fluid"></a>
            <p>
                <strong>
                    @if (session('boleto')!='error')
                        <a href="{{ session('boleto')->link_boleto }}" id="forward_boleto" class="forward_boleto" target="_blank">IMPRIMIR BOLETO <i class="fa fa-check"></i></a>
                    @else
                    Emissão de Boleto indisponível no momento! Tente novamente em alguns minutos ou entre em contato pelo chat. COD: Erro Pagseguro
                    @endif
                </strong>
            </p>
        </div>
    </div>
</form>
