<?php

namespace App\Models\Configuration;

use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Image;

class Configurations
{
    protected $request;
    protected $router;

    public function __construct(Request $request, Router $router)
    {
        $this->request = $request;
        $this->router = $router;
    }

    public function UpdatePermissionsSystem()
    {
        $permissions =
            [
                //CmsPages-------------------
                [
                    'ambient'         =>'Panel',
                    'name'            =>'CmsPages',
                    'group'           =>'CMS das Páginas',
                ],
            ];
        //----------------------------------------------------
        foreach ($permissions as $p)
        {
            $name = $p['name'];

            $validationUniqueAll = Permissions::where('name', "Panel.$name.all")->get()->first();
            if($validationUniqueAll==null)
            {
                $permissionAll = Permissions::create([
                    'name'                 =>$p['ambient'].'.'.$p['name'].'.all',
                    'label'                =>"Pode ver a lista de todos ".$p['name'].' do sistema',
                    'group'                =>$p['group'],
                    'system'               =>1,
                    'create_user_id'       =>1
                ]);
            }
            $validationUnique1 = Permissions::where('name', "Panel.$name.index")->get()->first();
            if($validationUnique1==null)
            {
                $permissionShow1 = Permissions::create([
                    'name'                 =>$p['ambient'].'.'.$p['name'].'.index',
                    'label'                =>"Pode Acessar ".$p['name'],
                    'group'                =>$p['group'],
                    'system'               =>1,
                    'create_user_id'       =>1
                ]);
            }
            $validationUnique2 = Permissions::where('name', "Panel.$name.create")->get()->first();
            if($validationUnique2==null)
            {
                $permissionShow2 = Permissions::create([
                    'name'                 => $p['ambient'].'.'.$p['name'].'.create',
                    'label'                => "Pode Cadastrar " . $p['name'],
                    'group'                => $p['group'],
                    'system'               =>1,
                    'create_user_id'       =>1
                ]);
            }
            $validationUnique3 = Permissions::where('name', "Panel.$name.edit")->get()->first();
            if($validationUnique3==null)
            {
                $permissionShow3 = Permissions::create([
                    'name'                 =>$p['ambient'].'.'.$p['name'].'.edit',
                    'label'                =>"Pode Editar ".$p['name'],
                    'group'                =>$p['group'],
                    'system'               =>1,
                    'create_user_id'       =>1
                ]);
            }
            $validationUnique4 = Permissions::where('name', "Panel.$name.infor")->get()->first();
            if($validationUnique4==null)
            {
                $permissionShow4 = Permissions::create([
                    'name'                 =>$p['ambient'].'.'.$p['name'].'.infor',
                    'label'                =>"Pode Acessar ".$p['name'],
                    'group'                =>$p['group'],
                    'system'               =>1,
                    'create_user_id'       =>1
                ]);
            }
            $validationUnique5 = Permissions::where('name', "Panel.$name.show")->get()->first();
            if($validationUnique5==null)
            {
                $permissionShow5 = Permissions::create([
                    'name'                 =>$p['ambient'].'.'.$p['name'].'.show',
                    'label'                =>"Pode Acessar ".$p['name'],
                    'group'                =>$p['group'],
                    'system'               =>1,
                    'create_user_id'       =>1
                ]);
            }
            $validationUnique6 = Permissions::where('name', "Panel.$name.store")->get()->first();
            if($validationUnique6==null)
            {
                $permissionShow6 = Permissions::create([
                    'name'                 =>$p['ambient'].'.'.$p['name'].'.store',
                    'label'                =>"Pode Cadastrar ".$p['name'],
                    'group'                =>$p['group'],
                    'system'               =>1,
                    'create_user_id'       =>1
                ]);
            }
            $validationUnique7 = Permissions::where('name', "Panel.$name.update")->get()->first();
            if($validationUnique7==null)
            {
                $permissionShow7 = Permissions::create([
                    'name'                 =>$p['ambient'].'.'.$p['name'].'.update',
                    'label'                =>"Pode Editar ".$p['name'],
                    'group'                =>$p['group'],
                    'system'               =>1,
                    'create_user_id'       =>1
                ]);
            }
            $validationUnique8 = Permissions::where('name', "Panel.$name.delete")->get()->first();
            if($validationUnique8==null)
            {
                $permissionShow8 = Permissions::create([
                    'name'                 =>$p['ambient'].'.'.$p['name'].'.delete',
                    'label'                =>"Pode Deletar ".$p['name'],
                    'group'                =>$p['group'],
                    'system'               =>1,
                    'create_user_id'       =>1
                ]);
            }
            $validationUnique9 = Permissions::where('name', "Panel.$name.trashed")->get()->first();
            if($validationUnique9==null)
            {
                $permissionShow9 = Permissions::create([
                    'name'                 =>$p['ambient'].'.'.$p['name'].'.trashed',
                    'label'                   =>"Pode ver lista de deletados de ".$p['name'],
                    'group'                =>$p['group'],
                    'system'               =>1,
                    'create_user_id'       =>1
                ]);
            }
            $validationUnique10 = Permissions::where('name', "Panel.$name.restore")->get()->first();
            if($validationUnique10==null)
            {
                $permissionShow10 = Permissions::create([
                    'name'                 =>$p['ambient'].'.'.$p['name'].'.restore',
                    'label'                   =>"Pode restaurar deletados de ".$p['name'],
                    'group'                =>$p['group'],
                    'system'               =>1,
                    'create_user_id'       =>1
                ]);
            }
        }
        //----------------------------------------------------
        $listRoutes = $this->routeList();
        foreach ($listRoutes as $item => $route)
        {
            if
            (
                $route!='login'                                 &&
                $route!='logout'                                &&
                $route!='register'                              &&
                $route!='password.request'                      &&
                $route!='password.email'                        &&
                $route!='password.reset'                        &&
                $route!='password.update'                       &&
                $route!='Api.Cdi.list'
            )
            {
                $verExist = Permissions::where('name', $route)->get()->first();
                if($verExist==null && env('APP_DEBUG')==true)
                {
                    dd('Inclua a Permissão para rota : ['.$route.'], no Model de Configurations');
                }
            }
        }
    }

    public function routeList()
    {
        return array_keys($this->router->getRoutes()->getRoutesByName());
    }

    public function rotaFull()
    {
        return explode('/', $this->request->getUri());
    }

    public function ipClient()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    public function getRouteName(Request $request)
    {
        if($request->route()!=null)
        {
            return explode('.', $request->route()->getName());
        }
        else
        {
            return 'Home.Main.index';
        }
    }

    public function allRoutes()
    {

    }

    public function consumeApi(Request $request, $url)
    {

    }

    public function Tagear($item)
    {
        //Transformando o titulo em tag
        $urlP = $item;
        $table = array(
            'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z',
            'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
            'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
            'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
            'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
            'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
            'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
            'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
            'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
            'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
            'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
            'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
            'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
            'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r',
        );
        // Traduz os caracteres em $string, baseado no vetor $table
        $urlP = strtr($urlP, $table);
        // converte para minúsculo
        $urlP = strtolower($urlP);
        // remove caracteres indesejáveis (que não estão no padrão)
        $urlP = preg_replace("/[^a-z0-9_\s-]/", "", $urlP);
        // Remove múltiplas ocorrências de hífens ou espaços
        $urlP = preg_replace("/[\s-]+/", " ", $urlP);
        // Transforma espaços e underscores em hífens
        $urlP = preg_replace("/[\s_]/", "-", $urlP);
        //Transformando o titulo em tag
        return $urlP;
    }

    public function SalvarImagem($request, $inputName, $caminho, $largura, $altura)
    {
        if($request->hasFile($inputName)){
            $arquivo = $request->file($inputName);
            $filename = time() . '-' . rand() . '.' . $arquivo->getClientOriginalExtension();
            if($arquivo->getClientOriginalExtension() != 'jpg' && $arquivo->getClientOriginalExtension() != 'jpeg' && $arquivo->getClientOriginalExtension() != 'png' && $arquivo->getClientOriginalExtension() != 'PNG' && $arquivo->getClientOriginalExtension() != 'JPG'){
                return false;
            }else{
                Image::make($arquivo)->resize($largura, $altura)->save($caminho.''.$filename);
                return $filename;
            }
        }
        else{
            return false;
        }
    }

    public function EditarImagem($request, $inputName, $inputNamebanco, $caminho, $largura, $altura)
    {
        if($request->hasFile($inputName)){
            $arquivo = $request->file($inputName);
            $filename = time() . '-' . rand() . '.' . $arquivo->getClientOriginalExtension();
            if($arquivo->getClientOriginalExtension() != 'jpg' && $arquivo->getClientOriginalExtension() != 'jpeg' && $arquivo->getClientOriginalExtension() != 'png' && $arquivo->getClientOriginalExtension() != 'PNG' && $arquivo->getClientOriginalExtension() != 'JPG'){
                return $request->$inputNamebanco;
            }else{
                //Excluindo arquivo de imagem se ele existir
                if(file_exists($caminho.''.$request->$inputNamebanco) && $request->$inputNamebanco!='')
                {
                    unlink($caminho.''.$request->$inputNamebanco."");
                }
                Image::make($arquivo)->resize($largura, $altura)->save($caminho.''.$filename);
                return $filename;
            }
        }
        else{
            return $request->$inputNamebanco;
        }
    }

    public function ExcluirImagem($nome, $caminho)
    {
        //Excluindo arquivo de imagem se ele existir
        if(file_exists($caminho.''.$nome) && $nome!='')
        {
            unlink($caminho.''.$nome."");
        }
        return true;
    }

    public function createError($e)
    {
        $user = auth()->user();
        Errors::create(['user_id'=>$user ? $user->id : 1, 'title'=>'Arquivo que gerou o erro:'.$e->getFile().' Linha do Erro: '.$e->getLine(), 'code'=>$e->getCode(), 'content'=>$e->getMessage(), 'status'=>0]);
    }

    public function CrudSystemNotModified($request, $model)
    {
        if(isset($request['id']) && isset($model))
        {
            if($model->find($request['id'])->system==1 && auth()->user()->roles[0]->name!='Administrator')
            {
                return true;
            }
            return false;
        }
        else
        {
            return false;
        }
    }

    public function CreateUniqueFile($request, $path, $inputName)
    {
        $nameFile = null;
        if ($request->hasFile($inputName) && $request->file($inputName)->isValid()) {

            $size = $request->$inputName->getClientSize();

            if($size < 10000000) // 5 megas
            {
                $name = uniqid(date('HisYmd'));
                $extension = strtoupper($request->$inputName->extension());
                if
                (
                    $extension=='DOC'    ||
                    $extension=='XLSX'   ||
                    $extension=='PDF'    ||
                    $extension=='TXT'    ||
                    $extension=='JPG'    ||
                    $extension=='JPEG'   ||
                    $extension=='PNG'    ||
                    $extension=='XLS'    ||
                    $extension=='PPT'    ||
                    $extension=='PPTX'
                )
                {
                    $nameFile = "{$name}.{$extension}";
                }
                else
                {
                    return null;
                }
                $upload = $request->$inputName->storeAs($path, $nameFile, 'public');

                if ( !$upload )
                {
                    return $nameFile;
                }
                else
                {
                    return
                        [
                            'nameFile'      => $nameFile,
                            'sizeFile'      => $size,
                            'pathFile'      => $path,
                            'extensionFile' => $extension,
                        ];
                }
            }
        }
        return $nameFile;
    }

}
