<?php

namespace App\Http\Controllers\Home\Main;

use App\Http\Controllers\Controller;
use App\Models\Configuration\Configurations;
use App\Services\Home\Main\ServiceMain;
use Illuminate\Http\Request;

class MainController extends Controller
{
    protected                          $configurations;
    protected                          $service;
    protected                          $courses;

    public function __construct
    (
        ServiceMain                     $service,
        Request                         $request,
        Configurations                  $configurations
    )
    {
        $this->service          =       $service;
        $this->request          =       $request;
        $this->configurations   =       $configurations;
    }

    public function index(Request $request)
    {
        $index = $this->service->index($request);
        return view($index['data']['route']['view'], $index['data']['compact']);
    }

    public function show(Request $request, $id, $tag=null)
    {
        $request['id'] = $id;
        $request['tag'] = $tag;
        $show = $this->service->show($request);
        return view($show['data']['route']['view'], $show['data']['compact']);
    }

    public function create(Request $request)
    {
        $create = $this->service->create($request);
        if($create['data']!=false)
        {
            return view($create['data']['route']['ambient'].'.'.$create['data']['route']['crud'].'.'.$create['data']['route']['function'], $create['data']['compact']);
        }
        else
        {
            return back()->with('error', 'Acesso negado')->withErrors('error', 'Acesso negado');
        }
    }

    public function store(Request $request)
    {
        $store = $this->service->store($request);
        if($store['data']!=false)
        {
            if($store['status']=='success')
            {
                return redirect($store['data']['compact']['redirect'])
                    ->with('success', $store['msg'])
                    ->with('boleto', $store['data']['compact']['boleto'])
                    ->with('model', $store['data']['compact']['model']['data']);
            }
            return back()->with($store['status'], $store['msg'])->with('request', $store['request'])->withErrors($store['data']);
        }
        else
        {
            return back()->with('error', 'Acesso negado')->withErrors('error', 'Acesso negado');
        }
    }

    public function edit(Request $request, $id)
    {
        $request['id'] = $id;
        $edit = $this->service->edit($request);
        if($edit['data']!=false)
        {
            return view($edit['data']['route']['ambient'].'.'.$edit['data']['route']['crud'].'.'.$edit['data']['route']['function'], $edit['data']['compact']);
        }
        else
        {
            return back()->with('error', 'Acesso negado')->withErrors('error', 'Acesso negado');
        }
    }

    public function update(Request $request)
    {
        $update = $this->service->update($request);
        if($update['data']!=false)
        {
            if($update['status']=='success')
            {
                return back()->with($update['status'], $update['msg'])->with($update['status'], $update['msg']);
            }
            return back()->with($update['status'], $update['msg'])->with('request', $update['request'])->withErrors($update['data']);
        }
        else
        {
            return back()->with('error', 'Acesso negado')->withErrors('error', 'Acesso negado');
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->service->delete($request);
        if($delete['data']!=false)
        {
            if($delete['status']=='success')
            {
                return back()->with($delete['status'], $delete['msg'])->with($delete['status'], $delete['msg']);
            }
            return back()->with($delete['status'], $delete['msg'])->with('request', $delete['request'])->withErrors($delete['data']);
        }
        else
        {
            return back()->with('error', 'Acesso negado')->withErrors('error', 'Acesso negado');
        }
    }

    public function restore(Request $request)
    {
        $restore = $this->service->restore($request);
        if($restore['data']!=false)
        {
            if($restore['status']=='success')
            {
                return back()->with($restore['status'], $restore['msg'])->with($restore['status'], $restore['msg']);
            }
            return back()->with($restore['status'], $restore['msg'])->with('request', $restore['request'])->withErrors($restore['data']);
        }
        else
        {
            return back()->with('error', 'Acesso negado')->withErrors('error', 'Acesso negado');
        }
    }


    public function validationEmailInscribes(Request $request, $email, $event_id)
    {
        $request['email'] = $email;
        $request['event_id'] = $event_id;
        return $this->service->validationEmailInscribes($request);
    }
    public function validationCPFInscribes(Request $request, $cpf, $event_id)
    {
        $request['cpf'] = $cpf;
        $request['event_id'] = $event_id;
        $request = $this->service->validationCPFInscribes($request);
        return $request;
    }

    public function validationNumberRegistrationInscribes(Request $request, $number_registration, $event_id)
    {
        $request['number_registration'] = $number_registration;
        $request['event_id'] = $event_id;
        return $this->service->validationNumberRegistrationInscribes($request);
    }

    public function search(Request $request, $id, $tag=null)
    {
        $request['id'] = $id;
        $request['tag'] = $tag;
        $show = $this->service->search($request);
        return view($show['data']['route']['ambient'].'.'.$show['data']['route']['crud'].'.'.$show['data']['route']['function'], $show['data']['compact']);
    }

    public function isSubcribeFixed(Request $request)
    {
        $subscriber = $this->service->isSubcribeFixed($request);
        return $subscriber;
    }

}
