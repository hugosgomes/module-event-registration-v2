<section class="page-content container-fluid">
    <div class="row">
        <div class="col">
            <div class="card">
                <h5 class="card-header">Editando {{ $crudName }}</h5>
                <form class="form-horizontal" id="validate-form" method="POST" action="{{ route("$route[0].$route[1].update") }}" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    <input type="hidden" name="id" value="{{ $item['id'] }}">

                    <div class="card-body">

                        <div class="form-body">

                            <div class="form-group row">
                                <label class="control-label text-right col-md-3">Problema</label>
                                <div class="col-md-5">
                                    <textarea class="form-control" autofocus rows="15">{{ $item->content }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right col-md-3">Status</label>
                                <div class="col-md-5">
                                    <select class="form-control {{ $errors->has('status') ? ' is-invalid' : '' }}" autofocus name="status">

                                        @if($item->status==0)
                                            <option value="0" selected>Não Resolvido</option>
                                            <option value="1">Resolvido</option>
                                        @else
                                            <option value="0">Não Resolvido</option>
                                            <option value="1" selected>Resolvido</option>
                                        @endif

                                    </select>
                                    @if ($errors->has('status'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('status') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer bg-light">
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="offset-sm-3 col-md-5">
                                            <button type="submit" class="btn btn-primary btn-rounded submit">Atualizar</button>
                                            <a href="{{ route("$route[0].$route[1].index") }}" class="btn btn-secondary clear-form btn-rounded btn-outline">Cancelar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
