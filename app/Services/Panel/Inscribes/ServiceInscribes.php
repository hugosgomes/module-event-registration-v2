<?php

namespace App\Services\Panel\Inscribes;

use App\Models\Configuration\Configurations;
use App\Models\Configuration\Inscribes;
use App\Repositories\Panel\Inscribes\RepositoryInscribes;
use App\Response\Response;
use App\Validations\Panel\Inscribes\ValidatorInscribes;
use Illuminate\Http\Request;

class ServiceInscribes
{
    //Variáveis Globais

    protected                            $request;
    protected                            $repository;
    protected                            $validator;
    protected                            $response;
    protected                            $model;
    protected                            $configurations;

    //Funções Padrões

    public function __construct
    (
        Request                          $request,
        RepositoryInscribes               $repository,
        ValidatorInscribes                $validator,
        Response                         $response,
        Inscribes                         $model,
        Configurations                   $configurations
    )
    {
        $this->request =                 $request;
        $this->repository =              $repository;
        $this->validator =               $validator;
        $this->response =                $response;
        $this->model =                   $model;
        $this->configurations =          $configurations;
    }


    //Funções Principais

    public function index(Request $request)
    {
        try
        {
            $validator = $this->validator->index($request);
            if($validator->fails())
            {
                return $this->response->return('error','index',null, $validator->errors()->messages());
            }
            $index = $this->repository->index($request);
            return $this->response->return('success','index', null, $index);
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','index', null, $e->getMessage());
        }
    }

    public function show(Request $request)
    {
        try
        {
            $validator = $this->validator->show($request);
            if($validator->fails())
            {
                return $this->response->return('error','show',null, $validator->errors()->messages());
            }
            $show = $this->repository->show($request);
            return $this->response->return('success','show', null, $show);
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','show', null, $e->getMessage());
        }
    }

    public function create(Request $request)
    {
        try
        {
            $validator = $this->validator->create($request);
            if($validator->fails())
            {
                return $this->response->return('error','create',null, $validator->errors()->messages());
            }
            $create = $this->repository->create($request);
            return $this->response->return('success','create', null, $create);
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','create', null, $e->getMessage());
        }
    }

    public function store(Request $request)
    {
        try
        {
            $validator = $this->validator->store($request);
            if($validator->fails())
            {
                return $this->response->return('error','store',$request,$validator->errors()->messages());
            }
            $store = $this->repository->store($request);
            return $this->response->return('success','store', $request->all(), $store);
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','store', $request->all(), $e->getMessage());
        }
    }

    public function edit(Request $request)
    {
        try
        {
            $validator = $this->validator->edit($request);
            if($validator->fails())
            {
                return $this->response->return('error','edit',null, $validator->errors()->messages());
            }
            $edit = $this->repository->edit($request);
            return $this->response->return('success','edit', null, $edit);
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','edit', null, $e->getMessage());
        }
    }

    public function update(Request $request)
    {
        try
        {
            $validator = $this->validator->update($request);
            if($validator->fails())
            {
                return $this->response->return('error','update',$request,$validator->errors()->messages());
            }
            $update = $this->repository->update($request);
            return $this->response->return('success','update', $request->all(), $update);
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','update', $request->all(), $e->getMessage());
        }
    }


    public function delete(Request $request)
    {
        try
        {
            $delete = $this->repository->delete($request);
            if($delete==0)
            {
                return $this->response->return('error','delete', $request, 'Item não encontrado');
            }
            else
            {
                return $this->response->return('success','delete', $request, "Item deletado!");
            }
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','delete', $request, $e->getMessage());
        }
    }

    public function restore(Request $request)
    {
        try
        {
            $validator = $this->validator->restore($request);
            if($validator->fails())
            {
                return $this->response->return('error','restore',$request,$validator->errors()->messages());
            }
            $restore = $this->repository->restore($request);
            return $this->response->return('success','restore', $request->all(), $restore);
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','restore', $request, $e->getMessage());
        }
    }

    public function report(Request $request)
    {
        try
        {
            $report = $this->repository->report($request);
            return $this->response->return('success','report', $request->all(), $report);
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','report', $request, $e->getMessage());
        }
    }

    public function import(Request $request)
    {
        try
        {
            $import = $this->repository->import($request);
            return $this->response->return('success','store', $request->all(), $import);
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','store', $request->all(), $e->getMessage());
        }
    }

    public function export(Request $request)
    {
        try
        {
            $export = $this->repository->export($request);
            return $this->response->return('success','store', $request->all(), $export);
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','store', $request->all(), $e->getMessage());
        }
    }
}
