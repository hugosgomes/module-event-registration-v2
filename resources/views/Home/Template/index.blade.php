<!DOCTYPE html>
<html lang="pt-br">
    @includeIf("$route[0].Template.head")
    <body>
        @includeIf("$route[0].Template.alerts")
        {{-- @includeIf("$route[0].Template.preloader") --}}
        {{-- @includeIf("$route[0].Template.loader_form") --}}
        {{-- @includeIf("$route[0].Template.header") --}}
        {{-- @includeIf("$route[0].Template.parallax_window_in") --}}
        @yield('content')
        {{-- @includeIf("$route[0].Template.footer") --}}
        @includeIf("$route[0].Template.cd-overlay-nav")
        @includeIf("$route[0].Template.cd-overlay-content")
        @includeIf("$route[0].Template.javascript")
    </body>
</html>




