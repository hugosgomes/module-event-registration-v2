<?php

use App\Events\Users\UserRegister;
use App\Models\Configuration\Roles;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('roles')->delete();
        //Criando os Admins
        Roles::create([
            'id'                   => 1,
            'name'                 =>'Administrator',
            'label'                =>'Administrador Master',
            'system'               =>1,
            'group'                =>'Funções do Sistema',
            'create_user_id'       =>1,
        ]);
        //Criando as Funções de Temporary
        Roles::create([
            'id'                   => 2,
            'name'                 =>'Temporary',
            'label'                =>'Usuários Temporários',
            'system'               =>1,
            'group'                =>'Funções do Sistema',
            'create_user_id'       =>1,
        ]);

        //Criando as Funções de Dev
        Roles::create([
            'id'                   => 3,
            'name'                 =>'Development1',
            'label'                =>'Função Geral de Desenvolvimento',
            'system'               =>1,
            'group'                =>'Funções do Sistema',
            'create_user_id'       =>1,
        ]);

        //Criando as Funções de Operator
        Roles::create([
            'id'                   => 4,
            'name'                 =>'Operador',
            'label'                =>'Função Geral de Operador',
            'system'               =>1,
            'group'                =>'Funções do Sistema',
            'create_user_id'       =>1,
        ]);
        //------------------------------------------------
        event(new UserRegister(User::find(1), 1));
        event(new UserRegister(User::find(2), 3));
        event(new UserRegister(User::find(3), 4));
    }
}
