<div class="lateral-cursos">

    <!--- Card para cursos --->
    <h2>Menu Lateral</h2>
    <div class="card unigdigital">
        <div class="progresso-100"></div>
        <div>
            <h4 class="f-20">Título Card</h4>
            <p>
                Texto Card<br>
                <img class="card-icone" src="{{ asset('HomeP/img/pin.svg') }}" /> Texto 2 Card
            </p>
        </div>
    </div>
    <!------------------------>

</div>
