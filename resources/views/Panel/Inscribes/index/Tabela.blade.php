<style>

.formulario select{
    height: 38px;
    padding: 0 30px;
    border-radius: .25rem !important;
    margin-bottom: 20px;
    background: #28a745;
    color: white;
    border-color: white;
}

.formulario select option{
    line-height: 80px;
}

.icon-money:before {
    content: '\e91a';
}

table .btn {
    width: 40px;
}

.btn-outline-light {
    width: 100px !important;
}

.fa-dollar-sign:before {
  content: "\f155"; }
</style>

<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title"><h3>Tabela de {{ $crudName }} - Quantidade de Inscritos: {{ $model->count() }}</h3></div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-3">
                            <form action="{{route('Panel.Inscribes.index')}}" method="get">
                                <label class="control-label text-right">Evento</label>
                                <select class="form-control" id="event_id" name="event_id" required {{ $disabled }} style="margin-right:0; display: inline; width:300px;">
                                <option value="">Todos</option>
                                    @foreach ($events as $event)
                                        <option value="{{$event['id']}}" {{ $request['event_id'] == $event['id'] ? 'selected' : '' }}>{{$event['name']}}</option>
                                    @endforeach
                                </select>
                            </form>
                        </div>
                        {{-- VESTIBULAR --}}
                        @if ($request['event_id'] == 5)
                            <div class="col-lg-2">
                                <a href="{{route('Panel.Inscribes.export')}}" class="btn btn-success">Exportar Inscritos Csv</a>
                            </div>
                        @endif
                    </div>
                    <br>
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width: 5%;">#</th>
                                <th style="width: 10%;">Evento</th>
                                <th style="width: 20%;">Nome</th>
                                <th style="width: 5%;">Aluno?</th>
                                <th style="width: 5%;">Matrícula</th>
                                <th style="width: 5%;">Pago?</th>
                                <th style="width: 8%;">Cpf</th>
                                <th style="width: 7%;">Nascimento</th>
                                <th style="width: 15%;">E-mail</th>
                                <th style="width: 10%;">Celular</th>
                                <th style="width: 10%;">Ação</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($model as $item)
                            @if($item->person)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->event['name'] }}</td>
                                    <td>{{ $item->person['full_name'] }}</td>
                                    <td>{{ $item->isStudent }}</td>
                                    <td>{{ $item->registration['number_registration'] ?? null }}</td>
                                    <td>{{ $item->status == 3 ? 'Sim' : 'Não' }}</td>
                                    <td>{{ $item->person['cpf'] }}</td>
                                    <td>{{ $item->person['birth'] }}</td>
                                    <td>{{ $item->person['email'] }}</td>
                                    <td>
                                        @if ($item->person['phones'])
                                            @foreach ($item->person['phones'] as $phone)
                                                @if ($phone['type'] == 'cell_phone')
                                                    {{ $phone['formated_phone'] }}
                                                    @break
                                                @endif
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(isset($route[0]) && isset($route[1]))

                                            @can("$route[0].$route[1].edit")
                                                <a href="{{ route("$route[0].$route[1].edit", $item->id) }}" class="btn btn-warning" title="Editar"><i class="fa fa-edit"></i></a>
                                            @endcan

                                            @can("$route[0].$route[1].delete")
                                                <a href="#" id="{{ $item->id }}" data-toggle="modal" data-target="#modal-delete{{ $item->id }}" class="btn btn-danger" title="Excluir"><i class="fa fa-trash"></i></a>
                                                <div class="modal fade" id="modal-delete{{ $item->id }}">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content bg-danger">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Exclusão</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>Item de ID: {{ $item->id }}, será excluído!</p>
                                                                <p>Tem certeza que deseja excluir?</p>
                                                            </div>
                                                            <form method="get" action="{{ route("$route[0].$route[1].delete", $item->id) }}">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="id" value="{{ $item->id }}"/>
                                                                <div class="modal-footer justify-content-between">
                                                                    <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancelar</button>
                                                                    <button type="submit" class="btn btn-outline-light">Excluir</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endcan

                                            @can("Panel.PaymentInscribes.update")
                                                <a href="#" id="{{ $item->id }}" data-toggle="modal" data-target="#modal-manual-payment{{ $item->id }}" class="btn btn-success" title="Confirmar Pagamento Manual"><i class="far fa-money-bill-alt"></i></a>
                                                <div class="modal fade" id="modal-manual-payment{{ $item->id }}">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content bg-success">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Confirmaçaõ de Pagamento Manual</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <form class="formulario" method="post" action="{{ route("Panel.PaymentInscribes.update") }}">
                                                            <div class="modal-body">
                                                                <p>O Pagamento Será Confirmado Manualmente!</p>
                                                                <p>Tem certeza que deseja continuar?</p>
                                                                <select name="manual_payment_reason" id="manual_payment_reason">
                                                                    <option value="Envio de Comprovante">Envio de Comprovante</option>
                                                                    <option value="Funcionário">Funcionário</option>
                                                                </select>
                                                            </div>
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="inscribe_id" value="{{ $item->id }}"/>
                                                                <input type="hidden" name="name" value="{{ $item->event['name'] }}"/>
                                                                <input type="hidden" name="value" value="0.00"/>
                                                                <div class="modal-footer justify-content-between">
                                                                    <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancelar</button>
                                                                    <button type="submit" class="btn btn-outline-light">Confirmar</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endcan

                                        @endif
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Evento</th>
                                <th>Nome</th>
                                <th>Aluno?</th>
                                <th>Matrícula</th>
                                <th>Pago?</th>
                                <th>Cpf</th>
                                <th>Nascimento</th>
                                <th>E-mail</th>
                                <th>Celular</th>
                                <th>Ação</th>
                            </tr>
                        </tfoot>
                    </table>


                </div>
            </div>
        </div>
    </div>
    <form action="{{ route('Panel.Inscribes.import') }}" method="post" enctype="multipart/form-data";>
        {{ csrf_field() }}
        <input type="hidden" name="type" value="0">
        <input type="file" name="planilha" id="planilha" accept=".csv, .xls, .xlsx" style="display:none">
    </form>
</section>
