<!DOCTYPE html>
<html lang="pt-BR">
    @includeIf("$route[0].Template.head")
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            @includeIf("$route[0].Template.navbar")
            @yield('content')
            @includeIf("$route[0].Template.main-sidebar")
            @includeIf("$route[0].Template.footer")
            @includeIf("$route[0].Template.aside-right")
        </div>
        @includeIf("$route[0].Template.javascript")
        @includeIf("Template.javascript-mask")
    </body>
</html>
