<?php

namespace App\Listeners\Users;

use App\Events\Users\UserRegister;
use App\Events\Users\UserUpdate;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RelationshipUpdate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegister  $event
     * @return void
     */
    public function handle(UserUpdate $event)
    {
        $event->user->roles()->sync($event->role_id);
    }
}
