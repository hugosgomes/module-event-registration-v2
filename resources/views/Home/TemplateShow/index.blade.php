<!DOCTYPE html>
<html lang="pt-br">
    @includeIf("$route[0].TemplateShow.head")
    <body>
        @includeIf("$route[0].TemplateShow.alerts")
        @includeIf("$route[0].TemplateShow.preloader")
        @includeIf("$route[0].TemplateShow.loader_form")
        @includeIf("$route[0].TemplateShow.cd-primary-nav")
        @yield('content')
        @includeIf("$route[0].TemplateShow.cd-overlay-nav")
        @includeIf("$route[0].TemplateShow.cd-overlay-content")
        {{-- @includeIf("$route[0].TemplateShow.cd-nav-trigger") --}}
        @includeIf("$route[0].TemplateShow.modal")
        @includeIf("$route[0].TemplateShow.javascript")
    </body>
</html>




