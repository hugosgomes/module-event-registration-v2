@extends("$route[0].Template.index")

@section('content')
    <div class="content-wrapper">

        @includeIf("$route[0].$route[1].report.Header")
        @includeIf("$route[0].$route[1].report.Tabela")

    </div>
@endsection

