@extends("$route[0].TemplateShow.index")

@section('content')

    <div class="container-fluid full-height">
        <div class="row row-height">
            <div class="col-lg-6 content-left">
                <div class="content-left-wrapper">
                    {{-- <a href="{{ route("Home.Main.index") }}" id="logo"><img src="{{ $cms_page->logo2!=null && $cms_page->logo2!='default' ? asset("HomeP/img/CmsPages/$cms_page->logo2") : asset("HomeP/img/CmsPages/logo2.png") }}" alt="" width="35" height="35"></a> --}}
                    <div>
                        <i style="display: none;">{{ $img = $model['img'] }}</i>
                        <figure><img src="{{ $img!=null && $img!='default' ? env('PATH_URL_EUNIG') . "PanelP/img/Events/$img" : env('PATH_URL_EUNIG') . "PanelP/img/Events/default.png" }}" alt="" class="img-fluid" width="200" height="200"></figure>
                        <p><a href="{{route('Home.Main.show', ['id' => $model['id']])}}" class="btn_1 rounded">Inscreva-se</a>
                        <a href="{{route('Home.Main.show', ['id' => $model['id']])}}" class="btn_1 rounded mobile_btn">Inscreva-se</a>
                    </div>
                    <div class="copy">© {{ date('Y') }} Unig Digital</div>
                </div>
                <!-- /content-left-wrapper -->
            </div>
            <!-- /content-left -->

            <div class="col-lg-6 content-right" id="start">
                <div id="wizard_container">
                    <div id="top-wizard">
                        <div id="progressbar"></div>
                    </div>
                    <!-- /top-wizard -->


                    @includeIf("Home.Main.Forms.search")


                </div>
                <!-- /Wizard container -->
            </div>
            <!-- /content-right-->
        </div>
        <!-- /row-->
    </div>
    <!-- /container-fluid -->
@endsection

