<?php

namespace App\Http\Controllers\Auth;

use App\Events\Users\UserRegister;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\Configuration\Configurations;
use Illuminate\Http\Request;
use Gate;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected                     $redirectTo = '/Panel/Main';
    protected                     $configurations;
    protected                     $routeArray;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Configurations $configurations, Request $request)
    {
        $this->middleware('guest');
        $this->request =          $request;
        $this->configurations =   $configurations;
        $this->routeArray =       $this->configurations->getRouteName($request);
    }

    public function showRegistrationForm(Request $request)
    {
        $title = 'Faça seu Cadastro';
        $route = ['auth','main','register'];
        $ipaddress = $this->configurations->ipClient();
        return view('auth.register', compact('ipaddress', 'request', 'route', 'title'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'           => ['required', 'string', 'max:255'],
            'email'          => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password'       => ['required', 'string', 'min:8', 'confirmed'],
            'role_id'        => ['required']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if(isset(auth()->user()->roles[0]) && (auth()->user()->roles[0]->name=='Administrator' || auth()->user()->roles[0]->name=='Development1'))
        {
            $user =  User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
            ]);
            event(new UserRegister($user, $data['role_id']));
            return redirect('/Panel/Users/index')->with('success', 'Cadastro Realizado com sucesso');
        }
        else
        {
            $user =  User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
            ]);
            event(new UserRegister($user, 2));
            return $user;
        }
    }
}
