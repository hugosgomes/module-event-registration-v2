<section class="page-content container-fluid">
    <div class="row">
        <div class="col">
            <div class="card">
                <h5 class="card-header">Criando {{ $crudName }}</h5>
                <form class="form-horizontal" id="validate-form" method="POST" action="{{ route("$route[0].$route[1].store") }}" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    <div class="card-body">

                        <div class="form-body">
                            <div class="form-group row">
                                <label class="control-label text-right col-md-3">Nome</label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="Nome da Função" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" autofocus name="name" value="{{ session('request')['name'] ? session('request')['name'] : '' }}">
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <hr class="dashed ">
                            <div class="form-group row">
                                <label class="control-label text-right col-md-3">Descrição</label>
                                <div class="col-md-5">
                                    <textarea placeholder="Digite a descrição" class="form-control {{ $errors->has('label') ? ' is-invalid' : '' }}" name="label" rows="5">{{ session('request')['label'] ? session('request')['label'] : '' }}</textarea>
                                    @if ($errors->has('label'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('label') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <hr class="dashed ">

                            <div class="form-group row">
                                <label class="control-label text-right col-md-1">Permissões: </label>
                                <div class="col-md-10">
                                    <select class="form-control duallistbox {{ $errors->has('permissions_ids') ? ' is-invalid' : '' }}" multiple="multiple" name="permissions_ids[]" style="height: 300px;">
                                        @foreach($groups as $group)
                                            @if($group->group!='Levels')
                                                <optgroup label="{{ $group->group }}">
                                                    @foreach($permissions as $permission)
                                                        @if($permission->group==$group->group && $permission->group!='Levels')
                                                            @if($permission->default==1)
                                                                <option value="{{ $permission->id }}" selected>{{ $permission->name }} --- ({{ $permission->label }})</option>
                                                            @else
                                                                <option value="{{ $permission->id }}">{{ $permission->name }} --- ({{ $permission->label }})</option>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </optgroup>
                                            @endif
                                        @endforeach
                                    </select>
                                    @if ($errors->has('permissions_ids'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('permissions_ids') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer bg-light">
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="offset-sm-3 col-md-5">
                                            <button type="submit" class="btn btn-primary btn-rounded submit">Cadastrar</button>
                                            <a href="{{ route("$route[0].$route[1].index") }}" class="btn btn-secondary clear-form btn-rounded btn-outline">Cancelar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
