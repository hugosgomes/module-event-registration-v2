<script src="{{ asset('PanelP/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('PanelP/plugins/datatables/dataTables.bootstrap4.js') }}"></script>
<!--Data Tables js-->
<script src="{{ asset('PanelP/assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('PanelP/assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('PanelP/assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('PanelP/assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('PanelP/assets/plugins/bootstrap-datatable/js/jszip.min.js') }}"></script>
<script src="{{ asset('PanelP/assets/plugins/bootstrap-datatable/js/pdfmake.min.js') }}"></script>
<script src="{{ asset('PanelP/assets/plugins/bootstrap-datatable/js/vfs_fonts.js') }}"></script>
<script src="{{ asset('PanelP/assets/plugins/bootstrap-datatable/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('PanelP/assets/plugins/bootstrap-datatable/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('PanelP/assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js') }}"></script>
<script>
    $(function () {
        $('#example2').DataTable({
            dom: 'Bfrtip',
            buttons:
                [
                    {
                        extend:    'copy',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-file"> | Copiar</i>',
                        titleAttr: 'Copiar para Área de Transferência',
                    },
                    {
                        extend:    'excel',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-file-excel"> | Exp. Excel</i>',
                        titleAttr: 'Exportar para Arquivo  de Excel'
                    },
                    {
                        extend:    'pdf',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-file-pdf"> | Exp. PDF</i>',
                        titleAttr: 'Exportar para Arquivo  de PDF'
                    },
                    {
                        extend:    'csv',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-file-csv"> | Exp. CSV</i>',
                        titleAttr: 'Exportar para Arquivo  de CSV'
                    },
                    {
                        extend:    'print',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-print"> | Imprimir</i>',
                        titleAttr: 'Imprimir esta tabela'
                    },
                    @can('Panel.Inscribes.import')
                    {
                        className: 'btn btn-primary',
                        text: '<i class="fa fa-plus"> | Importar</i>',
                        action: importFile,
                        titleAttr: "Importar {{ $crudName }}"
                    },
                    @endcan
                    {
                        extend:    'colvis',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-cogs"> | Config. Tab.</i>',
                        titleAttr: 'Selecionar Colunas'
                    },
                ],
            stateSave: true,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "pagingType": "full_numbers",
            "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            }
        });
    });
</script>


<script>
    $(function () {
        $('#example3').DataTable({
            dom: 'Bfrtip',
            buttons:
                [
                    {
                        extend:    'copy',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-file"> | Copiar</i>',
                        titleAttr: 'Copiar para Área de Transferência',
                    },
                    {
                        extend:    'excel',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-file-excel"> | Exp. Excel</i>',
                        titleAttr: 'Exportar para Arquivo  de Excel'
                    },
                    {
                        extend:    'pdf',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-file-pdf"> | Exp. PDF</i>',
                        titleAttr: 'Exportar para Arquivo  de PDF'
                    },
                    {
                        extend:    'csv',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-file-csv"> | Exp. CSV</i>',
                        titleAttr: 'Exportar para Arquivo  de CSV'
                    },
                    {
                        extend:    'print',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-print"> | Imprimir</i>',
                        titleAttr: 'Imprimir esta tabela'
                    },
                ],
            stateSave: true,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "pagingType": "full_numbers",
            "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            }
        });
    });
</script>

<script>
    $('#event_id, #unity').change(function () {
        $(this).closest('form').trigger('submit');
    });
</script>
<script>
    function importFile() {
        $('#planilha').trigger('click');
    }

    $('#planilha').change(function () {
        if($('#planilha').val() !=  ""){
            $('.alertLoagingImport').fadeIn();
            $(this).closest('form').trigger('submit');
        }
    });
</script>

<script>
    //PESQUISA DE CPF NA API EXTERNA
    $("#cep").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Consulta o webservice viacep.com.br/
            $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                if (!("erro" in dados)) {
                    //Atualiza os campos com os valores da consulta.
                    $("#street").val(dados.logradouro);
                    $("#district").val(dados.bairro);
                    $("#city").val(dados.localidade);
                    $("#state").val(dados.uf);
                    $("#complement").val(dados.complemento);
                }
                else {
                    alert("CEP não encontrado.");
                }
            });
        }
    });
</script>
