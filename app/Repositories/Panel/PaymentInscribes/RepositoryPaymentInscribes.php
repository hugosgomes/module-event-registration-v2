<?php

namespace App\Repositories\Panel\PaymentInscribes;

use App\APIIntranet\Models\ModelBase;
use App\Models\Configuration\Configurations;
use App\Models\Configuration\Inscribes;
use App\Models\Configuration\PaymentInscribes;
use Illuminate\Http\Request;
use Gate;
use Illuminate\Support\Arr;

class RepositoryPaymentInscribes
{
    //Variáveis Globais

    protected                     $request;
    protected                     $model;
    protected                     $routeArray;
    protected                     $modelsIntranet;

    //Funções Padrões

    public function __construct
    (
        Request                   $request,
        PaymentInscribes          $model,
        Configurations            $configurations,
        ModelBase                 $modelsIntranet
    )
    {
        $this->request          = $request;
        $this->model            = $model;
        $this->configurations   = $configurations;
        $this->modelsIntranet   = $modelsIntranet;
        $this->routeArray       = $this->configurations->getRouteName($request);
        $this->crudName         = 'Inscrições';
    }


    //Main Functions

    public function index(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model = $this->model->where('id', '!=', 0)->get();
            return
            [
                'route'       =>
                    [
                        'ambient'    => $this->routeArray[0],
                        'crud'       => $this->routeArray[1],
                        'function'   => $this->routeArray[2],
                    ],
                'compact'     =>
                    [
                        'title'      => $this->crudName,
                        'route'      => $this->routeArray,
                        'crudName'   => $this->crudName,
                        'model'      => $model,
                        'trashed'    => $this->model->where('id', '!=', 0)->onlyTrashed()->get()
                    ],
                'request'            => $request,
            ];
        }
        else
        {
            return false;
        }
    }

    public function show(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model = $this->model->find($request['id']);
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $model->id,
                            'route'      => $this->routeArray,
                            'crudName'   => $this->crudName,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function create(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model = $this->model;
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'crudName'   => $this->crudName,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function store(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            //-----------------------------------------------------
            $model =  $this->model->create($request->all());
            //-----------------------------------------------------
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                            'redirect'   => $this->routeArray[0].'/'.$this->routeArray[1].'/index'
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function edit(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model = $this->model->find($request['id']);
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                            'item'       => $model,
                            'crudName'   => $this->crudName,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function update(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]) && $this->configurations->CrudSystemNotModified($request, $this->model)==false)
        {
            $data = $request->all();
            //-----------------------------------------------------
            $model = $this->model->orWhere('id', $data['inscribe_id'])
                ->orWhere('inscribe_id', $data['inscribe_id'])
                ->first();


            if ($data['inscribe_id']) {
                $inscribe = Inscribes::find($data['inscribe_id']);
                $inscribe->update(['status' => 3]);
            }

            if (!$model) {
                $data['type'] = 'boleto';
                $data['inscribe_date'] = $inscribe->created_at;
                $data['code'] = '0';
                $data['status'] = 3;
                $data['email_confirmed'] = 1;
                $this->model->create($data);
            }else{
                $model->update($data);
            }

            //-----------------------------------------------------
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }


    public function delete(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]) && $this->configurations->CrudSystemNotModified($request, $this->model)==false)
        {
            $model = $this->model->destroy($request['id']);
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function restore(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model = $this->model->withTrashed()->where('id', $request['id'])->restore();
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

}
