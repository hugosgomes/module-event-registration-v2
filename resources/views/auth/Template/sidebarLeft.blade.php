<input type="checkbox" id="check" checked>

<div class="lateral-home">

    <div class="fechar">
        <label for="check">
            <img src="{{ asset('HomeP/img/fechar.png') }}" />
        </label>
    </div>

    <div class="logo">
        <img src="{{ asset('HomeP/img/logo.png') }}" />
    </div>

    <div class="box-login">
        <div class="user">
            <img src="{{ asset('HomeP/img/icone-user-white.svg') }}" />
        </div>
        <p>
            Seja bem-vindo(a),<br>faça seu login para acessar a plataforma:
        </p>

        @if ($errors->has('email'))
            <div class="error-login">
                <span>{{ $errors->first('email') }}</span>
            </div>
        @endif

        @if ($errors->has('password'))
            <div class="error-login">
                <span>{{ $errors->first('password') }}</span>
            </div>
        @endif

        <form method="post" action="{{ route('login') }}">
            {{ csrf_field() }}
            <input type="text" class="login-home {{ $errors->has('email') ? ' is-invalid' : '' }}" id="user-login" name="email" placeholder="Matrícula" required><br>
            <input type="password" class="pass-home {{ $errors->has('password') ? ' is-invalid' : '' }}" id="user-pass" name="password" placeholder="Senha" required><br>
            <input type="submit" class="submit-home" value="Entrar →">
        </form>
    </div>

    <div class="box-ip-home">
        <p>Unig Digital.</p>
    </div>
</div>

<div class="lateral-home-closed">
    <div class="fechado">
        <img class="logo-simples" src="{{ asset('HomeP/img/logo-simples.png') }}" width="40" />
        <label for="check">
            <img src="{{ asset('HomeP/img/menu.png') }}" />
        </label>
    </div>
</div>
