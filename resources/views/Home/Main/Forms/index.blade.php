<!--Tag id="wrapped" retirada -->
<style>
    select.form-control {
        height: 42px !important;
    }
</style>


<form method="post" action="{{route("Home.Main.store")}}" enctype="multipart/form-data">
    <input id="website" name="website" type="text" value="">
    <input name="event_id" type="hidden" value="{{ $model['id'] }}">
    <input name="event_name" type="hidden" value="{{ $model['name'] }}">
    <input id="registration_id" name="registration_id" type="hidden" value="">
    <input id="person_id" name="person_id" type="hidden" value="">
    <input id="hashInput" name="hashInput" type="hidden" value="">
    <input id="fixed_subscribers" name="fixed_subscribers" type="hidden" value="{{isset($model['fixed_subscribers']) ? $model['fixed_subscribers'] : ''}}">
    <input id="price" name="price" type="hidden" value="{{$model['type']['name'] == 'Vestibular' ? '30.00' : ''}}">
    {{ csrf_field() }}
        <div id="middle-wizard">
        @forelse($steps as $item => $step)
            <i style="display: none;">{{ $item + 1 == count($steps) ? $submit = 'submit' : $submit = '' }}</i>
        <div class="step {{ $submit }}" step="{{$item+1}}">
                <h3 class="main_question"><strong> {{ $item+1 }} / {{ count($steps) }} </strong>{{ $step['name'] }}</h3>
                @forelse($step['fields'] as $field)
                    {{-- VESTIBULAR --}}
                    @if ($model['type']['name'] == 'Vestibular' && $item == 0)
                        <div class="form-group">
                            <select id="unity" class="col-12 form-control required" name="unity" size="1">
                                <option value="">SELECIONE O CAMPUS</option>
                                <option value="IT">ITAPERUNA</option>
                                <option value="NI">NOVA IGUAÇU</option>
                            </select>
                        </div>
                        <div id="course-void" class="form-group">
                            <select id="fpasso1:curso" class="col-12 form-control" name="course" size="1">
                                <option value="">SELECIONE O CURSO - TURNO</option>
                            </select>
                        </div>
                        <div id="course-it" class="form-group" style="display:none">
                            <select id="fpasso1:curso" class="col-12 form-control" name="course" size="1">
                                <option value="">SELECIONE O CURSO - TURNO</option>
                                <option value="ADMINISTRACAO - Turno: NOITE">ADMINISTRACAO - Turno: NOITE - Vagas: 162</option>
                                <option value="DIREITO - Turno: MANHA">DIREITO - Turno: MANHA - Vagas: 54</option>
                                <option value="DIREITO - Turno: NOITE">DIREITO - Turno: NOITE - Vagas: 135</option>
                                <option value="EDUCACAO FISICA - LICENCIATURA - Turno: NOITE">EDUCACAO FISICA - LICENCIATURA - Turno: NOITE - Vagas: 117</option>
                                <option value="ENFERMAGEM - Turno: NOITE">ENFERMAGEM - Turno: NOITE - Vagas: 54</option>
                                <option value="ENGENHARIA CIVIL - Turno: MANHA">ENGENHARIA CIVIL - Turno: MANHA - Vagas: 54</option>
                                <option value="ENGENHARIA CIVIL - Turno: NOITE">ENGENHARIA CIVIL - Turno: NOITE - Vagas: 126</option>
                                <option value="ENGENHARIA DE PRODUCAO - Turno: NOITE">ENGENHARIA DE PRODUCAO - Turno: NOITE - Vagas: 81</option>
                                <option value="FARMACIA - Turno: NOITE">FARMACIA - Turno: NOITE - Vagas: 135</option>
                                <option value="FISIOTERAPIA - Turno: NOITE">FISIOTERAPIA - Turno: NOITE - Vagas: 58</option>
                                <option value="MEDICINA VETERINARIA - Turno: TARDE">MEDICINA VETERINARIA - Turno: TARDE - Vagas: 72</option>
                                <option value="NUTRICAO - Turno: MANHA">NUTRICAO - Turno: MANHA - Vagas: 36</option>
                                <option value="NUTRICAO - Turno: NOITE">NUTRICAO - Turno: NOITE - Vagas: 72</option>
                                <option value="ODONTOLOGIA - Turno: INTEGRAL">ODONTOLOGIA - Turno: INTEGRAL - Vagas: 108</option>
                            </select>
                        </div>
                        <div id="course-ni" class="form-group" style="display:none">
                            <select id="fpasso1:curso" class="col-12 form-control" name="course" size="1">
                                <option value="">SELECIONE O CURSO - TURNO</option>
                                <option value="ADMINISTRACAO - Turno: NOITE">ADMINISTRACAO - Turno: NOITE - Vagas: 162</option>
                                <option value="BIOMEDICINA - Turno: NOITE">BIOMEDICINA - Turno: NOITE - Vagas: 72</option>
                                <option value="CIENCIAS BIOLOGICAS - BACHARELADO - Turno: NOITE">CIENCIAS BIOLOGICAS - BACHARELADO - Turno: NOITE - Vagas: 36</option>
                                <option value="CIENCIAS BIOLOGICAS - LICENCIATURA - Turno: NOITE">CIENCIAS BIOLOGICAS - LICENCIATURA - Turno: NOITE - Vagas: 72</option>
                                <option value="DIREITO - Turno: MANHA">DIREITO - Turno: MANHA - Vagas: 67</option>
                                <option value="DIREITO - Turno: NOITE">DIREITO - Turno: NOITE - Vagas: 180</option>
                                <option value="DIREITO - Turno: TARDE">DIREITO - Turno: TARDE - Vagas: 144</option>
                                <option value="EDUCACAO FISICA - BACHARELADO - Turno: NOITE">EDUCACAO FISICA - BACHARELADO - Turno: NOITE - Vagas: 45</option>
                                <option value="EDUCACAO FISICA - LICENCIATURA - Turno: MANHA">EDUCACAO FISICA - LICENCIATURA - Turno: MANHA - Vagas: 45</option>
                                <option value="EDUCACAO FISICA - LICENCIATURA - Turno: NOITE">EDUCACAO FISICA - LICENCIATURA - Turno: NOITE - Vagas: 135</option>
                                <option value="ENFERMAGEM - Turno: MANHA">ENFERMAGEM - Turno: MANHA - Vagas: 54</option>
                                <option value="ENFERMAGEM - Turno: NOITE">ENFERMAGEM - Turno: NOITE - Vagas: 81</option>
                                <option value="ENFERMAGEM - Turno: TARDE">ENFERMAGEM - Turno: TARDE - Vagas: 45</option>
                                <option value="ENGENHARIA CIVIL - Turno: MANHA">ENGENHARIA CIVIL - Turno: MANHA - Vagas: 36</option>
                                <option value="ENGENHARIA CIVIL - Turno: NOITE">ENGENHARIA CIVIL - Turno: NOITE - Vagas: 126</option>
                                <option value="ENGENHARIA DE PRODUCAO - Turno: NOITE">ENGENHARIA DE PRODUCAO - Turno: NOITE - Vagas: 162</option>
                                <option value="ENGENHARIA MECANICA - Turno: NOITE">ENGENHARIA MECANICA - Turno: NOITE - Vagas: 162</option>
                                <option value="FARMACIA - Turno: MANHA">FARMACIA - Turno: MANHA - Vagas: 45</option>
                                <option value="FARMACIA - Turno: NOITE">FARMACIA - Turno: NOITE - Vagas: 90</option>
                                <option value="FISIOTERAPIA - Turno: NOITE">FISIOTERAPIA - Turno: NOITE - Vagas: 90</option>
                                <option value="MEDICINA VETERINARIA - Turno: TARDE">MEDICINA VETERINARIA - Turno: TARDE - Vagas: 144</option>
                                <option value="NUTRICAO - Turno: MANHA">NUTRICAO - Turno: MANHA - Vagas: 45</option>
                                <option value="NUTRICAO - Turno: NOITE">NUTRICAO - Turno: NOITE - Vagas: 99</option>
                                <option value="ODONTOLOGIA - Turno: INTEGRAL">ODONTOLOGIA - Turno: INTEGRAL - Vagas: 180</option>
                                <option value="PEDAGOGIA - Turno: NOITE">PEDAGOGIA - Turno: NOITE - Vagas: 72</option>
                                <option value="TECNOLOGIA EM ESTETICA E COSMETICA - Turno: NOITE">TECNOLOGIA EM ESTETICA E COSMETICA - Turno: NOITE - Vagas: 72</option>
                                <option value="TECNOLOGIA EM GESTAO DE RECURSOS HUMANOS - Turno: NOITE">TECNOLOGIA EM GESTAO DE RECURSOS HUMANOS - Turno: NOITE - Vagas: 90</option>
                                <option value="TECNOLOGIA EM LOGISTICA - Turno: NOITE">TECNOLOGIA EM LOGISTICA - Turno: NOITE - Vagas: 72</option>
                            </select>
                        </div>
                    @elseif($model['fixed_subscribers'] && $item == 0)
                        <div class="ead col-12">
                            <div class="form-group">
                                <input type="text" name="cpf_ead" class="form-control required" id="cpf_ead" placeholder="Digite o CPF..." minlength="11" size="11">
                            </div>
                            <div id="loading_cpf_ead" style="margin-bottom: 10px; display:none">
                                <span style="color: darkblue;"><img src="{{ asset('HomeP/img/loading.gif') }}" style="width: 30px;height: 30px;margin-bottom: 10px;"> | Verificando CPF</span>
                            </div>
                            <div id="inscribe_cpf_ead_success" style="margin-bottom: 10px; display:none">
                                <span style="color: darkgreen;"><img src="{{ asset('HomeP/img/icon-success.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | Prossiga a inscrição!</span>
                            </div>
                            <div id="inscribe_cpf_ead_error" style="margin-bottom: 10px; display:none">
                                <span style="color: darkred;"><img src="{{ asset('HomeP/img/icon-error.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | CPF já inscrito!</span>
                            </div>
                            <div id="cpf_ead_invalidated" style="margin-bottom: 10px; display:none">
                                <span style="color: darkred;"><img src="{{ asset('HomeP/img/icon-error.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | CPF não encontrado! Entre em contato pelo chat!</span>
                            </div>
                        </div>
                    @endif
                    @if ($model['type']['name'] == 'Vestibular' && $item == 4)
                        <div class="form-group">
                            <select id="race" class="col-12 form-control required" name="race" size="1">
                                <option value="" selected="selected">RAÇA/COR</option>
                                <option value="NAO DECLARADA">NÃO DECLARADA</option>
                                <option value="BRANCA">BRANCA</option>
                                <option value="PRETA">PRETA</option>
                                <option value="PARDA">PARDA</option>
                                <option value="AMARELA">AMARELA</option>
                                <option value="INDIGENA">INDÍGENA</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input id="country_of_origin" type="text" name="country_of_origin" class="col-12 form-control required" placeholder="País de Origem" style="margin-bottom: 10px;">
                        </div>
                        <div class="form-group">
                            <input id="naturalness" type="text" name="naturalness" class="col-12 form-control required" placeholder="Naturalidade" style="margin-bottom: 10px;">
                        </div>
                        <div class="form-group">
                            <input id="municipality_of_origin" type="text" name="municipality_of_origin" class="col-12 form-control required" placeholder="Município de Origem" style="margin-bottom: 10px;">
                        </div>
                        <div class="form-group">
                            <input id="passport" type="text" name="passport" class="col-12 form-control" placeholder="Passaporte" style="margin-bottom: 10px;">
                        </div>
                    @endif
                    {{-- VESTIBULAR --}}
                    @if($field['type']=='text' || $field['type']=='number' || $field['type']=='date' || $field['type']=='email' || $field['type']=='url')
                        <div class="form-group">
                            <input id="{{ $field['tag_id'] }}" {{ $field['required']==1 ? 'required' : '' }} type="{{ $field['type'] }}" name="{{ $field['name']['name'] }}" {{ $field['tags_extras'] }} class="{{ $field['class'] }} {{ $field['required']==1 ? 'required' : '' }} col-{{ $field['cols'] }} form-control" placeholder="{{ $field['placeholder'] }}" style="margin-bottom: 10px;">
                            @if($item==0)
                                <div class="error_number_registration_null" style="margin-bottom: 10px;">
                                    <span style="color: darkred;"><img src="{{ asset('HomeP/img/icon-error.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | Digite o número de matrícula se for aluno!!</span>
                                </div>
                                <div id="loading_person" style="margin-bottom: 10px;">
                                    <span style="color: darkblue;"><img src="{{ asset('HomeP/img/loading.gif') }}" style="width: 30px;height: 30px;margin-bottom: 10px;"> | Verificando os dados no sistema...</span>
                                </div>
                                <div id="person_success" style="margin-bottom: 10px;">
                                    <span style="color: darkgreen;"><img src="{{ asset('HomeP/img/icon-success.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | Dados encontrados!</span>
                                </div>
                                <div id="person_error" style="margin-bottom: 10px;">
                                    <span style="color: darkred;"><img src="{{ asset('HomeP/img/icon-error.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | Dados não Encontrado!</span>
                                </div>

                                <div id="loading_inscribe" style="margin-bottom: 10px;">
                                    <span style="color: darkblue;"><img src="{{ asset('HomeP/img/loading.gif') }}" style="width: 30px;height: 30px;margin-bottom: 10px;"> | Verificando inscrição existente...</span>
                                </div>
                                <div id="inscribe_success" style="margin-bottom: 10px;">
                                    <span style="color: darkgreen;"><img src="{{ asset('HomeP/img/icon-success.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | Prossiga a inscrição!</span>
                                </div>
                                <div id="inscribe_error" style="margin-bottom: 10px;">
                                    <span style="color: darkred;"><img src="{{ asset('HomeP/img/icon-error.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | Matrícula já Inscrita, <a href="#">clique aqui</a>, para ver os dados!</span>
                                </div>
                            @endif
                            @if($field['name']['name']=='email')
                                <input id="email_hidden" type="text" value="" class="col-12 form-control" placeholder="Digite o E-mail...">
                                <div id="loading_email" style="margin-bottom: 10px;">
                                    <span style="color: darkblue;"><img src="{{ asset('HomeP/img/loading.gif') }}" style="width: 30px;height: 30px;margin-bottom: 10px;"> | Verificando inscrição existente...</span>
                                </div>
                                <div id="person_email_success" style="margin-bottom: 10px;">
                                    <span style="color: darkgreen;"><img src="{{ asset('HomeP/img/icon-success.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | Dados encontrados! | <a href="#" id="email_data_use">Usar Esses Dados?</a> | <a style="color: red !important;" href="#" id="email_data_clear" class="fa fa-trash"></a></span>
                                </div>
                                <div id="loading_email_inscribe" style="margin-bottom: 10px;">
                                    <span style="color: darkblue;"><img src="{{ asset('HomeP/img/loading.gif') }}" style="width: 30px;height: 30px;margin-bottom: 10px;"> | Verificando inscrição existente...</span>
                                </div>
                                <div id="inscribe_email_success" style="margin-bottom: 10px;">
                                    <span style="color: darkgreen;"><img src="{{ asset('HomeP/img/icon-success.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | Prossiga a inscrição!</span>
                                </div>
                                <div id="inscribe_email_error" style="margin-bottom: 10px;">
                                    <span style="color: darkred;"><img src="{{ asset('HomeP/img/icon-error.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | E-mail já inscrito, <a href="#">clique aqui</a>, para ver os dados!</span>
                                </div>
                            @endif
                            @if($field['name']['name']=='cpf')
                                <input id="cpf_hidden" type="text" value="" class="col-12 form-control" placeholder="Digite o CPF...">
                                <div id="loading_cpf" style="margin-bottom: 10px;">
                                    <span style="color: darkblue;"><img src="{{ asset('HomeP/img/loading.gif') }}" style="width: 30px;height: 30px;margin-bottom: 10px;"> | Verificando inscrição existente...</span>
                                </div>
                                <div id="person_cpf_success" style="margin-bottom: 10px;">
                                    <span style="color: darkgreen;"><img src="{{ asset('HomeP/img/icon-success.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | Dados encontrados! | <a href="#" id="cpf_data_use">Usar Esses Dados?</a> | <a style="color: red !important;" href="#" id="cpf_data_clear" class="fa fa-trash"></a></span>
                                </div>
                                <div id="loading_cpf_inscribe" style="margin-bottom: 10px;">
                                    <span style="color: darkblue;"><img src="{{ asset('HomeP/img/loading.gif') }}" style="width: 30px;height: 30px;margin-bottom: 10px;"> | Verificando inscrição existente...</span>
                                </div>
                                <div id="inscribe_cpf_success" style="margin-bottom: 10px;">
                                    <span style="color: darkgreen;"><img src="{{ asset('HomeP/img/icon-success.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | Prossiga a inscrição!</span>
                                </div>
                                <div id="inscribe_cpf_error" style="margin-bottom: 10px;">
                                <span style="color: darkred;"><img src="{{ asset('HomeP/img/icon-error.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | CPF já inscrito, <a href="{{route('Home.Main.search', ['id' => $model['id'], 'tag' => $model['tag']])}}">clique aqui</a>, para ver os dados!</span>
                                </div>
                                <div id="cpf_invalidated" style="margin-bottom: 10px;">
                                    <span style="color: darkred;"><img src="{{ asset('HomeP/img/icon-error.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | CPF inválido!</span>
                                </div>
                            @endif
                            @if($field['name']['name']=='birth')
                                <input id="birth_hidden" type="text" value="" class="col-12 form-control" placeholder="Data de Nascimento">
                            @endif
                            @if($field['name']['name']=='cell_phone')
                                <input id="cell_phone_hidden" type="text" value="" class="col-12 form-control" placeholder="(xx)xxxxx-xxxx">
                            @endif
                        </div>
                    @elseif($field['type']=='checkbox' && $field['name']['name']=='policy_terms')
                        <div class="form-group terms">
                            <label class="container_check">{{ $field['placeholder'] }}
                                <a id="terms" href="#" data-toggle="modal" data-target="#terms-txt">Termos de Serviço</a>
                                <input id="{{ $field['tag_id'] }}" type="checkbox" name="policy_terms"  class="{{ $field['required']==1 ? 'required' : '' }} policy_fadeout {{ $field['class'] }}" {{ $field['tags_extras'] }}>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    @elseif($field['type']=='checkbox' && $field['name']['name']=='update_account')
                        <div class="form-group terms">
                            <label class="container_check">{{ $field['placeholder'] }}
                                <input id="{{ $field['tag_id'] }}" type="checkbox" name="update_account" {{ $field['required']==1 ? 'checked' : '' }} class="{{ $field['required']==1 ? 'required' : '' }} {{ $field['class'] }}" {{ $field['tags_extras'] }}>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    @elseif($field['type']=='checkbox' && $field['name']['name']=='newsletter')
                        <div class="form-group terms">
                            <label class="container_check">{{ $field['placeholder'] }}
                                <input id="{{ $field['tag_id'] }}" type="checkbox" name="newsletter" {{ $field['required']==1 ? 'checked' : '' }} class="{{ $field['required']==1 ? 'required' : '' }} {{ $field['class'] }}" {{ $field['tags_extras'] }}>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    @elseif($field['type']=='checkbox' && ($field['name']['name']!='policy_terms' && $field['name']['name']!='update_account' && $field['name']['name']!='newsletter'))
                        <div class="form-group terms">
                            <label class="container_check">{{ $field['placeholder'] }}
                                <input id="{{ $field['tag_id'] }}" type="checkbox" name="{{ $field['name']['name'] }}" {{ $field['required']==1 ? 'required' : '' }} class="{{ $field['required']==1 ? 'required' : '' }}" {{ $field['tags_extras'] }}>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    @elseif($field['type']=='select' && $field['select_dinamic']!='static')
                        <div class="form-group">
                            <div class="styled-select clearfix" style="display:none">
                                {{-- VESTIBULAR --}}
                                <select id="{{ $field['tag_id'] }}" class="wide {{ $field['required']==1 ? 'required' : '' }} {{ $field['class'] }}" {{ $field['required']==1 ? 'required' : '' }} name="{{ $field['name']['name'] }}" {{ $field['tags_extras'] }}>
                                    <option value="" selected>{{ $field['placeholder'] }}</option>
                                    @if($field['select_dinamic']=='courses')
                                        @foreach($courses as $course)
                                            <option value="{{ $course['id'] }}" {{ $field['tags_extras'] }}>{{ $course['name'] }} | {{ $course['unity']['corporate_name'] }} | {{ $course['shift']['name'] }} | {{ $course['modality'] }} | {{ $course['category']['name'] }}</option>
                                        @endforeach
                                    @elseif($field['select_dinamic']=='courses')
                                        @foreach($courses as $course)
                                            <option value="{{ $course['id'] }}" {{ $field['tags_extras'] }}>{{ $course['name'] }} | {{ $course['unity']['corporate_name'] }} | {{ $course['shift']['name'] }} | {{ $course['modality'] }} | {{ $course['category']['name'] }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    @elseif($field['type']=='select' && $field['select_dinamic']=='static')
                        <div class="form-group">
                            <select id="{{ $field['tag_id'] }}" class="col-12 form-control {{ $field['required']==1 ? 'required' : '' }} {{ $field['class'] }}" name="{{ $field['name']['name'] }}" {{ $field['tags_extras'] }}>
                                <option value="" selected>{{ $field['placeholder'] }}</option>
                                @foreach($field['options'] as $option)
                                    <option value="{{ $option['value'] }}">{{ $option['title'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    @elseif($field['type']=='radio')
                        <div class="col-{{ $field['cols'] }}">
                            <div class="form-group radio_input">
                                <label class="container_radio">{{ $field['placeholder'] }}
                                    <input id="{{ $field['tag_id'] }}" type="{{ $field['type'] }}" name="{{ $field['name']['name'] }}" value="{{ $field['value'] }}" class="{{ $field['required']==1 ? 'required' : '' }}  {{ $field['class'] }}" {{ $field['required']==1 ? 'required' : '' }} {{ $field['tags_extras'] }}>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    @endif

                    <div class="loading_payment" style="margin-bottom: 10px;">
                        <span style="color: darkblue;"><img src="{{ asset('HomeP/img/loading.gif') }}" style="width: 30px;height: 30px;margin-bottom: 10px;"> | Gerando Boleto ...</span>
                    </div>

                @empty
                    <div class="form-group">
                        <span>Não existem campos cadastados para esta etapa!</span>
                    </div>
                @endforelse
            </div>
        @empty
            <div class="step submit">
                <h3 class="main_question"><strong>0/0</strong>Não existem etapas para este formulário!</h3>
            </div>
        @endforelse
    </div>
    <div id="bottom-wizard">
        <button type="button" id="backward" name="backward" class="backward"><i class="fa fa-backward"></i> Voltar</button>
        <button type="button" id="reset" class="forward_error">Resetar</button>
        <button type="button" name="forward" id="forward" class="forward">Avançar <i class="fa fa-forward"></i></button>
        <button type="button" id="forward_search" class="forward_error">Avançar <i class="fa fa-check"></i></button>
        <button type="submit" name="process" class="submit submit_finish">Confirmar</button>
    </div>
</form>

<script>
    $(document).ready(function(){
        if ("{{isset($model['type']['name'])}}") {
            if ("{{$model['type']['name']}}" == 'Vestibular') {
                forward_search.hide();
                forward.fadeIn();
                $('#term-link').click();
            }else if("{{$model['fixed_subscribers'] == "1"}}"){
                forward_search.hide();
                forward.fadeIn();
            }
        }
    });
</script>

<script>
    $('#cpf_ead').focusout(function () {
        if($(this).val() == '') return;
        forward.attr("disabled", true);
        $('.ead #inscribe_cpf_ead_success').hide();
        $('.ead #cpf_ead_invalidated').hide();
        $('.ead #loading_cpf_ead').show();
        $.ajax({
            url: "{{ route('Home.Main.isSubcribeFixed')}}",
            type:"post",
            data:{
                cpf: $(this).val(),
                event_id: $('input[name="event_id"]').val()
            }
        })
        .done(function (response) {
            let person = response != null ? response.data.compact.model : null;
            let inscribe = response != null ? response.data.compact.inscribe : null;
            if (person) {
                if (inscribe) {
                    $('.ead #inscribe_cpf_ead_error').show();
                    forward.attr("disabled", true);
                } else {
                    $('.ead #inscribe_cpf_ead_success').show();
                    forward.attr("disabled", false);
                    $('#first_name').val(person.name);
                    $('#email').val(person.email);
                    $('#birth').val(person.birth);
                }
            } else {
                $('.ead #cpf_ead_invalidated').show();
                forward.attr("disabled", true);
            }
        })
        .fail(function (fail) {
            //
        }).always(function () {
            $('.ead #loading_cpf_ead').hide();
        })
    });
</script>
