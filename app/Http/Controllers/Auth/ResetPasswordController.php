<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use App\Models\Configuration\Configurations;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected                     $redirectTo = '/Panel/Main';
    protected                     $configurations;
    protected                     $routeArray;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Configurations $configurations, Request $request)
    {
        $this->middleware('guest');
        $this->request =          $request;
        $this->configurations =   $configurations;
        $this->routeArray =       $this->configurations->getRouteName($request);
    }

    public function showResetForm(Request $request, $gets = null)
    {
        $token = explode('|||', $gets)[0];
        $email = explode('|||', $gets)[1];
        $title = 'Resetando a Senha';
        $route = ['auth','main','passwordReset'];
        $ipaddress = $this->configurations->ipClient();

        return view('auth.passwords.reset', compact('ipaddress', 'request', 'route', 'title', 'token', 'email'));
    }
}
