<?php

namespace App\Models\Home;

use Illuminate\Http\Request;
use Illuminate\Routing\Router;

class Main
{
    protected                        $request;
    protected                        $router;

    public function __construct
    (
        Request $request,
        Router $router
    )
    {
        $this->request = $request;
        $this->router = $router;
    }
}
