<?php


namespace App\Models\Configuration;

use SimpleXLSX;

class Functions
{

    public static function convertXlsToCsv($file)
    {
        $xlsx = new SimpleXLSX($file);

        $fp = fopen( 'file.csv', 'w');
        foreach($xlsx->rows() as $fields ) {
            fputcsv( $fp, $fields);
        }
        fclose($fp);
    }

    public static function importaCSV($file, $delimitador){

        $nameFile = $file['planilha']->getClientOriginalName(); //PEGANDO NOME COMPLETO DO ARQUIVO
        $ext = pathinfo($nameFile, PATHINFO_EXTENSION);         //PEGANDO EXTENSÃO DO ARQUIVO
        if (in_array($ext, ['xls', 'xlsx'])) {                  //SE A EXTENSÃO DO ARQUIVO FOR UMA DESSAS ELE É CONVERTIDO PARA CSV
            Functions::convertXlsToCsv($file['planilha']);
            $file = 'file.csv';
        } elseif ($ext == 'csv') {
            $file =  $file['planilha']->getRealPath();
        }

        $linhas = array();

        try{
            // Abrir arquivo para leitura
            $f = fopen($file, 'r');
            if ($f) {

                // Ler cabecalho do arquivo
                $headerFile = fgetcsv($f, 0, $delimitador);
                // $headerFile = array_map("utf8_encode", $headerFile);

                // Enquanto nao terminar o arquivo
                while (!feof($f)) {

                    // Ler uma linha do arquivo
                    $linha = fgetcsv($f, 0, $delimitador);
                    if (!$linha) {
                        continue;
                    }

                    $linha = array_map("utf8_encode", $linha);
                    array_push($linhas, array_combine($headerFile, $linha));
                }
                fclose($f);
                unlink("file.csv");
            }
            return $linhas;
        }
        catch (\Exception $e)
        {
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return "Erro na Importação";
        }
    }

    public static function exportCsv($array, $fileName){
        try {
            $fp = fopen($fileName, 'w');
            $header = array_keys($array[0]);
            fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
            fputcsv($fp, $header, '|');
            $rows = array_values($array);
            foreach($rows as $row) {
                fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
                fputcsv($fp, $row, '|');
            }
            fclose($fp);
            return true;
        } catch (\Exception $e) {
            if(env('APP_DEBUG')=='true')
            {
                dd($e);
            }
            return false;
        }
    }

    public static function removeMaskPhone($phone)
    {
        $phone = str_replace(['(', ')', '-',' '], '', $phone);
        $phone = array(
            'codeArea' => substr($phone, 0, 2),
            'phone'    => substr($phone, 2, 11)
        );
        return $phone;
    }

    public static function setMaskPhone($numberPhone, $codeArea = null)
    {
        if (isset($codeArea)) {
            if (strlen($numberPhone) == 9) {
                return '(' . $codeArea . ')' . substr($numberPhone,0, 5) . '-' . substr($numberPhone,5, 9);
            } else {
                return '(' . $codeArea . ')' . substr($numberPhone,0, 4) . '-' . substr($numberPhone,4, 8);
            }
        } else {
            if (strlen($numberPhone) == 11) {
                return '(' . substr($numberPhone,0, 2) . ')' . substr($numberPhone,2, 7) . '-' . substr(7, 11);
            } else {
                return '(' . substr($numberPhone,0, 2) . ')' . substr($numberPhone,2, 6) . '-' . substr(6, 10);
            }
        }
    }

    public static function removeMaskCpf($cpf)
    {
        return str_replace(['.', '-'], '', $cpf);
    }

    public static function formatDateBD($date)
    {
        $date = explode('/', $date);
        $date = date('Y-m-d', strtotime($date[2].'-'.$date[1].'-'.$date[0]));
        return $date;
    }

    public static function formatDateView($date)
    {
        $date = explode('-', $date);
        $date = date('d/m/Y', strtotime($date[2].'-'.$date[1].'-'.$date[0]));
        return $date;
    }
}
