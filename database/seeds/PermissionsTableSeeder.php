<?php

use App\Models\Configuration\Permissions;
use App\Models\Configuration\Roles;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        //----------------------------------------------------
        DB::table('permissions')->delete();
        //----------------------------------------------------

        $permissionPainel = Permissions::create([
            'name'                 =>'Panel.Main.index',
            'label'                =>'Pode Visualizar o Panel do Sistema',
            'group'                =>'Administrador',
            'system'               =>1,
            'default'              =>1,
            'create_user_id'       =>1
        ]);
        $permissionPainelConfig = Permissions::create([
            'name'                 =>'Panel.Main.update',
            'label'                =>'Pode Configurar dados do Sistema',
            'group'                =>'Administrador',
            'system'               =>1,
            'create_user_id'       =>1
        ]);
        $permissionHome = Permissions::create([
            'name'                 =>'Home.Main.index',
            'label'                =>'Pode Visualizar o Home do Sistema',
            'group'                =>'Público',
            'system'               =>1,
            'create_user_id'       =>1
        ]);
        $permissionPainel->roles()->sync(Roles::where('name', '!=', 'Administrator')->where('name', '!=', 'Development1')->get());
        $permissionHome->roles()->sync(Roles::where('name', '!=', 'Administrator')->where('name', '!=', 'Development1')->get());
        //----------------------------------------------------
        $permissions =
            [
                //Users--------------------------------
                [
                    'ambient'     =>'Panel',
                    'name'        =>'Users',
                    'group'       =>'Usuários',
                ],
                //Roles--------------------------------
                [
                    'ambient'     =>'Panel',
                    'name'        =>'Roles',
                    'group'       =>'Funções',
                ],
                //Permissions--------------------------
                [
                    'ambient'     =>'Panel',
                    'name'        =>'Permissions',
                    'group'       =>'Permissões',
                ],
                ////////////////////////////
            ];
        //----------------------------------------------------
        foreach ($permissions as $p)
        {
            $permissionAll = Permissions::create([
                'name'                 =>$p['ambient'].'.'.$p['name'].'.all',
                'label'                =>"Pode ver a lista de todos ".$p['name'].' do sistema',
                'group'                =>$p['group'],
                'system'               =>1,
                'create_user_id'       =>1
            ]);
            $permissionShow1 = Permissions::create([
                'name'                 =>$p['ambient'].'.'.$p['name'].'.index',
                'label'                =>"Pode Acessar Lista ou item de ".$p['name'],
                'group'                =>$p['group'],
                'system'               =>1,
                'create_user_id'       =>1
            ]);
            $permissionShow2 = Permissions::create([
                'name'                 =>$p['ambient'].'.'.$p['name'].'.create',
                'label'                =>"Pode Visualizar a Tela de Cadastro ".$p['name'],
                'group'                =>$p['group'],
                'system'               =>1,
                'create_user_id'       =>1
            ]);
            $permissionShow3 = Permissions::create([
                'name'                 =>$p['ambient'].'.'.$p['name'].'.edit',
                'label'                =>"Pode Visualizar a Tela de Editar ".$p['name'],
                'group'                =>$p['group'],
                'system'               =>1,
                'create_user_id'       =>1
            ]);
            $permissionShow4 = Permissions::create([
                'name'                 =>$p['ambient'].'.'.$p['name'].'.infor',
                'label'                =>"Pode Visualizar Informações ".$p['name'],
                'group'                =>$p['group'],
                'system'               =>1,
                'create_user_id'       =>1
            ]);
            $permissionShow5 = Permissions::create([
                'name'                 =>$p['ambient'].'.'.$p['name'].'.show',
                'label'                =>"Pode Visualizar Item único de ".$p['name'],
                'group'                =>$p['group'],
                'system'               =>1,
                'create_user_id'       =>1
            ]);
            $permissionShow6 = Permissions::create([
                'name'                 =>$p['ambient'].'.'.$p['name'].'.store',
                'label'                =>"Pode Cadastrar ".$p['name'],
                'group'                =>$p['group'],
                'system'               =>1,
                'create_user_id'       =>1
            ]);
            $permissionShow7 = Permissions::create([
                'name'                 =>$p['ambient'].'.'.$p['name'].'.update',
                'label'                =>"Pode Editar ".$p['name'],
                'group'                =>$p['group'],
                'system'               =>1,
                'create_user_id'       =>1
            ]);
            $permissionShow8 = Permissions::create([
                'name'                 =>$p['ambient'].'.'.$p['name'].'.delete',
                'label'                =>"Pode Deletar ".$p['name'],
                'group'                =>$p['group'],
                'system'               =>1,
                'create_user_id'       =>1
            ]);
            $permissionShow9 = Permissions::create([
                'name'                 =>$p['ambient'].'.'.$p['name'].'.trashed',
                'label'                =>"Pode ver lista de deletados de ".$p['name'],
                'group'                =>$p['group'],
                'system'               =>1,
                'create_user_id'       =>1
            ]);
            $permissionShow10 = Permissions::create([
                'name'                 =>$p['ambient'].'.'.$p['name'].'.restore',
                'label'                =>"Pode restaurar deletados de ".$p['name'],
                'group'                =>$p['group'],
                'system'               =>1,
                'create_user_id'       =>1
            ]);
        }
        //----------------------------------------------------
        Permissions::create([
            'name'                 =>'Panel.Inscribes.index',
            'label'                =>"Pode Visualizar o Panel das Inscrições",
            'group'                =>"Inscrições",
            'system'               =>1,
            'create_user_id'       =>1
        ]);
        //----------------------------------------------------
        Permissions::create([
            'name'                 =>'Panel.Inscribes.report',
            'label'                =>"Pode Visualizar o Relatório de Inscritos",
            'group'                =>"Inscrições",
            'system'               =>1,
            'create_user_id'       =>1
        ]);
    }
}
