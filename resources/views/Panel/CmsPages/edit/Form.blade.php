<section class="page-content container-fluid">
    <div class="row">
        <div class="col">
            <div class="card">
                <h5 class="card-header">Editando {{ $crudName }}</h5>
                <form class="form-horizontal" id="validate-form" method="POST" action="{{ route("$route[0].$route[1].update") }}" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    <input type="hidden" name="id" value="{{ $model->id }}">

                    <input type="hidden" name="logo1T" value="{{ $model->logo1 }}">
                    <input type="hidden" name="logo2T" value="{{ $model->logo2 }}">
                    <input type="hidden" name="logo3T" value="{{ $model->logo3 }}">
                    <input type="hidden" name="session_1_img_backgroundT" value="{{ $model->session_1_img_background }}">
                    <input type="hidden" name="session_2_img_rightT" value="{{ $model->session_2_img_right }}">

                    <div class="card-body">

                        <div class="form-body">
                            <div class="form-body">

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Nome</label>
                                    <div class="col-md-5">
                                        <input type="text" placeholder="Nome da Página" class="form-control {{ $errors->has('page_name') ? ' is-invalid' : '' }}" autofocus name="page_name" value="{{ $item->page_name }}">
                                        @if ($errors->has('page_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('page_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <hr class="dashed ">

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Logo 1 (200x200)</label>
                                    <div class="col-md-5">
                                        <input type="file"  class="form-control {{ $errors->has('logo1') ? ' is-invalid' : '' }}" name="logo1" style="height: 45px;" {{ $model->logo1==null || $model->logo1=='default' ? 'required' : '' }}>
                                        @if ($errors->has('logo1'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('logo1') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <hr class="dashed ">

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Logo 2 (200x200)</label>
                                    <div class="col-md-5">
                                        <input type="file"  class="form-control {{ $errors->has('logo2') ? ' is-invalid' : '' }}" name="logo2" style="height: 45px;">
                                        @if ($errors->has('logo2'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('logo2') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <hr class="dashed ">

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Logo 3 (200x200)</label>
                                    <div class="col-md-5">
                                        <input type="file"  class="form-control {{ $errors->has('logo3') ? ' is-invalid' : '' }}" name="logo3" style="height: 45px;">
                                        @if ($errors->has('logo3'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('logo3') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <hr class="dashed ">

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Facebook</label>
                                    <div class="col-md-5">
                                        <input type="text" placeholder="Facebook" class="form-control {{ $errors->has('facebook') ? ' is-invalid' : '' }}" name="facebook" value="{{ $item->facebook }}">
                                        @if ($errors->has('facebook'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('facebook') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Twitter</label>
                                    <div class="col-md-5">
                                        <input type="text" placeholder="Twitter" class="form-control {{ $errors->has('twitter') ? ' is-invalid' : '' }}" name="twitter" value="{{ $item->twitter }}">
                                        @if ($errors->has('twitter'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('twitter') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">google</label>
                                    <div class="col-md-5">
                                        <input type="text" placeholder="google" class="form-control {{ $errors->has('google') ? ' is-invalid' : '' }}" name="google" value="{{ $item->google }}">
                                        @if ($errors->has('google'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('google') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">linkedin</label>
                                    <div class="col-md-5">
                                        <input type="text" placeholder="linkedin" class="form-control {{ $errors->has('linkedin') ? ' is-invalid' : '' }}" name="linkedin" value="{{ $item->linkedin }}">
                                        @if ($errors->has('linkedin'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('linkedin') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">instagram</label>
                                    <div class="col-md-5">
                                        <input type="text" placeholder="instagram" class="form-control {{ $errors->has('instagram') ? ' is-invalid' : '' }}" name="instagram" value="{{ $item->instagram }}">
                                        @if ($errors->has('instagram'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('instagram') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <hr class="dashed ">

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Imagem de Fundo Sessão 1 (1400x800)</label>
                                    <div class="col-md-5">
                                        <input type="file"  class="form-control {{ $errors->has('session_1_img_background') ? ' is-invalid' : '' }}" name="session_1_img_background" style="height: 45px;" {{ $model->session_1_img_background==null || $model->session_1_img_background=='default' ? 'required' : '' }}>
                                        @if ($errors->has('session_1_img_background'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('session_1_img_background') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Título da sessão 1</label>
                                    <div class="col-md-5">
                                        <input type="text" placeholder="Título da sessão 1" class="form-control {{ $errors->has('session_1_title') ? ' is-invalid' : '' }}" name="session_1_title" value="{{ $item->session_1_title }}">
                                        @if ($errors->has('session_1_title'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('session_1_title') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Sub-Título da sessão 1</label>
                                    <div class="col-md-5">
                                        <input type="text" placeholder="Sub-Título da sessão 1" class="form-control {{ $errors->has('session_1_subtitle') ? ' is-invalid' : '' }}" name="session_1_subtitle" value="{{ $item->session_1_subtitle }}">
                                        @if ($errors->has('session_1_subtitle'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('session_1_subtitle') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <hr class="dashed ">

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Imagem Lateral Direita Sessão 2 (500x533)</label>
                                    <div class="col-md-5">
                                        <input type="file"  class="form-control {{ $errors->has('session_2_img_right') ? ' is-invalid' : '' }}" name="session_2_img_right" style="height: 45px;" {{ $model->session_2_img_right==null || $model->session_2_img_right=='default' ? 'required' : '' }}>
                                        @if ($errors->has('session_2_img_right'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('session_2_img_right') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Título da sessão 2</label>
                                    <div class="col-md-5">
                                        <input type="text" placeholder="Título da sessão 2" class="form-control {{ $errors->has('session_2_title') ? ' is-invalid' : '' }}" name="session_2_title" value="{{ $item->session_2_title }}">
                                        @if ($errors->has('session_2_title'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('session_2_title') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Conteúdo da sessão 2</label>
                                    <div class="col-md-5">
                                        <textarea placeholder="Conteúdo da sessão 2" class="form-control {{ $errors->has('session_2_content') ? ' is-invalid' : '' }} textarea" name="session_2_content">{{ $item->session_2_content }}</textarea>
                                        @if ($errors->has('session_2_content'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('session_2_content') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <hr class="dashed ">

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Título da sessão 3</label>
                                    <div class="col-md-5">
                                        <input type="text" placeholder="Título da sessão 3" class="form-control {{ $errors->has('session_3_title') ? ' is-invalid' : '' }}" name="session_3_title" value="{{ $item->session_3_title }}">
                                        @if ($errors->has('session_3_title'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('session_3_title') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Sub-Título da sessão 3</label>
                                    <div class="col-md-5">
                                        <input type="text" placeholder="Sub-Título da sessão 3" class="form-control {{ $errors->has('session_3_subtitle') ? ' is-invalid' : '' }}" name="session_3_subtitle" value="{{ $item->session_3_subtitle }}">
                                        @if ($errors->has('session_3_subtitle'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('session_3_subtitle') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="card-footer bg-light">
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="offset-sm-3 col-md-5">
                                            <button type="submit" class="btn btn-primary btn-rounded submit">Atualizar</button>
                                            <a href="{{ route("$route[0].$route[1].index") }}" class="btn btn-secondary clear-form btn-rounded btn-outline">Cancelar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
