<?php
namespace App\PagSeguro;

use Illuminate\Http\Request;
use App\Models\Configuration\Configurations;
use App\Models\Configuration\Errors;
use App\Models\Configuration\Functions;
use App\Models\Configuration\Inscribes;
use App\Models\Configuration\PaymentInscribes;
use DateTime;
use Illuminate\Support\Facades\Auth;
use PagSeguro\Library;
use PagSeguro\Domains\Requests\DirectPayment\Boleto;
use PagSeguro\Domains\Requests\DirectPayment\CreditCard;

class PagSeguro
{
    protected                          $configurations;
    protected                          $routeArray;
    protected                          $library;
    protected                          $boleto;
    protected                          $creditcard;

    public function __construct
    (
        Configurations                 $configurations,
        Request                        $request,
        Library                        $library,
        Boleto                         $boleto,
        CreditCard                     $creditcard
    )
    {
        $this->library =               $library;
        $this->library->initialize();
        $this->library->cmsVersion()->setName("Nome")->setRelease("1.0.0");
        $this->library->moduleVersion()->setName("Nome")->setRelease("1.0.0");
        $this->configurations =        $configurations;
        $this->routeArray =            $this->configurations->getRouteName($request);
        $this->boleto =                $boleto;
        $this->creditcard =            $creditcard;
    }

    /*
     *
     * Funções Principais
     *
     */


    //Mostrar lista ----------------------------------------------------------------------------------------------------
    public function getSession()
    {
        try
        {
            $sessionCode = \PagSeguro\Services\Session::create
            (
                \PagSeguro\Configuration\Configure::getAccountCredentials()
            );
            return $sessionCode->getResult();
        } catch (\Exception $e)
        {
            dd($e->getMessage());
        }
        return false;
    }
    //------------------------------------------------------------------------------------------------------------------

    public function clearRequest($request)
    {
        //formatando telefone
        $phone = $request['cell_phone'] ? Functions::removeMaskPhone($request['cell_phone']) : ['codeArea' => '21', 'phone' => '27654000'];

        //formatando cpf
        $cpf_1 = $request['cpf'] ? $request['cpf'] : '00000000000';
        $cpf_2 = str_replace(['.', '-'], '', $cpf_1);

        //formatando preço
        if(!isset($request['price'])){
            $price_1 = isset($request['professional_category'])  ? $request['professional_category'] : null;
            if($price_1  ==   null      ? $price_2  ='20.00'  : '');
            if($price_1  ==   'ag_20'   ? $price_2  ='20.00' : '');
            if($price_1  ==   'apl_20'  ? $price_2  ='30.00' : '');
            if($price_1  ==   'aps_50'  ? $price_2  ='50.00' : '');
            if($price_1  ==   'pp_60'   ? $price_2  ='60.00' : '');
        }else{
            $price_2 = $request['price'];
        }

        $request['cep'] = isset($request['cep']) ? str_replace('-', '',  $request['cep']) : '26260045';

        //formatando endereço
        $address_2 =
            [
                'street'          => $request['street'] ?? 'Av. Abílio Augusto Távora',
                'number'          => $request['number'] ?? '2134',
                'district'        => $request['district'] ?? 'Dom Rodrigo',
                'postalCode'      => $request['cep'],
                'city'            => $request['city'] ?? 'Nova Iguaçu',
                'state'           => $request['state'] ?? 'RJ',
            ];


        return
            [
                'ddd'         => $phone['codeArea'],
                'phone'       => $phone['phone'],
                'cpf'         => $cpf_2,
                'price'       => $price_2,
                'address_2'   => $address_2,
            ];
    }


    // Pagar com boleto ------------------------------------------------------------------------------------------------
    public function boleto(Request $request)
    {

        try {

            if (env('PAGSEGURO_ENV') == 'sandbox') {
                $boleto = $this->boletoSandbox($request);
            } else {
                $boleto = $this->boletoProduction($request);
            }

            return $boleto;

        } catch (\Exception $e) {

            if(env('APP_DEBUG')=='true')
            {
                dd($e);
            }

            return $this->boletoError($e->getMessage());
        }
    }
    //------------------------------------------------------------------------------------------------------------------

    public function boletoSandbox(Request $request){
        $returnRequest = $this->clearRequest($request->all());

        $email  = 'emailuser@sandbox.pagseguro.com.br';
        $token  = env('PAGSEGURO_TOKEN_SANDBOX');
        $url    = 'https://ws.sandbox.pagseguro.uol.com.br/v2/transactions';
        $name = $request['first_name'];
        $unity = $request['unity'] == 'IT' ? 'ITAPERUNA' : 'NOVA IGUAÇU';
        $instructions = 'Curso: ' . $request['course'] . ' - Campus: ' . $unity;

        $data['paymentMode'] = 'default';
        $data['paymentMethod'] = 'boleto';
        $data['receiverEmail'] = env('PAGSEGURO_EMAIL');
        $data['email'] = env('PAGSEGURO_EMAIL');
        $data['token'] = $token;
        $data['currency'] = 'BRL';
        $data['extraAmount'] = '0.00';
        $data['itemId1'] = $request['event_id'];
        $data['itemDescription1'] = $request['event_name'];
        $data['itemAmount1'] = $returnRequest['price'];
        $data['itemQuantity1'] = '1';
        $data['reference'] = $request['inscribe_id'];
        $data['instructions'] = $instructions;
        $data['senderName'] = $name;
        $data['senderCPF'] = $returnRequest['cpf'];
        $data['senderAreaCode'] = $returnRequest['ddd'];
        $data['senderPhone'] = $returnRequest['phone'];
        $data['senderEmail'] = $email;
        $data['senderHash'] = $request['hashInput'];
        $data['shippingAddressStreet'] = $returnRequest['address_2']['street'];
        $data['shippingAddressNumber'] = $returnRequest['address_2']['number'];
        $data['shippingAddressDistrict'] = $returnRequest['address_2']['district'];
        $data['shippingAddressPostalCode'] = $returnRequest['address_2']['postalCode'];
        $data['shippingAddressCity'] = $returnRequest['address_2']['city'];
        $data['shippingAddressState'] = $returnRequest['address_2']['state'];
        $data['shippingAddressCountry'] = 'BRA';
        $data['shippingType'] = '3';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, Array('Content-Type: application/x-www-form-urlencoded; charset=ISO-8859-1'));
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $resp = curl_exec($curl);
        curl_close($curl);
        if ($resp == 'Unauthorized') {
          // JOGA ERRO DE Unauthorized
        } else {
            $boleto = simplexml_load_string($resp);
        }

        if (isset($boleto->error)) {
            return $this->boletoError($boleto->error);
        }

        return $boleto;
    }

    public function boletoProduction(Request $request){
        $returnRequest = $this->clearRequest($request->all());

        $email  = $request['email'];
        $url    = 'https://ws.pagseguro.uol.com.br/recurring-payment/boletos?email='.env('PAGSEGURO_EMAIL').'&token='.env('PAGSEGURO_TOKEN_PRODUCTION');

        $unity = $request['unity'] == 'IT' ? 'ITAPERUNA' : 'NOVA IGUAÇU';
        $instructions = 'Curso: ' . $request['course'] . ' - Campus: ' . $unity;
        $vencimento = new DateTime('+1 day');
        $vencimento = $vencimento->format('Y-m-d');

        $data["reference"] = $request['inscribe_id'];
        $data["firstDueDate"] = $vencimento;
        $data["numberOfPayments"] = "1";
        $data["periodicity"] = "monthly";
        $data["amount"] = $returnRequest['price'];
        $data["instructions"] = $instructions;
        $data["description"] = $request['event_name'];
        $data["customer"] = array(
        "document" => array(
        "type" => 'CPF',
        "value" => $returnRequest['cpf']
        ),
        "name" => $request['first_name'],
        "email" => $email,
        "phone" => array(
            "areaCode" => $returnRequest['ddd'],
            "number" => $returnRequest['phone']
        ),
        "address" => array(
            "postalCode" => $returnRequest['address_2']['postalCode'],
            "street" => $returnRequest['address_2']['street'],
            "number" => $returnRequest['address_2']['number'],
            "district" => $returnRequest['address_2']['district'],
            "city" => $returnRequest['address_2']['city'],
            "state" => $returnRequest['address_2']['state']
        )
        );

        $postdata = json_encode($data);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json;charset=ISO-8859-1',
            'Accept: application/json;charset=ISO-8859-1'
        ));

        $boleto = curl_exec($ch);
        curl_close($ch);

        $boleto = json_decode($boleto);

        if (isset($boleto->errors)) {
            return $this->boletoError($boleto->errors[0]->message);
        }

        return $boleto->boletos[0];
    }

    public function boletoError($error){

        $user = Auth::user();
        Errors::create([
            'user_id'   => $user ? $user->id : 1,
            'title'     => 'Erro no PagSeguro',
            'code'      => '500',
            'content'   => $error,
            'status'    => 1
        ]);

        return 'error';
    }

    public function notification(Request $request)
    {
        $code = str_replace('-', '', $request['notificationCode']);
        $email = env('PAGSEGURO_EMAIL');
        if(env('PAGSEGURO_ENV')=='sandbox')
        {
            $token = env('PAGSEGURO_TOKEN_SANDBOX');
            $url = 'https://ws.sandbox.pagseguro.uol.com.br/v2/transactions/notifications';
        }
        else
        {
            $token = env('PAGSEGURO_TOKEN_PRODUCTION');
            $url = 'https://ws.pagseguro.uol.com.br/v2/transactions/notifications';
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "$url/$code?email=$email&token=$token");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_ENCODING, "");
        curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
            'token' => $token,
            'email' => $email,
        )));
        $response =    curl_exec($curl);
        curl_close($curl);
        $xml = simplexml_load_string($response);

        if(isset($xml) && $xml->status==3)
        {
            $payment = PaymentInscribes::where('code', $xml->code)->get()->first();
            $payment->status = 3;
            $payment->save();
            $inscribe = Inscribes::find($payment->inscribe_id);
            $inscribe->status =  3;
            $inscribe->save();
        }

    }
}
