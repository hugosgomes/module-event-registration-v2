<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Tabela de {{ $crudName }}</h3>
                </div>
                <div class="card-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th style="width: 5%;">#</th>
                            <th style="width: 20%;">Name</th>
                            <th style="width: 20%;">Descrição</th>
                            <th style="width: 15%;">Ação</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($model as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->label }}</td>
                                <td>
                                    @if(isset($route[0]) && isset($route[1]))

                                        @can("$route[0].$route[1].edit")
                                            <a href="{{ route("$route[0].$route[1].edit", $item->id) }}" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                                        @endcan

                                        @can("$route[0].$route[1].delete")
                                            <a href="#" id="{{ $item->id }}" data-toggle="modal" data-target="#modal-delete{{ $item->id }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                            <div class="modal fade" id="modal-delete{{ $item->id }}">
                                                <div class="modal-dialog">
                                                    <div class="modal-content bg-danger">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Exclusão</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Item de ID: {{ $item->id }}, será excluído!</p>
                                                            <p>Tem certeza que deseja excluir?</p>
                                                        </div>
                                                        <form method="get" action="{{ route("$route[0].$route[1].delete", $item->id) }}">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="id" value="{{ $item->id }}"/>
                                                            <div class="modal-footer justify-content-between">
                                                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancelar</button>
                                                                <button type="submit" class="btn btn-outline-light">Excluir</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        @endcan

                                        @includeIf("$route[0].Template.Modals.crud_user", ['item'=>$item])

                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th style="width: 5%;">#</th>
                            <th style="width: 20%;">Name</th>
                            <th style="width: 20%;">Descrição</th>
                            <th style="width: 15%;">Ação</th>
                        </tr>
                        </tfoot>
                    </table>


                </div>
            </div>
        </div>
    </div>

    @can("$route[0].$route[1].trashed")
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Lixeira de {{ $crudName }}</h3>
                    </div>
                    <div class="card-body">
                        <table id="example3" class="table table-bordered table-hover table-danger">
                            <thead>
                            <tr>
                                <th style="width: 5%;">#</th>
                                <th style="width: 20%;">Name</th>
                                <th style="width: 20%;">Descrição</th>
                                <th style="width: 15%;">Ação</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($trashed as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->label }}</td>
                                    <td>
                                        @if(isset($route[0]) && isset($route[1]))

                                            @can("$route[0].$route[1].restore")
                                                <a href="#" id="{{ $item->id }}" data-toggle="modal" data-target="#modal-restore{{ $item->id }}" class="btn btn-success"><i class="fa fa-trash-restore"></i></a>
                                                <div class="modal fade" id="modal-restore{{ $item->id }}">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content bg-success">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Restauração</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>Item de ID: {{ $item->id }}, será restaurado!</p>
                                                                <p>Tem certeza que deseja restaurar?</p>
                                                            </div>
                                                            <form method="get" action="{{ route("$route[0].$route[1].restore", $item->id) }}">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="id" value="{{ $item->id }}"/>
                                                                <div class="modal-footer justify-content-between">
                                                                    <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancelar</button>
                                                                    <button type="submit" class="btn btn-outline-light">Restaurar</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endcan

                                            @includeIf("$route[0].Template.Modals.crud_user", ['item'=>$item])

                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th style="width: 5%;">#</th>
                                <th style="width: 20%;">Name</th>
                                <th style="width: 20%;">Descrição</th>
                                <th style="width: 15%;">Ação</th>
                            </tr>
                            </tfoot>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    @endcan

</section>
