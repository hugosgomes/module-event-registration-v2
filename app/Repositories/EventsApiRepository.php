<?php

namespace App\Repositories;

use App\APIIntranet\Models\ModelBase;
use App\Models\Configuration\Configurations;
use Illuminate\Http\Request;

class EventsApiRepository
{
    //Variáveis Globais

    protected                     $request;
    protected                     $routeArray;
    protected                     $modelsIntranet;
    protected                     $configurations;
    protected                     $crudName;

    //Funções Padrões

    public function __construct
    (
        Request                   $request,
        Configurations            $configurations,
        ModelBase                 $modelsIntranet
    )
    {
        $this->request          = $request;
        $this->configurations   = $configurations;
        $this->modelsIntranet   = $modelsIntranet;
        $this->routeArray       = $this->configurations->getRouteName($request);
        $this->crudName         = 'Inscritos';
    }


    //Main Functions

    public function index(Request $request){
        $events = $this->modelsIntranet->index($request,'Events');
        if(!$events || $events['status'] == 'error' || $events['data'] == []){
            return env('APP_DEBUG')=='true' ? dd($events) : null;
        }
        return $events['data'];
    }

    public function show(Request $request, $id){
        $event = $this->modelsIntranet->show($request,'Events', $id);
        if(!$event || $event['status'] == 'error' || $event['data'] == []){
            return env('APP_DEBUG')=='true' ? dd($event) : null;
        }
        return $event['data'];
    }

    public function isSubcribeFixed(Request $request){
        $event = $this->modelsIntranet->consumeApi('get', 'Events', 'isSubcribeFixed', $request, null);
        if(!$event || $event['status'] == 'error' || $event['data'] == []){
            return !env('APP_DEBUG') ? dd($event) : null;
        }
        return $event['data'];
    }

}
