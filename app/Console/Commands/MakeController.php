<?php

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;

class MakeController extends GeneratorCommand
{
    /**
     * O nome e a assinatura do comando do console.
     *
     * @var string
     */
    protected $name = 'make:controller-crud';

    /**
     * A descrição do comando do console.
     *
     * @var string
     */
    protected $description = 'Create a new Controller CRUD class';

    /**
     * O tipo de classe sendo gerada.
     *
     * @var string
     */
    protected $type = 'Controller';

    /**
     * Substitui o nome da classe para o stub fornecido.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $name = $this->argument('name');
        if (strpos($name, '/') != false)
        {
            $array_path         = explode('/', $name);
            $className          = array_pop($array_path);
            $dependenceService  = implode(DIRECTORY_SEPARATOR, $array_path);
            $dependenceService  = $dependenceService . DIRECTORY_SEPARATOR . 'Service' . str_replace('Controller', '', $className);
            $array_dependence   = explode(DIRECTORY_SEPARATOR, $dependenceService);
            $nameService        = array_pop($array_dependence);
        }
        else
        {
            $className = $name;
            $dependenceService = 'Service' . str_replace('Controller', '', $name);
            $nameService = $dependenceService;
        }
        $stub = parent::replaceClass($stub, $className);
        return str_replace(['ClassName', 'DependenceService', 'NameService'], [$className, $dependenceService, $nameService], $stub);
    }
    /**
     * Obtém o arquivo stub para o gerador.
     *
     * @return string
     */
    protected function getStub()
    {
        return  app_path() . '/Console/Commands/Stubs/Controller/make-controller.stub';
    }
    /**
     * Obtém o namespace padrão para a classe.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Http\Controllers';
    }

    /**
     * Obtém os argumentos do comando do console.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the controller.'],
        ];
    }

}
