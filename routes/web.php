<?php

Route::group(['namespace'=>'Home'], function (){

    //Main
    Route::get('/', 'Main\MainController@index')->name('Home.Main.index');
    Route::get('/evento/{id?}/{tag?}', 'Main\MainController@show')->name('Home.Main.show');
    Route::post('/evento/{id?}/{tag?}', 'Main\MainController@show')->name('Home.Main.show');
    Route::post('/store/', 'Main\MainController@store')->name('Home.Main.store');
    Route::get('/evento/search/{id?}/{tag?}', 'Main\MainController@search')->name('Home.Main.search');
    Route::post('/evento/search/{id?}/{tag?}', 'Main\MainController@search')->name('Home.Main.search');
    Route::post('/home/isSubcribeFixed', 'Main\MainController@isSubcribeFixed')->name('Home.Main.isSubcribeFixed');

    //Validações javascript
    Route::get('/validationEmailInscribes/{email?}/{event_id?}', 'Main\MainController@validationEmailInscribes')
        ->name('Home.Main.validationEmailInscribes');
    Route::get('/validationCPFInscribes/{cpf?}/{event_id?}', 'Main\MainController@validationCPFInscribes')
        ->name('Home.Main.validationCPFInscribes');
    Route::get('/validationNumberRegistrationInscribes/{number_registration?}/{event_id?}', 'Main\MainController@validationNumberRegistrationInscribes')
        ->name('Home.Main.validationNumberRegistrationInscribes');
});

//PagSeguro
Route::post('Home/PagSeguro/notification', 'PagSeguroController@notification')->name('Home.PagSeguro.notification');
Route::post('Home/PagSeguro/boleto', 'PagSeguroController@boleto')->name('Home.PagSeguro.boleto');

Auth::routes();

View::composer('*', function($view)
{
    $view
        ->with('users_temporary', \App\User::orderBy('id', 'DESC')->whereHas('roles', function ($query){$query->where('name', 'Temporary');})->get())
        ->with('variable', 'content');
});

Route::namespace('Panel')->prefix('Panel')->group(function (){

    //Rota Panel
    Route::get('/Main', 'Main\MainController@index')->name('Panel.Main.index');

    //Rota Errors
    Route::get('/Errors/fixed', 'Errors\ErrorsController@fixed')->name('Panel.Errors.fixed');
    Route::get('/Errors/index', 'Errors\ErrorsController@index')->name('Panel.Errors.index');
    Route::get('/Errors/show/{id?}', 'Errors\ErrorsController@show')->name('Panel.Errors.show');
    //------
    Route::get('/Errors/create', 'Errors\ErrorsController@create')->name('Panel.Errors.create');
    Route::get('/Errors/edit/{id?}', 'Errors\ErrorsController@edit')->name('Panel.Errors.edit');
    //------
    Route::post('/Errors/store', 'Errors\ErrorsController@store')->name('Panel.Errors.store');
    Route::post('/Errors/update', 'Errors\ErrorsController@update')->name('Panel.Errors.update');
    Route::get('/Errors/delete', 'Errors\ErrorsController@delete')->name('Panel.Errors.delete');
    //------
    Route::get('/Errors/trashed', 'Errors\ErrorsController@trashed')->name('Panel.Errors.trashed');
    Route::get('/Errors/restore', 'Errors\ErrorsController@restore')->name('Panel.Errors.restore');

    //Users
    Route::get('/Users/index', 'Users\UsersController@index')->name('Panel.Users.index');
    //Route::get('/Users/show/{id?}', 'Users\UsersController@show')->name('Panel.Users.show');
    //------
    Route::get('/Users/create', 'Users\UsersController@create')->name('Panel.Users.create');
    Route::get('/Users/edit/{id?}', 'Users\UsersController@edit')->name('Panel.Users.edit');
    //------
    Route::post('/Users/store', 'Users\UsersController@store')->name('Panel.Users.store');
    Route::post('/Users/update', 'Users\UsersController@update')->name('Panel.Users.update');
    Route::get('/Users/delete', 'Users\UsersController@delete')->name('Panel.Users.delete');
    //------
    // Route::get('/Users/trashed', 'Users\UsersController@trashed')->name('Panel.Users.trashed');
    Route::get('/Users/restore', 'Users\UsersController@restore')->name('Panel.Users.restore');

    //Roles
    Route::get('/Roles/infor', 'Roles\RolesController@infor')->name('Panel.Roles.infor');
    Route::get('/Roles/index', 'Roles\RolesController@index')->name('Panel.Roles.index');
    Route::get('/Roles/show/{id?}', 'Roles\RolesController@show')->name('Panel.Roles.show');
    //------
    Route::get('/Roles/create', 'Roles\RolesController@create')->name('Panel.Roles.create');
    Route::get('/Roles/edit/{id?}', 'Roles\RolesController@edit')->name('Panel.Roles.edit');
    //------
    Route::post('/Roles/store', 'Roles\RolesController@store')->name('Panel.Roles.store');
    Route::post('/Roles/update', 'Roles\RolesController@update')->name('Panel.Roles.update');
    Route::get('/Roles/delete', 'Roles\RolesController@delete')->name('Panel.Roles.delete');
    //------
    Route::get('/Roles/trashed', 'Roles\RolesController@trashed')->name('Panel.Roles.trashed');
    Route::get('/Roles/restore', 'Roles\RolesController@restore')->name('Panel.Roles.restore');

    //Permissions
    Route::get('/Permissions/infor', 'Permissions\PermissionsController@infor')->name('Panel.Permissions.infor');
    Route::get('/Permissions/index', 'Permissions\PermissionsController@index')->name('Panel.Permissions.index');
    Route::get('/Permissions/show/{id?}', 'Permissions\PermissionsController@show')->name('Panel.Permissions.show');
    //------
    Route::get('/Permissions/create', 'Permissions\PermissionsController@create')->name('Panel.Permissions.create');
    Route::get('/Permissions/edit/{id?}', 'Permissions\PermissionsController@edit')->name('Panel.Permissions.edit');
    //------
    Route::post('/Permissions/store', 'Permissions\PermissionsController@store')->name('Panel.Permissions.store');
    Route::post('/Permissions', 'Permissions\PermissionsController@update')->name('Panel.Permissions.update');
    Route::get('/Permissions/delete', 'Permissions\PermissionsController@delete')->name('Panel.Permissions.delete');
    //------
    Route::get('/Permissions/trashed', 'Permissions\PermissionsController@trashed')->name('Panel.Permissions.trashed');
    Route::get('/Permissions/restore', 'Permissions\PermissionsController@restore')->name('Panel.Permissions.restore');

    //CmsPages
    Route::get('/CmsPages/infor', 'CmsPages\CmsPagesController@infor')->name('Panel.CmsPages.infor');
    Route::get('/CmsPages/index', 'CmsPages\CmsPagesController@index')->name('Panel.CmsPages.index');
    Route::get('/CmsPages/show/{id?}', 'CmsPages\CmsPagesController@show')->name('Panel.CmsPages.show');
    //------
    Route::get('/CmsPages/create', 'CmsPages\CmsPagesController@create')->name('Panel.CmsPages.create');
    Route::get('/CmsPages/edit/{id?}', 'CmsPages\CmsPagesController@edit')->name('Panel.CmsPages.edit');
    //------
    Route::post('/CmsPages/store', 'CmsPages\CmsPagesController@store')->name('Panel.CmsPages.store');
    Route::post('/CmsPages/update', 'CmsPages\CmsPagesController@update')->name('Panel.CmsPages.update');
    Route::get('/CmsPages/delete', 'CmsPages\CmsPagesController@delete')->name('Panel.CmsPages.delete');
    //------
    Route::get('/CmsPages/trashed', 'CmsPages\CmsPagesController@trashed')->name('Panel.CmsPages.trashed');
    Route::get('/CmsPages/restore', 'CmsPages\CmsPagesController@restore')->name('Panel.CmsPages.restore');

    //CmsPages
    Route::get('/Inscribes/index', 'Inscribes\InscribesController@index')->name('Panel.Inscribes.index');
    Route::get('/Inscribes/report', 'Inscribes\InscribesController@report')->name('Panel.Inscribes.report');
    //------
    Route::get('/Inscribes/edit/{id?}', 'Inscribes\InscribesController@edit')->name('Panel.Inscribes.edit');
    //------
    Route::post('/Inscribes/update', 'Inscribes\InscribesController@update')->name('Panel.Inscribes.update');
    Route::get('/Inscribes/delete', 'Inscribes\InscribesController@delete')->name('Panel.Inscribes.delete');
    //------
    Route::get('/Inscribes/trashed', 'Inscribes\InscribesController@trashed')->name('Panel.Inscribes.trashed');
    Route::get('/Inscribes/restore', 'Inscribes\InscribesController@restore')->name('Panel.Inscribes.restore');
    Route::post('/Inscribes/import', 'Inscribes\InscribesController@import')->name('Panel.Inscribes.import');
    Route::get('/Inscribes/export', 'Inscribes\InscribesController@export')->name('Panel.Inscribes.export');

    //PaymentInscribes
    Route::post('/PaymentInscribes/update', 'PaymentInscribes\PaymentInscribesController@update')->name('Panel.PaymentInscribes.update');

});
