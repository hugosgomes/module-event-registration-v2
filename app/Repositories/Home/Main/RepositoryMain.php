<?php

    namespace App\Repositories\Home\Main;

use App\APIIntranet\Models\ModelBase;
use App\Models\Configuration\CmsPages;
use App\Models\Configuration\Configurations;
use App\Models\Configuration\Functions;
use App\Models\Configuration\Inscribes;
use App\Models\Configuration\PaymentInscribes;
use App\Models\Home\Main;
use App\PagSeguro\PagSeguro;
use App\Repositories\EventsApiRepository;
use Illuminate\Http\Request;


    class RepositoryMain
    {
        //Variáveis Globais

        protected                     $request;
        protected                     $model;
        protected                     $routeArray;
        protected                     $modelsIntranet;
        protected                     $pagseguro;
        protected                     $eventsApiRepository;

        //Funções Padrões

        public function __construct
        (
            Request                   $request,
            Main                      $model,
            Configurations            $configurations,
            ModelBase                 $modelsIntranet,
            PagSeguro                 $pagseguro,
            EventsApiRepository       $EventsApiRepository
        )
        {
            $this->request              = $request;
            $this->model                = $model;
            $this->configurations       = $configurations;
            $this->routeArray           = $this->configurations->getRouteName($request);
            $this->modelsIntranet       = $modelsIntranet;
            $this->pagseguro            = $pagseguro;
            $this->crudName             = 'Principal do Portal';
            $this->eventsApiRepository  = $EventsApiRepository;
        }

        //Main Functions

        public function index(Request $request)
        {
            $model = $this->modelsIntranet->index($request,'Events');
            $error = $this->modelsIntranet->errors($model);
            if($error==false)
                return
                    [
                        'route'       =>
                            [
                                'ambient'    => $this->routeArray[0],
                                'crud'       => $this->routeArray[1],
                                'function'   => $this->routeArray[2],
                            ],
                        'compact'     =>
                            [
                                'title'      => $this->crudName,
                                'route'      => $this->routeArray,
                                'crudName'   => $this->crudName,
                                'model'      => $model['data'],
                                'cms_page'   => CmsPages::find(1),
                            ],
                        'request'            => $request,
                    ];
            else
                return $error;
        }

        public function show(Request $request)
        {
            $request->session()->put('tokenSession', $pagSeguro = $this->pagseguro->getSession());
            $model = $this->modelsIntranet->show($request,'Events', $request['id']);
            $error = $this->modelsIntranet->errors($model);
            if($error==false)
            {
                if($model['data']['forms']==[])
                {
                    $resp = 'no-form';
                }
                elseif($model['data']['forms']!=[] && $model['data']['forms'][0]['steps']==[])
                {
                    $resp = 'no-steps';
                }
                elseif($model['data']['forms']!=[] && $model['data']['forms'][0]['steps']!=[])
                {
                    $resp = $model['data']['forms'][0]['steps'];
                }

                return
                    [
                        'route'       =>
                            [
                                'ambient'        => $this->routeArray[0],
                                'crud'           => $this->routeArray[1],
                                'function'       => $this->routeArray[2],
                            ],
                        'compact'     =>
                            [
                                'title'          => "Inscrição para " . $model['data']['name'],
                                'route'          => $this->routeArray,
                                'crudName'       => "Inscrição para " . $model['data']['name'],
                                'model'          => $model['data'],
                                'cms_page'       => CmsPages::find(1),
                                'steps'          => $resp,
                                'courses'        => $this->modelsIntranet->index($request, "Courses")['data'],
                                'request'        => $request,
                                'tokenSession'   => $request->session()->get('tokenSession'),
                            ],
                        'request'                => $request,
                    ];
            }
            else
                return $error;
        }

        public function create(Request $request)
        {
            $model = $this->model;
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $model['status']=='success' ? $this->routeArray[1] : 'Errors',
                            'function'   => $model['status']=='success' ? $this->routeArray[2] : 'show',
                        ],
                    'compact'     =>
                        [
                            'title'      => 'Criando '.$this->crudName,
                            'route'      => $this->routeArray,
                            'crudName'   => 'Criando '.$this->crudName,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }

        public function store(Request $request)
        {
            $payment = null;
            $model = $this->modelsIntranet->show($request,'Events', $request['event_id']);
            $request['start'] = $model['data']['formatted_start'];
            if(isset($request['cpf_ead'])) $request['cpf'] = $request['cpf_ead'];
            $inscribe = $this->storeInscribe($request);
            if($model['data']['paid_out'] == 1) $payment = $this->storePayment($inscribe, $request);
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => 'Salvando '.$this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                            'boleto'     => $payment,
                            'redirect'   => env('APP_URL').'/'.'evento/'.$model['data']['id'].'/'.$model['data']['tag']
                        ],
                    'request'            => $request,
                ];
        }

        public function storeInscribe($request)
        {
            $inscribe = '';
            $this->clearRequest($request);
            $return = $this->modelsIntranet->store($request,'Person');

            if(!$return || $return['status'] == 'error' || $return['data'] == []){
                return env('APP_DEBUG')=='true' ? dd($return) : null;
            }

            if($return['data']['registration']!=[])
            {
                $valInscribeExist = Inscribes::where('registration_id', $return['data']['registration']['id'])
                    ->where('event_id', $request['event_id'])
                    ->get()->first();
                if($valInscribeExist==null)
                {
                    $inscribe = Inscribes::create(['registration_id'=>$return['data']['registration']['id'], 'event_id'=>$request['event_id'], 'status'=>1]);
                }
                else
                {
                    $inscribe = $valInscribeExist;
                }
            }

            if($return['data']['person']!=[])
            {
                $valInscribeExist = Inscribes::where('person_id', $return['data']['person']['id'])
                    ->where('event_id', $request['event_id'])
                    ->get()->first();
                if($valInscribeExist==null)
                {
                    $inscribe = Inscribes::create([
                        'person_id' => $return['data']['person']['id'],
                        'event_id'  => $request['event_id'],
                        'unity'     => $request['unity'],
                        'course'    => $request['course'],
                        'status'    => 1
                    ]);
                }
                else
                {
                    $inscribe = $valInscribeExist;
                }
            }

            return $inscribe;
        }

        public function storePayment($inscribe, $request){
            $request = $this->clearRequest($request);
            $valPayment =  PaymentInscribes::where('inscribe_id', $inscribe->id)->get()->first();
            if($valPayment==null)
            {
                $request['inscribe_id'] = $inscribe->id;
                $boleto = $this->pagseguro->boleto($request);
                if ($boleto != 'error') {
                    $code = (string)$boleto->code;
                    $link = (string)$boleto->paymentLink;
                    //-----------------------------------------------------------------------
                    return PaymentInscribes::create([
                        'inscribe_id'      => $inscribe->id,
                        'type'             => 'boleto',
                        'name'             => $request['event_name'],
                        'value'            => $request['price'],
                        'inscribe_date'    => $inscribe->created_at,
                        'expiry_date'      => now()->addDays(3),
                        'link_boleto'      => $link,
                        'code'             => $code,
                        'status'           => 1,
                        'email_confirmed'  => 0
                    ]);
                }else{
                    return $boleto;
                }
            }
            else
            {
                return $valPayment;
            }
        }

        public function edit(Request $request)
        {
            $model = $this->model;
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $model['status']=='success' ? $this->routeArray[1] : 'Errors',
                            'function'   => $model['status']=='success' ? $this->routeArray[2] : 'show',
                        ],
                    'compact'     =>
                        [
                            'title'      => 'Editando '.$this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                            'item'       => $model,
                            'crudName'   => 'Editando Item de ID :',
                        ],
                    'request'            => $request,
                ];
        }

        public function update(Request $request)
        {
            $model = $this->model;
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $model['status']=='success' ? $this->routeArray[1] : 'Errors',
                            'function'   => $model['status']=='success' ? $this->routeArray[2] : 'show',
                        ],
                    'compact'     =>
                        [
                            'title'      => 'Atualiando '.$this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }

        public function delete(Request $request)
        {
            $model = $this->model;
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $model['status']=='success' ? $this->routeArray[1] : 'Errors',
                            'function'   => $model['status']=='success' ? $this->routeArray[2] : 'show',
                        ],
                    'compact'     =>
                        [
                            'title'      => 'Deletando '.$this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }

        public function restore(Request $request)
        {
            $model = $this->model;
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $model['status']=='success' ? $this->routeArray[1] : 'Errors',
                            'function'   => $model['status']=='success' ? $this->routeArray[2] : 'show',
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }

        public function validationEmailInscribes(Request $request)
        {
            $person = $this->modelsIntranet->consumeApi("get", "Person", "showByEmail", $request, $request['email']);
            // $person = $this->modelsIntranet->show($request, "Person", $request['email']);
            $inscribe = Inscribes::where('person_id', $person['data']['id'])
                ->where('event_id', $request->event_id)
                ->with('payments')
                ->get()
                ->first();
            return ['person'=>$person, 'inscribe'=> $inscribe];
        }

        public function validationCPFInscribes(Request $request)
        {
            $request['cpf'] = str_replace(['-', '.'],'',$request['cpf']);
            $person = $this->modelsIntranet->consumeApi("get", "Person", "showByCpf", $request, $request['cpf']);
            $inscribes = [];
            $inscribe = null;

            if ($person['data']) {
                if($person['data']['registrations']){
                    foreach ($person['data']['registrations'] as $key => $value) {
                        array_push($inscribes, $value['id']);
                    }
                    $inscribe = Inscribes::whereIn('registration_id', $inscribes)
                        ->orWhere('person_id', $person['data']['id']);
                    //Erro que impede que estudantes sejam consultados no vestibular
                }else{
                    $inscribe = Inscribes::where('person_id', $person['data']['id']);
                }
                $event = $this->modelsIntranet->show($request,'Events', $request->event_id);
                $inscribe = $inscribe->where('event_id', $request->event_id)
                ->with('payments')
                ->get()
                ->first();
                if ($inscribe) {
                    $inscribe->event = $event['data'];
                }
            }
            return ['person'=>$person, 'inscribe'=> $inscribe];
        }

        public function validationNumberRegistrationInscribes(Request $request)
        {
            $request['number_registration'] = str_replace('-','',$request['number_registration']);
            $person = $this->modelsIntranet->consumeApi("get", "Person", "showByNumberRegistration", $request, $request['number_registration']);
            if($person['data']){
                $registration_id = $person['data']['registrations'][0]['id'];
                $inscribe = Inscribes::where('registration_id', $registration_id)
                    ->where('event_id', $request->event_id)
                    ->with('payments')
                    ->get()
                    ->first();
            }
            $event = $this->modelsIntranet->show($request,'Events', $request->event_id);
            if ($inscribe) {
                $inscribe->event = $event['data'];
            }
            return [
                'person'=>$person['data'] ? $person : null,
                'inscribe'=> $inscribe ?? null
            ];
        }

        public function validationNumberRegistrationInscribesSearch(Request $request)
        {
            $request['number_registration'] = str_replace('-','',$request['number_registration']);
            $person = $this->modelsIntranet->show($request, "Person", $request['number_registration']);

            $inscribe = Inscribes::where('registration_id', $person['data']['registration']['id'] ?? $person['data']['registration']['id'])
                ->where('event_id', $request->event_id)
                ->with('payments')
                ->get()
                ->first();
            return ['person'=>$person, 'inscribe'=> $inscribe];
        }

        public function search(Request $request)
        {
            $model = $this->modelsIntranet->show($request,'Events', $request['id']);
            return
                [
                    'route'       =>
                        [
                            'ambient'        => $this->routeArray[0],
                            'crud'           => $this->routeArray[1],
                            'function'       => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'          => "Consulta de Inscrição",
                            'route'          => $this->routeArray,
                            'crudName'       => "Consulta de Inscrição",
                            'model'          => $model['data'],
                            'cms_page'       => CmsPages::find(1),
                            'request'        => $request,
                        ],
                    'request'                => $request,
                ];

        }

        public function isSubcribeFixed(Request $request)
        {
            $model = $this->eventsApiRepository->isSubcribeFixed($request);
            $inscribe = Inscribes::where('person_id', $model['id'])
                ->where('event_id', $request['event_id'])
                ->get()->first();

            return
                [
                    'route'       =>
                        [
                            'ambient'        => $this->routeArray[0],
                            'crud'           => $this->routeArray[1],
                            'function'       => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'          => "Consulta de Inscrição",
                            'route'          => $this->routeArray,
                            'crudName'       => "Consulta de Inscrição",
                            'model'          => $model,
                            'cms_page'       => CmsPages::find(1),
                            'request'        => $request,
                            'inscribe'       => $inscribe,
                        ],
                    'request'                => $request,
                ];

        }

        public function clearRequest($request)
        {

            //formatando telefone
            if ($request['cell_phone']) {
                $phone = Functions::removeMaskPhone($request['cell_phone']);
                $request['cell_phone'] = $phone['codeArea'].$phone['phone'];
            }

            if ($request['home_phone']) {
                $phone = Functions::removeMaskPhone($request['home_phone']);
                $request['home_phone'] = $phone['codeArea'].$phone['phone'];
            }

            //formatando cpf
            if ($request['cpf']) {
                $cpf = Functions::removeMaskCpf($request['cpf']);
                $request['cpf'] = $cpf;
            }

            //formatando preço
            if(!$request['price']){
                $price_1 = isset($request['professional_category'])  ? $request['professional_category'] : null;
                if($price_1  ==   null      ? $price_2  ='20.00' : '');
                if($price_1  ==   'ag_20'   ? $price_2  ='20.00' : '');
                if($price_1  ==   'apl_20'  ? $price_2  ='30.00' : '');
                if($price_1  ==   'aps_50'  ? $price_2  ='50.00' : '');
                if($price_1  ==   'pp_60'   ? $price_2  ='60.00' : '');
            }else{
                $price_2 = $request['price'];
            }

            $request['price'] = $price_2;

            return $request;
        }

    }




