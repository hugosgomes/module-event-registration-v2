<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->text('label');
            $table->string('group');
            $table->boolean('system')->default(0);
            $table->boolean('default')->default(0);

            //----------------------------------------------------------------
            $table->unsignedBigInteger('create_user_id')->nullable();
            $table->foreign('create_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->unsignedBigInteger('update_user_id')->nullable();
            $table->foreign('update_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->unsignedBigInteger('delete_user_id')->nullable();
            $table->foreign('delete_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            //----------------------------------------------------------------

            $table->timestamps();
            $table->softDeletes();
        });
        //-----------------------------------------------------------------
        Schema::create('permission_role', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('permission_id')->unsigned();
            $table->bigInteger('role_id')->unsigned();
            //-----------------------------------------------------------------
            $table->foreign('permission_id')
                ->references('id')
                ->on('permissions')
                ->onDelete('cascade');
            //-----------------------------------------------------------------
            $table->foreign('role_id')
                ->references('id')
                ->on('roles')
                ->onDelete('cascade');

        });
    }

    //------------------------------------------------------------------------------------------------------------------
    public function down()
    {
        Schema::dropIfExists('permission_role');
        Schema::dropIfExists('permissions');
    }
}
