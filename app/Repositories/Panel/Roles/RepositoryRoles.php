<?php

namespace App\Repositories\Panel\Roles;

use App\Events\Roles\RolesRegister;
use App\Events\Roles\RolesUpdate;
use App\Models\Configuration\Configurations;
use App\Models\Configuration\Permissions;
use App\Models\Configuration\Roles;
use Illuminate\Http\Request;
use Gate;


class RepositoryRoles
{
    //Variáveis Globais

    protected                     $request;
    protected                     $model;
    protected                     $routeArray;

    //Funções Padrões

    public function __construct
    (
        Request                   $request,
        Roles                     $model,
        Configurations            $configurations
    )
    {
        $this->request          = $request;
        $this->model            = $model;
        $this->configurations   = $configurations;
        $this->routeArray       = $this->configurations->getRouteName($request);
        $this->crudName         = 'Funções';
    }


    //Main Functions

    public function index(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model = $this->model->where('id', '!=', 0)->with('permissions')->get();
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'crudName'   => $this->crudName,
                            'model'      => $model,
                            'trashed'    => $this->model->where('id', '!=', 0)->onlyTrashed()->get()
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function show(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model = $this->model->find($request['id']);
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $model->id,
                            'route'      => $this->routeArray,
                            'crudName'   => $this->crudName,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function create(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model = $this->model;
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'crudName'   => $this->crudName,
                            'permissions'=> Permissions::all(),
                            'model'      => $model,
                            'groups'     => Permissions::distinct()->select('group')->get(),
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function store(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            //-----------------------------------------------------
            $model =  $this->model->create($request->all());
            event(new RolesRegister($model, $request->permissions_ids));
            //-----------------------------------------------------
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                            'redirect'   => $this->routeArray[0].'/'.$this->routeArray[1].'/index'
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function edit(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model = $this->model->find($request['id']);
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                            'item'       => $model,
                            'crudName'   => $this->crudName,
                            'permissions'=> Permissions::all(),
                            'groups'     => Permissions::distinct()->select('group')->get(),
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function update(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]) && $this->configurations->CrudSystemNotModified($request, $this->model)==false)
        {
            //-----------------------------------------------------
            $model = $this->model->find($request->id);
            $model->update($request->all());
            event(new RolesUpdate($model, $request->permissions_ids));
            //-----------------------------------------------------
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function delete(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]) && $this->configurations->CrudSystemNotModified($request, $this->model)==false)
        {
            $model = $this->model->destroy($request['id']);
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function restore(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model = $this->model->withTrashed()->where('id', $request['id'])->restore();
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

}
