<?php

namespace App\Repositories\Panel\Inscribes;

use App\APIIntranet\Models\ModelBase;
use App\Models\Configuration\Configurations;
use App\Models\Configuration\Functions;
use App\Models\Configuration\Inscribes;
use App\Repositories\EventsApiRepository;
use Exception;
use Illuminate\Http\Request;
use Gate;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class RepositoryInscribes
{
    //Variáveis Globais

    protected                     $request;
    protected                     $model;
    protected                     $routeArray;
    protected                     $modelsIntranet;
    protected                     $eventsApiRepository;

    //Funções Padrões

    public function __construct(
        Request                   $request,
        Inscribes                 $model,
        Configurations            $configurations,
        ModelBase                 $modelsIntranet,
        EventsApiRepository       $eventsApiRepository
    ) {
        $this->request              = $request;
        $this->model                = $model;
        $this->configurations       = $configurations;
        $this->modelsIntranet       = $modelsIntranet;
        $this->routeArray           = $this->configurations->getRouteName($request);
        $this->crudName             = 'Inscritos';
        $this->eventsApiRepository  = $eventsApiRepository;
    }


    //Main Functions

    public function index(Request $request)
    {
        if (Gate::allows($this->routeArray[0] . '.' . $this->routeArray[1] . '.' . $this->routeArray[2])) {
            //Criando um array sem os nulos para filtrar as inscrições
            $data = array_where($request->all(), function ($value, $key) {
                return !is_null($value);
            });

            $disabled = '';

            //Buscando eventos para alimentar o select//////////////////////////
            $events = $this->eventsApiRepository->index($request);

            //Buscando usuário logado
            $user = auth()->user();

            //Controle que determina quais eventos o usuário pode ver
            foreach ($events as $key => $event) {
                if ($event['id'] == $user->profile_event) {
                    $data['event_id'] = $event['id'];
                    $request['event_id'] = $event['id'];
                    $disabled = 'disabled';
                    break;
                }
            }

            $model = $this->model->where($data)->get();
            $request['person_ids'] = $this->model->where('person_id', '<>', null)->pluck('person_id')->toArray();
            $request['registration_ids'] = $this->model->where('registration_id', '<>', null)->pluck('registration_id')->toArray();
            $persons =  $this->modelsIntranet->index($request, 'Person');

            if ($model->count() > 0) {
                foreach ($model as $inscribe) {
                    $registration_id = $inscribe->registration_id;
                    if ($registration_id != null) {
                        foreach ($persons['data'] as $person) {
                            if (!$person['registrations']) continue;
                            foreach ($person['registrations'] as $registration) {
                                if ($registration['id'] == $inscribe->registration_id) {
                                    $inscribe->person = $person;
                                    $inscribe->registration = $registration;
                                }
                            }
                        }
                        $inscribe->isStudent = 'Sim';
                    } else {
                        if (isset($persons['data'])) {
                            foreach ($persons['data'] as $person) {
                                // if ($person['registrations']) continue;
                                if ($person['id'] == $inscribe->person_id) {
                                    $inscribe->person = $person;
                                }
                            }
                        }
                        $inscribe->isStudent = 'Não';
                    }
                    $event_id = $inscribe->event_id;
                    $inscribe->event =  Arr::first($events, function ($value, $key) use ($event_id) {
                        return $value['id'] == $event_id;
                    });
                }
            }
            return
                [
                    'route'       =>
                    [
                        'ambient'    => $this->routeArray[0],
                        'crud'       => $this->routeArray[1],
                        'function'   => $this->routeArray[2],
                    ],
                    'compact'     =>
                    [
                        'title'      => $this->crudName,
                        'route'      => $this->routeArray,
                        'crudName'   => $this->crudName,
                        'model'      => $model,
                        'trashed'    => $this->model->where('id', '!=', 0)->onlyTrashed()->get(),
                        'events'     => $events,
                        'request'    => $request,
                        'disabled'   => $disabled,
                    ],
                    'request'            => $request,
                ];
        } else {
            return false;
        }
    }

    function criterioArrayWhere($value, $event_id)
    {
        return $value['id'] == $event_id;
    }

    public function show(Request $request)
    {
        if (Gate::allows($this->routeArray[0] . '.' . $this->routeArray[1] . '.' . $this->routeArray[2])) {
            $model = $this->model->find($request['id']);
            return
                [
                    'route'       =>
                    [
                        'ambient'    => $this->routeArray[0],
                        'crud'       => $this->routeArray[1],
                        'function'   => $this->routeArray[2],
                    ],
                    'compact'     =>
                    [
                        'title'      => $model->id,
                        'route'      => $this->routeArray,
                        'crudName'   => $this->crudName,
                        'model'      => $model,
                    ],
                    'request'            => $request,
                ];
        } else {
            return false;
        }
    }

    public function create(Request $request)
    {
        if (Gate::allows($this->routeArray[0] . '.' . $this->routeArray[1] . '.' . $this->routeArray[2])) {
            $model = $this->model;
            return
                [
                    'route'       =>
                    [
                        'ambient'    => $this->routeArray[0],
                        'crud'       => $this->routeArray[1],
                        'function'   => $this->routeArray[2],
                    ],
                    'compact'     =>
                    [
                        'title'      => $this->crudName,
                        'route'      => $this->routeArray,
                        'crudName'   => $this->crudName,
                        'model'      => $model,
                    ],
                    'request'            => $request,
                ];
        } else {
            return false;
        }
    }

    public function store(Request $request)
    {
        if (Gate::allows($this->routeArray[0] . '.' . $this->routeArray[1] . '.' . $this->routeArray[2])) {
            //-----------------------------------------------------
            $model =  $this->model->create($request->all());
            //-----------------------------------------------------
            return
                [
                    'route'       =>
                    [
                        'ambient'    => $this->routeArray[0],
                        'crud'       => $this->routeArray[1],
                        'function'   => $this->routeArray[2],
                    ],
                    'compact'     =>
                    [
                        'title'      => $this->crudName,
                        'route'      => $this->routeArray,
                        'model'      => $model,
                        'redirect'   => $this->routeArray[0] . '/' . $this->routeArray[1] . '/index'
                    ],
                    'request'            => $request,
                ];
        } else {
            return false;
        }
    }

    public function edit(Request $request)
    {
        if (Gate::allows($this->routeArray[0] . '.' . $this->routeArray[1] . '.' . $this->routeArray[2])) {
            $model = $this->model->find($request['id']);

            //Buscando a pessoa
            if ($model->registration_id) {
                $request['isStudent'] = 'yes';
                $model->person =  $this->modelsIntranet->show($request, 'Person', $model->registration_id)['data'];
                $model->isStudent = 'Sim';
            } else {
                $request['isStudent'] = 'no';
                $model->person =  $this->modelsIntranet->show($request, 'Person', $model->person_id)['data'];
                $model->isStudent = 'Não';
            }
            $model->event =  $this->modelsIntranet->show($request, 'Events', $model->event_id)['data'];

            //Buscando eventos para alimentar o select//////////////////////////
            $events = $this->modelsIntranet->index($request, 'Events')['data'];

            //Filtrando os telefones para exibir///////////////////////////////
            $cellphone = Arr::first($model->person['phones'], function ($value, $key) {
                return $value['type'] == 'cell_phone';
            });
            $homephone = Arr::first($model->person['phones'], function ($value, $key) {
                return $value['type'] == 'home_phone';
            });
            $model->cell_phone = $cellphone['formated_phone'] ?? null;
            $model->home_phone = $homephone['formated_phone'] ?? null;

            return
                [
                    'route'       =>
                    [
                        'ambient'    => $this->routeArray[0],
                        'crud'       => $this->routeArray[1],
                        'function'   => $this->routeArray[2],
                    ],
                    'compact'     =>
                    [
                        'title'      => $this->crudName,
                        'route'      => $this->routeArray,
                        'model'      => $model,
                        'item'       => $model,
                        'events'     => $events,
                        'crudName'   => $this->crudName,
                    ],
                    'request'            => $request,
                ];
        } else {
            return false;
        }
    }

    public function update(Request $request)
    {
        if (Gate::allows($this->routeArray[0] . '.' . $this->routeArray[1] . '.' . $this->routeArray[2]) && $this->configurations->CrudSystemNotModified($request, $this->model) == false) {
            //-----------------------------------------------------
            $model = $this->model->find($request->id);
            $model->update($request->all());
            //-----------------------------------------------------
            $requestPerson = $request;
            $requestPerson['id'] = $request['person_id'];
            $person = $this->modelsIntranet->update($requestPerson, 'Person');
            //-----------------------------------------------------
            return
                [
                    'route'       =>
                    [
                        'ambient'    => $this->routeArray[0],
                        'crud'       => $this->routeArray[1],
                        'function'   => $this->routeArray[2],
                    ],
                    'compact'     =>
                    [
                        'title'      => $this->crudName,
                        'route'      => $this->routeArray,
                        'model'      => $model,
                    ],
                    'request'            => $request,
                ];
        } else {
            return false;
        }
    }


    public function delete(Request $request)
    {
        if (Gate::allows($this->routeArray[0] . '.' . $this->routeArray[1] . '.' . $this->routeArray[2]) && $this->configurations->CrudSystemNotModified($request, $this->model) == false) {
            $model = $this->model->destroy($request['id']);
            return
                [
                    'route'       =>
                    [
                        'ambient'    => $this->routeArray[0],
                        'crud'       => $this->routeArray[1],
                        'function'   => $this->routeArray[2],
                    ],
                    'compact'     =>
                    [
                        'title'      => $this->crudName,
                        'route'      => $this->routeArray,
                        'model'      => $model,
                    ],
                    'request'            => $request,
                ];
        } else {
            return false;
        }
    }

    public function restore(Request $request)
    {
        if (Gate::allows($this->routeArray[0] . '.' . $this->routeArray[1] . '.' . $this->routeArray[2])) {
            $model = $this->model->withTrashed()->where('id', $request['id'])->restore();
            return
                [
                    'route'       =>
                    [
                        'ambient'    => $this->routeArray[0],
                        'crud'       => $this->routeArray[1],
                        'function'   => $this->routeArray[2],
                    ],
                    'compact'     =>
                    [
                        'title'      => $this->crudName,
                        'route'      => $this->routeArray,
                        'model'      => $model,
                    ],
                    'request'            => $request,
                ];
        } else {
            return false;
        }
    }

    public function report(Request $request)
    {
        if (Gate::allows($this->routeArray[0] . '.' . $this->routeArray[1] . '.' . $this->routeArray[2])) {

            $data = array_where($request->all(), function ($value, $key) {
                return !is_null($value);
            });

            $disabled = '';

            //Buscando eventos para alimentar o select//////////////////////////
            $events = $this->modelsIntranet->index($request, 'Events')['data'];

            //Verificando a função do usuário logado para filtrar por evento
            $roleUser = auth()->user()->roles()->first();

            foreach ($events as $key => $event) {
                if ($event['name'] == $roleUser->name) {
                    $data['event_id'] = $event['id'];
                    $request['event_id'] = $event['id'];
                    $disabled = 'disabled';
                    break;
                }
            }

            $event_id = $data['event_id'] ?? null;
            if ($event_id) {
                $typeEvent = array_where($events, function ($value, $key) use ($event_id) {
                    return $value['id'] == $event_id;
                });
                $typeEvent = Arr::first($typeEvent)['type']['name'];
                if ($typeEvent == 'Vestibular') {
                    $data['unity'] = $request['unity'] ?? 'IT';
                    $statistics = $typeEvent == 'Vestibular' ? $this->vestibularStatisticalReport($data['unity'] ?? null) : null;
                    $statisticsSum = $typeEvent == 'Vestibular' ? $this->vestibularStatisticalReportTotals($data['unity'] ?? null) : null;
                } else {
                    $data['unity'] = null;
                }
            }

            $report['totalInscribes'] = $this->model->where($data)->count();
            $report['totalInscribesPaidOut'] = $this->model->where($data)->where('status', 3)->count();
            $report['totalInscribesNoPaidOut'] = $this->model->where($data)->where('status', '<>', 3)->count();

            return
                [
                    'route'       =>
                    [
                        'ambient'       => $this->routeArray[0],
                        'crud'          => $this->routeArray[1],
                        'function'      => $this->routeArray[2],
                    ],
                    'compact'     =>
                    [
                        'title'         => $this->crudName,
                        'route'         => $this->routeArray,
                        'crudName'      => $this->crudName,
                        'model'         => $report,
                        'events'        => $events,
                        'disabled'      => $disabled,
                        'statistics'    => $statistics ?? null,
                        'statisticsSum' => $statisticsSum ?? null
                    ],
                    'request'            => $request,
                ];
        } else {
            return false;
        }
    }

    public function vestibularStatisticalReport($unity)
    {
        $query = DB::select("SELECT course, COUNT(id) as inscritos, SUM(CASE WHEN status = 3 THEN 1 ELSE 0 END) AS pagos, " .
            "SUM(CASE WHEN status = 1 THEN 1 ELSE 0 END) AS nao_pagos " .
            "FROM inscribes WHERE event_id = 5 AND deleted_at IS NULL AND unity = ? GROUP BY course", [$unity]);
        return $query;
    }

    public function vestibularStatisticalReportTotals($unity)
    {
        $unity = $unity ?? 'IT';
        $querySum = DB::select("SELECT COUNT(id) as inscritos, SUM(CASE WHEN status = 3 THEN 1 ELSE 0 END) AS pagos, " .
            "SUM(CASE WHEN status = 1 THEN 1 ELSE 0 END) AS nao_pagos " .
            "FROM inscribes WHERE event_id = 5 AND deleted_at IS NULL AND unity = ?", [$unity]);
        return $querySum[0];
    }

    public function getVagas()
    {
        return [
            'IT' => [
                'ADMINISTRACAO - Turno: NOITE'                              => 162,
                'DIREITO - Turno: MANHA'                                    => 54,
                'DIREITO - Turno: NOITE'                                    => 133,
                'EDUCACAO FISICA - LICENCIATURA - Turno: NOITE'             => 117,
                'ENFERMAGEM - Turno: NOITE'                                 => 54,
                'ENGENHARIA CIVIL - Turno: MANHA'                           => 54,
                'ENGENHARIA CIVIL - Turno: NOITE'                           => 126,
                'ENGENHARIA DE PRODUCAO - Turno: NOITE'                     => 81,
                'FARMACIA - Turno: NOITE'                                   => 135,
                'FISIOTERAPIA - Turno: NOITE'                               => 58,
                'MEDICINA VETERINARIA - Turno: TARDE'                       => 72,
                'NUTRICAO - Turno: MANHA'                                   => 36,
                'NUTRICAO - Turno: NOITE'                                   => 72,
                'ODONTOLOGIA - Turno: INTEGRAL'                             => 162
            ],
            'NI' => [
                'ADMINISTRACAO - Turno: NOITE'                              => 162,
                'BIOMEDICINA - Turno: NOITE'                                => 72,
                'CIENCIAS BIOLOGICAS - BACHARELADO - Turno: NOITE'          => 36,
                'CIENCIAS BIOLOGICAS - LICENCIATURA - Turno: NOITE'         => 72,
                'DIREITO - Turno: MANHA'                                    => 67,
                'DIREITO - Turno: NOITE'                                    => 180,
                'DIREITO - Turno: TARDE'                                    => 144,
                'EDUCACAO FISICA - BACHARELADO - Turno: NOITE'              => 45,
                'EDUCACAO FISICA - LICENCIATURA - Turno: MANHA'             => 45,
                'EDUCACAO FISICA - LICENCIATURA - Turno: NOITE'             => 135,
                'ENFERMAGEM - Turno: MANHA'                                 => 54,
                'ENFERMAGEM - Turno: NOITE'                                 => 81,
                'ENFERMAGEM - Turno: TARDE'                                 => 45,
                'ENGENHARIA CIVIL - Turno: MANHA'                           => 36,
                'ENGENHARIA CIVIL - Turno: NOITE'                           => 126,
                'ENGENHARIA DE PRODUCAO - Turno: NOITE'                     => 162,
                'ENGENHARIA MECANICA - Turno: NOITE'                        => 162,
                'FARMACIA - Turno: MANHA'                                   => 45,
                'FARMACIA - Turno: NOITE'                                   => 90,
                'FISIOTERAPIA - Turno: NOITE'                               => 90,
                'MEDICINA VETERINARIA - Turno: TARDE'                       => 144,
                'NUTRICAO - Turno: MANHA'                                   => 45,
                'NUTRICAO - Turno: NOITE'                                   => 99,
                'ODONTOLOGIA - Turno: INTEGRAL'                             => 180,
                'PEDAGOGIA - Turno: NOITE'                                  => 72,
                'TECNOLOGIA EM ESTETICA E COSMETICA - Turno: NOITE'         => 72,
                'TECNOLOGIA EM GESTAO DE RECURSOS HUMANOS - Turno: NOITE'   => 90,
                'TECNOLOGIA EM LOGISTICA - Turno: NOITE'                    => 72
            ]
        ];
    }

    public function import(Request $request)
    {
        if (Gate::allows($this->routeArray[0] . '.' . $this->routeArray[1] . '.' . $this->routeArray[2])) {
            $data = [];
            $datas = [];
            $arrayFile =  Functions::importaCSV($request->file(), ',');

            $chunks = array_chunk($arrayFile, 1000);

            foreach ($chunks as $chunk) {
                foreach ($chunk as $key => $value) {
                    $data['cpf'] = $value['CPF'];
                    $data['name'] = $value['NOME'];
                    $data['first_name'] = $value['NOME'];
                    $data['name_father'] = $value['PAI'];
                    $data['name_mother'] = $value['MÃE'];
                    $data['rg'] = $value['IDENTIDADE'];
                    $data['issuing_body'] = $value['ÓRGÃO EXPEDIDOR'];
                    $data['issuing_uf'] = $value['UF EMISSOR'];
                    $data['passport'] = $value['PASSAPORTE'];
                    $data['birth'] = Carbon::parse($value['DT. NASCIMENTO'])->format('d/m/Y');
                    $data['race'] = $value['COR/RAÇA'];
                    $data['municipality_of_origin'] = $value['MUNICÍPIO DE NASCIMENTO'];
                    $data['naturalness'] = $value['UF NASCIMENTO'];
                    $data['nationality'] = $value['NACIONALIDADE'];
                    $data['country_of_origin'] = $value['PAÍS ORIGEM'];
                    $data['conclusion_institution'] = $value['ESTABELECIMENTO DE ENSINO (ENSINO MÉDIO)'];
                    $data['conclusion_uf'] = $value['UF ESTABELECIMENTO DE ENSINO (ENSINO MÉDIO)'];
                    $data['conclusion_year'] = $value['ANO CONCLUSÃO (ENSINO MÉDIO)'];
                    $data['street'] = $value['ENDEREÇO'];
                    $data['complement'] = $value['COMPLEMENTO'];
                    $data['district'] = $value['BAIRRO'];
                    $data['city'] = $value['CIDADE'];
                    $data['state'] = $value['UF'];
                    $data['cep'] = $value['CEP'];
                    $data['home_phone'] = $value['TELEFONE'];
                    $data['cell_phone'] = $value['CELULAR'];
                    $data['email'] = $value['E-MAIL'];
                    $data['unity'] = $value['CAMPUS'];
                    $data['course'] = $value['NOME CURSO'] . ' - Turno: ' . $value['TURNO'];
                    $data['status'] = $value['PAGO'] == 'SIM' ? 3 : 1;

                    array_push($datas, $data);
                }
            }
            $request['persons'] = $datas;
            $return = $this->modelsIntranet->store($request, 'Person');
            if (!$return || $return['status'] == 'error') return env('APP_DEBUG') == 'true' ? dd($return) : null;
            foreach ($return['data'] as $key => $value) {
                $this->storeInscribe($value);
            }
            //-----------------------------------------------------
            return
                [
                    'route'       =>
                    [
                        'ambient'    => $this->routeArray[0],
                        'crud'       => $this->routeArray[1],
                        'function'   => $this->routeArray[2],
                    ],
                    'compact'     =>
                    [
                        'title'      => $this->crudName,
                        'route'      => $this->routeArray,
                    ],
                    'request'            => $request,
                ];
        } else {
            return false;
        }
    }

    public function export(Request $request){
        if (Gate::allows($this->routeArray[0] . '.' . $this->routeArray[1] . '.' . $this->routeArray[2])){
            $export = [];
            $inscribes = $this->model->where('event_id', 5)->get();
            $request['person_ids'] = $this->model->where('person_id', '<>', null)->pluck('person_id')->toArray();
            $request['registration_ids'] = $this->model->where('registration_id', '<>', null)->pluck('registration_id')->toArray();
            $persons =  $this->modelsIntranet->index($request, 'Person');

            if(!$persons || $persons['status'] == 'error' || $persons['data'] == []){
                return env('APP_DEBUG')=='true' ? dd($persons) : null;
            }

            if ($inscribes->count() > 0) {
                foreach ($inscribes as $inscribe) {
                    $registration_id = $inscribe->registration_id;
                    if ($registration_id != null) {
                        foreach ($persons['data'] as $person) {
                            if (!$person['registrations']) continue;
                            foreach ($person['registrations'] as $registration) {
                                if ($registration['id'] == $inscribe->registration_id) {
                                    $inscribe->person = $person;
                                    $inscribe->registration = $registration;
                                }
                            }
                        }
                        $inscribe->isStudent = 'Sim';
                    } else {
                        if (isset($persons['data'])) {
                            foreach ($persons['data'] as $person) {
                                // if ($person['registrations']) continue;
                                if ($person['id'] == $inscribe->person_id) {
                                    $inscribe->person = $person;
                                }
                            }
                        }
                        $inscribe->isStudent = 'Não';
                    }
                }
                $export = $this->createArrayExport($inscribes);
                if(!is_dir("download")) mkdir('download');
                $filename = 'download/Relatório_Inscritos_' . date("Y-m-d") . ".csv";
                if(Functions::exportCsv($export, $filename)){
                    $export = $filename;
                }else{
                    $export = false;
                }
            }
            return
                [
                    'route'       =>
                    [
                        'ambient'    => $this->routeArray[0],
                        'crud'       => $this->routeArray[1],
                        'function'   => $this->routeArray[2],
                    ],
                    'compact'     =>
                    [
                        'title'      => $this->crudName,
                        'route'      => $this->routeArray,
                        'model'      => $export,
                    ],
                    'request'            => $request,
                ];
        }else{
            return false;
        }
    }

    public function createArrayExport($inscribes){
        $arrayExport = [];
        foreach ($inscribes as $inscribe) {
            $arrayInscribe = [
                'UNIDADE'                   => $inscribe->unity == 'NI' ? "Nova Iguaçu" : "Itaperuna",
                'CURSO'                     => 'Curso: ' . $inscribe->course,
                'NOME'                      => $inscribe->person['name'],
                'EMAIL'                     => $inscribe->person['email'],
                'DT. NASCIMENTO'            => $inscribe->person['birth'],
                'GÊNERO'                    => $inscribe->person['gender'],
                'NOME SOCIAL'               => $inscribe->person['social_name'],
                'RG'                        => $inscribe->person['rg'],
                'ÓRGÃO EMISSOR'             => $inscribe->person['issuing_body'],
                'ESTADO EMISSOR'            => $inscribe->person['issuing_uf'],
                'NOME DA MÃE'               => $inscribe->person['name_mother'],
                'NOME DO PAI'               => $inscribe->person['name_father'],
                'NACIONALIDADE'             => $inscribe->person['nationality'],
                'CPF'                       => $inscribe->person['cpf'],
                'MATRÍCULA'                 => count($inscribe->person['registrations'])>0 ? $inscribe->person['registrations'][0]['number_registration'] : '',
                'ESTADO CIVIL'              => $inscribe->person['marital_status'],
                'POSSUI DEFICIÊNCIA'        => $inscribe->person['disabled_person'] == 1 ? 'Sim' : 'Não',
                'PASSAPORTE'                => $inscribe->person['passport'],
                'PAÍS DE ORIGEM'            => $inscribe->person['country_of_origin'],
                'NATURALIDADE'              => $inscribe->person['naturalness'],
                'MUNICÍPIO DE ORIGEM'       => $inscribe->person['municipality_of_origin'],
                'RAÇA'                      => $inscribe->person['race'],
                'RUA'                       => $inscribe->person['address'][0]['street'] ?? null,
                'NÚMERO'                    => $inscribe->person['address'][0]['number'] ?? null,
                'COMPLEMENTO'               => $inscribe->person['address'][0]['complement'] ?? null,
                'BAIRRO'                    => $inscribe->person['address'][0]['district'] ?? null,
                'CIDADE'                    => $inscribe->person['address'][0]['city'] ?? null,
                'ESTADO'                    => $inscribe->person['address'][0]['state'] ?? null,
                'CEP'                       => $inscribe->person['address'][0]['cep'] ?? null,
                'TEL. CELULAR'              => $inscribe->person['phones'][0]['formated_phone'] ?? null,
                'TEL. FIXO'                 => $inscribe->person['phones'][1]['formated_phone'] ?? null,
                'INSTITUIÇÃO DE CONCLUSÃO'  => $inscribe->person['formations'][0]['conclusion_institution'] ?? null,
                'ANO DE CONCLUSÃO'          => $inscribe->person['formations'][0]['conclusion_year'] ?? null,
                'ESTADO DE CONCLUSÃO'       => $inscribe->person['formations'][0]['conclusion_uf'] ?? null,
                'PAGO'                      => $inscribe->status == 3 ? 'PAGO' : 'PENDENTE',
            ];
            $arrayInscribe = array_map(function ($value) {
                return str_replace(';', ',', $value);
            }, $arrayInscribe);
            array_push($arrayExport, $arrayInscribe);
        }
        return $arrayExport;
    }

    public function storeInscribe($request)
    {
        $inscribe = '';
        $this->clearRequest($request);
        $valInscribeExist = Inscribes::where('person_id', $request['id'])
            ->where('event_id', 5)
            ->get()->first();
        if ($valInscribeExist == null) {
            $inscribe = Inscribes::create([
                'person_id' => $request['id'],
                'event_id'  => 5,
                'unity'     => $request['unity'],
                'course'    => $request['course'],
                'status'    => $request['status']
            ]);
        } else {
            $inscribe = $valInscribeExist;
        }

        return $inscribe;
    }

    public function clearRequest($request)
    {

        //formatando telefone
        if ($request['cell_phone']) {
            $phone = Functions::removeMaskPhone($request['cell_phone']);
            $request['cell_phone'] = $phone['codeArea'] . $phone['phone'];
        }

        if ($request['home_phone']) {
            $phone = Functions::removeMaskPhone($request['home_phone']);
            $request['home_phone'] = $phone['codeArea'] . $phone['phone'];
        }

        //formatando cpf
        if ($request['cpf']) {
            $cpf = Functions::removeMaskCpf($request['cpf']);
            $request['cpf'] = $cpf;
        }

        //formatando preço
        if (!isset($request['price'])) {
            $price_1 = isset($request['professional_category'])  ? $request['professional_category'] : null;
            if ($price_1  ==   null      ? $price_2  = '20.00'  : '');
            if ($price_1  ==   'ag_20'   ? $price_2  = '20.00' : '');
            if ($price_1  ==   'apl_20'  ? $price_2  = '30.00' : '');
            if ($price_1  ==   'aps_50'  ? $price_2  = '50.00' : '');
            if ($price_1  ==   'pp_60'   ? $price_2  = '60.00' : '');
        } else {
            $price_2 = $request['price'];
        }

        return $request;
    }
}
