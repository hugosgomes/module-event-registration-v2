<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();
        $user0 = User::create([
            'id'                   =>1,
            'name'                 =>'Administrador',
            'email'                =>'admin@admin',
            'password'             =>bcrypt('cgs@020459'),
            'system'               =>1,
            'create_user_id'       =>1
        ]);
        $user1 = User::create([
            'id'                   =>2,
            'name'                 =>'Development',
            'email'                =>'dev@didatico',
            'password'             =>bcrypt('dev@020459'),
            'system'               =>1,
            'create_user_id'       =>1
        ]);
        $user1 = User::create([
            'id'                   =>3,
            'name'                 =>'Operador',
            'email'                =>'operador@didatico',
            'password'             =>bcrypt('op@020459'),
            'system'               =>1,
            'create_user_id'       =>1
        ]);
    }
}
