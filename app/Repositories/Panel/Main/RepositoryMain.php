<?php

namespace App\Repositories\Panel\Main;

use App\Models\Configuration\Configurations;
use App\Models\Panel\Main;
use Illuminate\Http\Request;
use Gate;


class RepositoryMain
{
    //Variáveis Globais

    protected                     $request;
    protected                     $model;
    protected                     $routeArray;

    //Funções Padrões

    public function __construct
    (
        Request                   $request,
        Main                      $model,
        Configurations            $configurations
    )
    {
        $this->request =          $request;
        $this->model =            $model;
        $this->configurations   = $configurations;
        $this->routeArray       = $this->configurations->getRouteName($request);
        $this->crudName         = 'Painel de Controle';
    }


    //Main Functions

    public function index(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model = $this->model;
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'crudName'   => $this->crudName,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function show(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            dd('teste');
            $model = $this->model->find($request['id']);
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $model->id,
                            'route'      => $this->routeArray,
                            'crudName'   => $this->crudName,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function create(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model = $this->model;
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'crudName'   => $this->crudName,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function store(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model =  $this->model;
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                            'redirect'   => $this->routeArray[0].'/'.$this->routeArray[1].'/index'
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function edit(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model = $this->model;
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                            'item'       => $model,
                            'crudName'   => 'Editando Item de ID :',
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function update(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]) && $this->configurations->CrudSystemNotModified($request, $this->model)==false)
        {
            $model = $this->model;
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function delete(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]) && $this->configurations->CrudSystemNotModified($request, $this->model)==false)
        {
            $model = $this->model;
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function restore(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model = $this->model;
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }
}
