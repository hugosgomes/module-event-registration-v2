<?php

namespace App\Repositories\Panel\CmsPages;

use App\Models\Configuration\Configurations;
use App\Models\Configuration\CmsPages;
use Illuminate\Http\Request;
use Gate;


class RepositoryCmsPages
{
    //Variáveis Globais

    protected                     $request;
    protected                     $model;
    protected                     $routeArray;

    //Funções Padrões

    public function __construct
    (
        Request                   $request,
        CmsPages                  $model,
        Configurations            $configurations
    )
    {
        $this->request          = $request;
        $this->model            = $model;
        $this->configurations   = $configurations;
        $this->routeArray       = $this->configurations->getRouteName($request);
        $this->crudName         = 'CMS das Páginas';
    }


    //Main Functions

    public function index(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model = $this->model->where('id', '!=', 0)->get();
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'crudName'   => $this->crudName,
                            'model'      => $model,
                            'trashed'    => $this->model->where('id', '!=', 0)->onlyTrashed()->get()
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function show(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model = $this->model->find($request['id']);
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $model->id,
                            'route'      => $this->routeArray,
                            'crudName'   => $this->crudName,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function create(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model = $this->model;
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'crudName'   => $this->crudName,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function store(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            //-----------------------------------------------------
            $model =  $this->model->create($request->all());
            //-----------------------------------------------------
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                            'redirect'   => $this->routeArray[0].'/'.$this->routeArray[1].'/index'
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function edit(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model = $this->model->find($request['id']);
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                            'item'       => $model,
                            'crudName'   => $this->crudName,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function update(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]) && $this->configurations->CrudSystemNotModified($request, $this->model)==false)
        {
            //-----------------------------------------------------
            $model = $this->model->find($request->id);
            $model->update($request->all());

            $filename1a = $this->configurations->EditarImagem($request,'logo1','logo1T','HomeP/img/CmsPages/','200','200');
            if($filename1a)
            {
                $model->logo1 = $filename1a;
                $model->save();
            }

            $filename1b = $this->configurations->EditarImagem($request,'logo2','logo2T','HomeP/img/CmsPages/','200','200');
            if($filename1b)
            {
                $model->logo2 = $filename1b;
                $model->save();
            }

            $filename1c = $this->configurations->EditarImagem($request,'logo3','logo3T','HomeP/img/CmsPages/','200','200');
            if($filename1c)
            {
                $model->logo3 = $filename1c;
                $model->save();
            }

            $filename2 = $this->configurations->EditarImagem($request,'session_1_img_background','session_1_img_backgroundT','HomeP/img/CmsPages/','1400','800');
            if($filename2)
            {
                $model->session_1_img_background = $filename2;
                $model->save();
            }

            $filename3 = $this->configurations->EditarImagem($request,'session_2_img_right','session_2_img_rightT','HomeP/img/CmsPages/','500','533');
            if($filename3)
            {
                $model->session_2_img_right = $filename3;
                $model->save();
            }
            //-----------------------------------------------------
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function delete(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]) && $this->configurations->CrudSystemNotModified($request, $this->model)==false)
        {
            $model = $this->model->destroy($request['id']);
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function restore(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model = $this->model->withTrashed()->where('id', $request['id'])->restore();
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

}
