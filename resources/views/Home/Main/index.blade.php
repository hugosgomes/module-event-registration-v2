@extends("$route[0].Template.index")

@section('content')
    <main id="general_page">
        <div class="container margin_60">
            <!--Team Carousel -->
            <div class="row">
                <div class="owl-carousel owl-theme team-carousel">
                    @foreach($model as $event)
                        {{-- Somente Eventos Ativos --}}
                        @if ($event['status'] == 1)
                            <div class="team-item">
                                <div class="team-item-img">
                                    <i style="display: none;">{{ $img = $event['img'] }}</i>
                                    <img src="{{ $img!=null && $img!='default' ? env('PATH_URL_EUNIG') . "PanelP/img/Events/$img" : env('PATH_URL_EUNIG') . "PanelP/img/Events/default.png" }}" alt="">
                                    <div class="team-item-detail">
                                        <div class="team-item-detail-inner">
                                            <h4>{{ $event['name'] }}</h4>
                                            <p>{!! $event['description'] !!}</p>
                                            <a href="{{ route("Home.Main.show", [$event['id'], $event['tag']]) }}" class="btn_1 white">Acessar Evento</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="team-item-info">
                                    <h4>{{ $event['name'] }}</h4>
                                    <p>{{ $event['code'] }}</p>
                                </div>
                            </div>
                        @endif
                        <!-- /team-item -->
                    @endforeach
                </div>
            </div>
            <!--End Team Carousel-->
        </div>
    </main>
@endsection
