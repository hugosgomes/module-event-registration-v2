<script>
    //Variáreis ------------------------------------------------------------------------------------
    var policy                                 = $('#policy');
    var policy_fadeout                         = $('.policy_fadeout');
    var terms                                  = $('#terms');
    var terms_fadeou                           = $('.terms_fadeou');
    var no_student                             = $('#no_student');
    var number_registration                    = $('.number_registration');
    var error_number_registration_null         = $('.error_number_registration_null');
    var forward_search                         = $('#forward_search');
    var forward                                = $('#forward');
    var reset                                  = $('#reset');
    var loading_person                         = $('#loading_person');
    var person_success                         = $('#person_success');
    var person_error                           = $('#person_error');
    var loading_inscribe                       = $('#loading_inscribe');
    var inscribe_success                       = $('#inscribe_success');
    var inscribe_error                         = $('#inscribe_error');
    var loading_email                          = $('#loading_email');
    var person_email_success                   = $('#person_email_success');
    var person_email_error                     = $('#person_email_error');
    var loading_email_inscribe                 = $('#loading_email_inscribe');
    var inscribe_email_success                 = $('#inscribe_email_success');
    var inscribe_email_error                   = $('#inscribe_email_error');
    var loading_cpf                            = $('#loading_cpf');
    var person_cpf_success                     = $('#person_cpf_success');
    var person_cpf_error                       = $('#person_cpf_error');
    var loading_cpf_inscribe                   = $('#loading_cpf_inscribe');
    var inscribe_cpf_success                   = $('#inscribe_cpf_success');
    var inscribe_cpf_error                     = $('#inscribe_cpf_error');
    var cpf_invalidated                        = $('#cpf_invalidated');
    var button_submit                          = $('.submit_finish');
    var button_backward                        = $('#backward');
    var email_data_use                         = $('#email_data_use');
    var email_data_clear                       = $('#email_data_clear');
    var cpf_data_use                           = $('#cpf_data_use');
    var cpf_data_clear                         = $('#cpf_data_clear');
    var search_number_registration             = $('#search_number_registration');
    var search_cpf                             = $('#search_cpf');
    var search_2                               = $('#search_2');
    var infor_person                           = $('.infor_person');
    var infor_inscribe                         = $('.infor_inscribe');
    var infor_payment                          = $('.infor_payment');
    var infor_status                           = $('.infor_status');
    var infor_boleto                           = $('.infor_boleto');
    //-------------------------------------
    var registration_id                        = $('#registration_id');
    var person_id                              = $('#person_id');
    var first_name                             = $('#first_name');
    var email                                  = $('#email');
    var cpf                                    = $('#cpf');
    var cpf_hidden                             = $('#cpf_hidden');
    var birth                                  = $('#birth');
    var birth_hidden                           = $('#birth_hidden');
    var cell_phone                             = $('#cell_phone');
    var cell_phone_hidden                      = $('#cell_phone_hidden');
    var home_phone                             = $('#home_phone');
    var email_hidden                           = $('#email_hidden');
    var registration_type                      = $('#registration_type');
    var professional_category                  = $('#professional_category');
    var loading_payment                        = $('.loading_payment');
    var institution_no_student                 = $('.institution_no_student');

    //document.ready---------------------------------------------------------------------------------
    $(document).ready(function(){
        $('.institution_no_student').hide();
        $('#conclusion_year').attr('max', 2021).attr('min', 1950);
        terms.click();
        startHashsPagSeguro();
    });

    //Start pagseguro--------------------------------------------------------------------------------
    function startHashsPagSeguro() {
        var hashInput = $('#hashInput');

        if(hashInput.val() == ''){
            PagSeguroDirectPayment.setSessionId("{{ isset($tokenSession) ? $tokenSession : '' }}");
            var hash = PagSeguroDirectPayment.getSenderHash();
            hashInput.val(hash);
        }


        PagSeguroDirectPayment.onSenderHashReady(function(response){
            if(response.status == 'error') {
                console.log(response.message);
                return false;
            }
            $('#hashInput').val(response.senderHash);
        });
    }


    //-----------------------------------------------------------------------------------------------
    //esconder botões
    function clearAlerts()
    {
        error_number_registration_null.hide();
        reset.hide();
        forward.hide();
        loading_person.hide();
        person_error.hide();
        loading_inscribe.hide();
        inscribe_success.hide();
        inscribe_error.hide();
        loading_email.hide();
        person_success.hide();
        person_email_success.hide();
        loading_email_inscribe.hide();
        inscribe_email_success.hide();
        inscribe_email_error.hide();
        loading_cpf.hide();
        person_cpf_success.hide();
        loading_cpf_inscribe.hide();
        inscribe_cpf_success.hide();
        inscribe_cpf_error.hide();
        cpf_invalidated.hide();
        cpf_hidden.hide();
        birth_hidden.hide();
        cell_phone_hidden.hide();
        email_hidden.hide();
        email_data_use.hide();
        email_data_clear.hide();
        cpf_data_use.hide();
        cpf_data_clear.hide();
        loading_payment.hide();
    }
    clearAlerts();
    //-----------------------------------------------------------------------------------------------
    function removeSessionAll()
    {
        //Limpar os itens no cache de session se existir
        if(window.sessionStorage.getItem('person')){ window.sessionStorage.removeItem('person')}
        if(window.sessionStorage.getItem('inscribe')){ window.sessionStorage.removeItem('inscribe')}
    }
    //-----------------------------------------------------------------------------------------------
    function getSessionPerson()
    {
        if(window.sessionStorage.getItem('person')){var getPerson = window.sessionStorage.getItem('person')}
        return JSON.parse(getPerson)['data'];
    }
    function getSessionInscribe()
    {
        if(window.sessionStorage.getItem('inscribe')){var getInscribe = window.sessionStorage.getItem('inscribe')}
        return JSON.parse(getInscribe);
    }
    //-----------------------------------------------------------------------------------------------
    policy.click(function () {
        //Checando checkbox de políticas ao aceitar o modal
        policy_fadeout.prop('checked', true);
    });
    terms.click(function () {
        //Checando checkbox de termos ao aceitar o modal
        terms_fadeou.prop('checked', true);
    });
    no_student.click(function () {
        clearAlerts();
        removeSessionAll();
        if(no_student.is(":checked")===true)
        {
            //Se checkbox de não sou aluno estiver checado faça isso
            number_registration.prop('required', false).removeClass('required');
            number_registration.val('');
            institution_no_student.prop('required', true).addClass('required');
            institution_no_student.fadeIn();

            error_number_registration_null.fadeOut();
            removeSessionAll();
            clearDataInputsPerson();
            forward_search.hide();
            forward.fadeIn();
        }
        else
        {
            //Se checkbox de "não sou aluno" não estiver checado faça isso
            number_registration.prop('required', true).addClass('required');
            institution_no_student.prop('required', false).removeClass('required');
            institution_no_student.hide();
            institution_no_student.val('');
            forward_search.fadeIn();
            forward.hide();
        }
    });
</script>

<script>

    //Verificação por matrícula

    var number_registration_exist = document.getElementById("number_registration");
    //Se campo number_registration existir faça isso
    if(number_registration_exist!==null)
    {
        //Se clicarr no input number_registration resetar a verificação
        number_registration.focusin(function () {
            forward_search.show();
            forward.hide();
        });

        //Se clicar no botão forward_search faça isso
        forward_search.click(function () {
            validateNumberRegistration();
        });
    }
    //Se campo number_registration não existir faça isso
    else
    {

    }

    function validateNumberRegistration() {
        //Limpar os itens no cache de session se existir
        removeSessionAll();
        //-------------
        clearAlerts();
        if(no_student.prop( "checked" )===true)
        {
            forward_search.hide();
            number_registration.removeClass("required").prop("required", false);
            forward.fadeIn().click();
        }
        else
        {
            number_registration.addClass("required").prop("required", true);
            if(number_registration.val()!=='')
            {
                loading_person.fadeIn();
                removeSessionAll();
                $.ajax({
                    url: "{{ route("Home.Main.validationNumberRegistrationInscribes") }}/" + number_registration.val() + '/' + "{{ isset($model['id']) ? $model['id'] : '' }}",
                    type:"GET",
                })
                    .done(function (response) {

                    })
                    .fail(function (fail) {
                        //
                    })
                    .always(function (always) {
                        validatePerson(always);
                    });
            }
            else
            {
                error_number_registration_null.fadeIn();
            }
        }
    }

    function validateInscribe(always) {
        var inscribeReturn = always.inscribe;
        if(inscribeReturn!==undefined)
        {
            if(inscribeReturn===null)
            {
                inscribe_error.hide();
                loading_inscribe.hide();
                inscribe_success.fadeIn();
                forward_search.hide();
                forward.show().click();
            }
            else
            {
                var inscribe = JSON.stringify(always.inscribe);
                window.sessionStorage.setItem('inscribe', inscribe);
                loading_inscribe.hide();
                inscribe_error.fadeIn();
            }
        }
        else
        {

        }
    }

    function validatePerson(always) {
        var personReturn = always.person;
        if(personReturn!==undefined && personReturn.status==='success')
        {
            clearDataInputsPerson();
            validateInscribe(always);
            var person = JSON.stringify(personReturn);
            window.sessionStorage.setItem('person', person);
            loading_person.hide();
            person_success.fadeIn();
            setDataInputsPerson(getSessionPerson());
        }
        else
        {
            loading_person.hide();
            person_error.fadeIn();
        }
    }

    function setDataInputsPerson(person) {
        for($i=0;$i<person.registrations.length;$i++)
        {
            if(person.registrations[$i].number_registration===number_registration.val())
            {
                var registrationID = person.registrations[$i].id;
            }
        }
        registration_id.val(registrationID);
        person_id.val(person.id);
        first_name.prop("readonly", true).val(person.first_name);
        birth.prop("readonly", true).val(person.birth);
        cpf.prop("readonly", true).val(person.cpf);
        cell_phone.prop("readonly", true).val(person.phones[0].formated_phone);

        if (person.email != '') {
            email.prop("readonly", true).val(person.email);
        }

        if (person.phones[0].formated_phone != '') {
            cell_phone.prop("readonly", true).val(person.phones[0].formated_phone);
        }
    }

    function clearDataInputsPerson() {
        registration_id.val("").text("");
        person_id.val("").text("");
        first_name.prop("readonly", false).val("").text("");
        email.prop("readonly", false).val("").text("");
        cpf.prop("readonly", false).val("").text("");
        birth.prop("readonly", false).val("").text("");
        cell_phone.prop("readonly", false).val("").text("");
    }

</script>

<script>

    //Verificação por email

    function validateInscribeEmail(always) {
        var inscribeReturn = always.inscribe;
        if(inscribeReturn!==undefined)
        {
            if(inscribeReturn===null)
            {
                inscribe_email_error.hide();
                loading_email_inscribe.hide();

                var valInsErrorEmail  = inscribe_email_error.is(':visible');
                var valInsErrorCPF  = inscribe_cpf_error.is(':visible');
                if (valInsErrorEmail===false && valInsErrorCPF===false){forward.prop("disabled", false);}

                inscribe_email_success.fadeIn();
                return inscribeReturn;
            }
            else
            {
                var inscribe = JSON.stringify(always.inscribe);
                window.sessionStorage.setItem('inscribe', inscribe);
                loading_email_inscribe.hide();
                forward.prop("disabled", true);
                inscribe_email_success.hide();
                inscribe_email_error.fadeIn();

                return inscribeReturn;
            }
        }
        else
        {
            return inscribeReturn;
        }
    }

    function validatePersonEmail(always) {
        var personReturn = always.person;

        if(personReturn!==undefined && personReturn.status==='success')
        {
            var varInscribe = validateInscribeEmail(always);
            var person = JSON.stringify(personReturn);
            window.sessionStorage.setItem('person', person);
            loading_email.hide();
            person_email_error.hide();
            person_email_success.fadeIn();

            var valInsSuccess  = person_email_error.is(':visible');
            if (valInsSuccess===true){inscribe_email_success.hide();}
            else if(varInscribe===null && valInsSuccess!==true){clearDataInputsPersonEmail();email_data_use.show();email_data_clear.show();}

            email_data_use.click(function () {
                event.preventDefault();
                setDataInputsPersonEmail(getSessionPerson());
            });

        }
        else
        {
            loading_email.hide();
            person_email_success.hide();
            person_email_error.hide();

            var valInsError  = inscribe_email_error.is(':visible');
            if (valInsError===true){inscribe_email_error.hide(); inscribe_email_success.hide();}
        }

    }

    function setDataInputsPersonEmail(person) {

        inscribe_email_error.hide();
        inscribe_email_success.hide();
        person_email_success.hide();
        person_email_error.hide();

        for($i=0;$i<person.registrations.length;$i++)
        {
            if(person.registrations[$i].number_registration===number_registration.val())
            {
                var registrationID = person.registrations[$i].id;
            }
        }
        registration_id.val(registrationID);
        person_id.val(person.id);
        first_name.prop("readonly", false).val(person.first_name);
        email.prop("readonly", false).val(person.email);
        birth.prop("readonly", false).val(person.birth);
        cell_phone.prop("readonly", false).val(person.phones[0].formated_phone);

        email_hidden.show().prop("readonly", true).val(person.email);
        email.hide().prop("readonly", true).val(person.email);
        //-----------------------------------------------------------
        cpf.prop("readonly", false).val(person.cpf);
        //-----------------------------------------------------------
        birth_hidden.show().prop("readonly", true).val(person.birth);
        birth.hide().prop("readonly", true).val(person.birth);
        //-----------------------------------------------------------
        cell_phone_filter = person.phones.filter(function (value) {
            return value['type'] == 'cell_phone';
        });
        cell_phone_hidden.show().prop("readonly", true).val(cell_phone_filter[0].formated_phone);
        cell_phone.hide().prop("readonly", true).val(cell_phone_filter[0].formated_phone);
        //-----------------------------------------------------------
        home_phone_filter = person.phones.filter(function (value) {
            return value['type'] == 'home_phone';
        });
        home_phone.prop("readonly", true).val(home_phone_filter[0].formated_phone);

        $('#gender').val(person.gender);
        $('#social_name').val(person.social_name);
        $('#rg').val(person.rg);
        $('#issuing_body').val(person.issuing_body);
        $('#issuing_uf').val(person.issuing_uf);
        $('#name_mother').val(person.name_mother);
        $('#name_father').val(person.name_father);
        $('#nationality').val(person.nationality);
        $('#marital_status').val(person.marital_status);
        $('#disabled_person').val(person.disabled_person);
        $('#passport').val(person.passport);
        $('#country_of_origin').val(person.country_of_origin);
        $('#naturalness').val(person.naturalness);
        $('#municipality_of_origin').val(person.municipality_of_origin);
        $('#race').val(person.race);
        $('#cep').val(person.address[0].cep).blur();
    }

    email_data_clear.click(function () {
        registration_id.val("").text("");
        person_id.val("").text("");
        first_name.prop("readonly", false).val("").text("");
        email.prop("readonly", false).val("").text("");
        cpf.show().prop("readonly", false).val("").text("");
        birth.show().prop("readonly", false).val("").text("");
        cell_phone.show().prop("readonly", false).val("").text("");
        cpf_hidden.hide().val("").text("");
        birth_hidden.hide().val("").text("");
        cell_phone_hidden.hide().val("").text("");
    });

    email.focusout(function () {
        if($('#fixed_subscribers').val() == 1) return;
        validateEmail();
    });

    function validateEmail() {
        loading_email.fadeIn();
        //removeSessionAll();
        $.ajax({
            url: "{{ route("Home.Main.validationEmailInscribes") }}/" + email.val() + '/' + "{{ isset($model['id']) ? $model['id'] : '' }}",
            type:"GET",
        })
        .done(function (response) {

        })
        .fail(function (fail) {
            //
        })
        .always(function (always) {
            validatePersonEmail(always);
        });
    }

    function clearDataInputsPersonEmail() {
        registration_id.val("").text("");
        person_id.val("").text("");
        first_name.prop("readonly", false);
        email.prop("readonly", false);
        cpf.prop("readonly", false);
        birth.prop("readonly", false);
        cell_phone.prop("readonly", false);
    }

</script>

<script>

    //Verificação por cpf

    function validateInscribeCPF(always) {
        var inscribeReturn = always.inscribe;
        if(inscribeReturn!==undefined)
        {
            if(inscribeReturn===null)
            {
                inscribe_cpf_error.hide();
                loading_cpf_inscribe.hide();

                var valInsErrorEmail  = inscribe_email_error.is(':visible');
                var valInsErrorCPF  = inscribe_cpf_error.is(':visible');
                if (valInsErrorEmail===false && valInsErrorCPF===false){forward.prop("disabled", false);}

                inscribe_cpf_success.fadeIn();

                return inscribeReturn;
            }
            else
            {
                var inscribe = JSON.stringify(always.inscribe);
                window.sessionStorage.setItem('inscribe', inscribe);
                loading_cpf_inscribe.hide();
                forward.prop("disabled", true);
                inscribe_cpf_error.fadeIn();

                return inscribeReturn;
            }
        }
        else
        {
            return inscribeReturn;
        }
    }

    function validatePersonCPF(always) {
        var personReturn = always.person;

        var valInsErrorEmail  = inscribe_email_error.is(':visible');
        var valInsErrorCPF  = inscribe_cpf_error.is(':visible');
        if (valInsErrorEmail!==true && valInsErrorCPF!==null){forward.prop("disabled", false);}

        if(personReturn!==undefined && personReturn.data)
        {
            var varInscribe = validateInscribeCPF(always);
            var person = JSON.stringify(personReturn);
            window.sessionStorage.setItem('person', person);
            loading_cpf.hide();
            person_cpf_error.hide();
            person_cpf_success.fadeIn();

            var valInsSuccess  = inscribe_cpf_error.is(':visible');
            if (valInsSuccess===true){inscribe_cpf_success.hide();}
            else if(varInscribe===null && valInsSuccess!==true){clearDataInputsPersonCPF();cpf_data_use.show();cpf_data_clear.show();}

            cpf_data_use.click(function () {
                event.preventDefault();
                setDataInputsPersonCPF(getSessionPerson());
            });
        }
        else
        {
            loading_cpf.hide();
            person_cpf_success.hide();
            person_cpf_error.hide();

            if (valInsErrorCPF===true){inscribe_cpf_error.hide(); inscribe_cpf_success.hide();}
        }
    }

    function testaCPF(strCPF) {
        strCPF = strCPF.replace('.', '').replace('.', '').replace('.', '').replace('-', '');
        var Soma;
        var Resto;
        Soma = 0;
        if (strCPF == "00000000000") return false;

        for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11))  Resto = 0;
        if (Resto != parseInt(strCPF.substring(9, 10)) ) return false;

        Soma = 0;
        for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11))  Resto = 0;
        if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false;
        return true;
    }

    function setDataInputsPersonCPF(person) {

        inscribe_email_error.hide();
        inscribe_email_success.hide();
        person_email_success.hide();
        person_email_error.hide();

        for($i=0;$i<person.registrations.length;$i++)
        {
            if(person.registrations[$i].number_registration===number_registration.val())
            {
                var registrationID = person.registrations[$i].id;
            }
        }
        registration_id.val(registrationID);
        person_id.val(person.id);
        first_name.prop("readonly", false).val(person.first_name);
        email.prop("readonly", false).val(person.email);
        birth.prop("readonly", false).val(person.birth);
        cell_phone.prop("readonly", false).val(person.phones[0].formated_phone);

        email_hidden.show().prop("readonly", true).val(person.email);
        email.hide().prop("readonly", true).val(person.email);
        //-----------------------------------------------------------
        cpf.prop("readonly", false).val(person.cpf);
        //-----------------------------------------------------------
        birth_hidden.show().prop("readonly", true).val(person.birth);
        birth.hide().prop("readonly", true).val(person.birth);
        //-----------------------------------------------------------
        cell_phone_filter = person.phones.filter(function (value) {
            return value['type'] == 'cell_phone';
        });
        cell_phone_hidden.show().prop("readonly", true).val(cell_phone_filter[0].formated_phone);
        cell_phone.hide().prop("readonly", true).val(cell_phone_filter[0].formated_phone);
        //-----------------------------------------------------------
        home_phone_filter = person.phones.filter(function (value) {
            return value['type'] == 'home_phone';
        });
        home_phone.prop("readonly", true).val(home_phone_filter[0].formated_phone);

        $('#gender').val(person.gender);
        $('#social_name').val(person.social_name);
        $('#rg').val(person.rg);
        $('#issuing_body').val(person.issuing_body);
        $('#issuing_uf').val(person.issuing_uf);
        $('#name_mother').val(person.name_mother);
        $('#name_father').val(person.name_father);
        $('#nationality').val(person.nationality);
        $('#marital_status').val(person.marital_status);
        $('#disabled_person').val(person.disabled_person);
        $('#passport').val(person.passport);
        $('#country_of_origin').val(person.country_of_origin);
        $('#naturalness').val(person.naturalness);
        $('#municipality_of_origin').val(person.municipality_of_origin);
        $('#race').val(person.race);
        $('#cep').val(person.address[0].cep).blur();

    }

    cpf_data_clear.click(function () {
        registration_id.val("").text("");
        person_id.val("").text("");
        first_name.prop("readonly", false).val("").text("");
        email.show().prop("readonly", false).val("").text("");
        cpf.show().prop("readonly", false).val("").text("");
        birth.show().prop("readonly", false).val("").text("");
        cell_phone.show().prop("readonly", false).val("").text("");
        email_hidden.hide().val("").text("");
        cpf_hidden.hide().val("").text("");
        birth_hidden.hide().val("").text("");
        cell_phone_hidden.hide().val("").text("");
    });


    cpf.focusout(function () {
        validateCPF();
    });

    function validateCPF() {
        inscribe_cpf_error.hide();
        inscribe_cpf_success.hide();
        person_cpf_success.hide();
        person_cpf_error.hide();
        loading_cpf.fadeIn();
        //removeSessionAll();
        isValid = testaCPF(cpf.val());
        if (!isValid) {
            cpf_invalidated.fadeIn();
            loading_cpf.hide();
            return false;
        }else{
            cpf_invalidated.hide();
        }

        $.ajax({
            url: "{{ route("Home.Main.validationCPFInscribes") }}/" + cpf.val() + '/' + "{{ isset($model['id']) ? $model['id'] : '' }}",
            type:"GET",
        })
        .done(function (response) {

        })
        .fail(function (fail) {
            //
        })
        .always(function (always) {
            validatePersonCPF(always);
        });
    }

    function clearDataInputsPersonCPF() {
        registration_id.val("").text("");
        person_id.val("").text("");
        first_name.prop("readonly", false);
        email.prop("readonly", false);
        cpf.prop("readonly", false);
        birth.prop("readonly", false);
        cell_phone.prop("readonly", false);
    }

</script>

<script>

    registration_type.change(function () {
        if($(this).val()==='')
        {
            registration_type.addClass('required');
        }
        else
        {
            registration_type.removeClass('required');
        }
    });

    professional_category.change(function () {
        if($(this).val()!=='')
        {
            professional_category.addClass('required');
        }
        else
        {
            professional_category.removeClass('required');
        }
    });

</script>


<script>
    button_submit.click(function () {
        if (!first_name.val().includes(' ', 0)) {
            alert('Por favor preencha seu nome completo!');
            event.preventDefault();
        }
        let checked = $('#boleto').prop('checked');
        if (checked) {
            loading_payment.fadeIn();
        }
    });

    first_name.focusout(function () {
        if (!first_name.val().includes(' ', 0)) {
            alert('Por favor preencha seu Nome Completo!');
        }
    });
</script>

<script>
        search_cpf.on('keyup', function (e) {
            if (e.keyCode === 13) {
                search_2.click();
            }
        });
        search_2.click(function () {
            removeSessionAll();
            infor_person.text("");
            infor_inscribe.text("");
            infor_boleto.text("");
            infor_status.text("");
            if(search_cpf.val()!='')
            {
                $('#loading').fadeIn();
                $.ajax({
                    url: "{{ route("Home.Main.validationCPFInscribes") }}/" + search_cpf.val() + '/' + "{{ isset($model['id']) ? $model['id'] : '' }}",
                    type:"GET",
                })
                .done(function (response) {
                    //
                })
                .fail(function (fail) {
                    //
                })
                .always(function (always) {
                    responseSearch(always);
                    $('#loading').hide();
                });
        }
    });

    function responseSearch(always) {
        var person_name;
        if (always.person.data) {
            person_name = always.person.data.first_name;
            if(always.inscribe===null)
            {
                var ins = 'ainda não se inscreveu neste evento! Você pode se inscrever clicando <a href="{{ route('Home.Main.show', isset($model['id']) ? $model['id'] : '', isset($model['tag']) ? $model['tag'] : '') }}">aqui</a>';
            }
            else
            {
                let event = always.inscribe.event;
                console.log(event);
                $('.inscribe-data').show();
                $('.full-name').text(person_name);
                if (event.type.name == 'Vestibular') {
                    $('.unity-course').show();
                    $('.infor-vestibular').show();
                    $('.unity').text(always.inscribe.unity == 'IT' ? 'ITAPERUNA' : 'NOVA IGUAÇU');
                    $('.course').text(always.inscribe.course);
                    infor_inscribe.hide();
                }
                var ins = 'sua inscrição foi realizada com sucesso!';
                if (always.inscribe.event.formatted_start == always.inscribe.event.formatted_end) {
                    infor_inscribe.text("O " + always.inscribe.event.name + ' ocorrerá no dia ' +
                        always.inscribe.event.formatted_start);
                }else{
                    infor_inscribe.text("O " + always.inscribe.event.name + ' ocorrerá do dia ' +
                        always.inscribe.event.formatted_start + ' ao dia ' + always.inscribe.event.formatted_end);
                }

                if(event.paid_out == 0){
                    var status = '<strong>Status</strong>: Inscrição Realizada!';
                }else{
                    var status = always.inscribe.status == 3 ? '<strong>Status</strong>: Pago' : '<strong>Status</strong>: Pendente Pagamento';
                }

                infor_status.append(status);

                if (always.inscribe.status != 3 && event.paid_out != 0) {
                    var boleto = "<input id='btn-search' type='submit' value='Imprimir Boleto' class='forward_error'>";
                    infor_boleto.append(boleto);
                }
                responseHidden(always);
            }
            infor_person.append("Olá, " + ins);

        }else{
            infor_person.append("Olá, " + 'ainda não se inscreveu neste evento! Você pode se inscrever clicando <a href="{{ route('Home.Main.show', isset($model['id']) ? $model['id'] : '', isset($model['tag']) ? $model['tag'] : '') }}">aqui</a>');
        }
        scrollingElement = (document.scrollingElement || document.body);
        $(scrollingElement).animate({
            scrollTop: document.body.scrollHeight
        }, 500);
    }

    function responseHidden(always){
        let person = always.person.data;
        let inscribe = always.inscribe;
        let cell_phone;
        let price;
        let address;

        cell_phone = person.phones.filter(function (value) {
            return value['type'] == 'cell_phone';
        });

        if(inscribe.payments.length > 0){
            price = inscribe.payments[0].value;
        }else{
            price = 0.00;
        }

        if(cell_phone.length > 0){
            cell_phone = cell_phone[0].formated_phone;
        }else{
            cell_phone = '(21)99999-9999';
        }

        if(person.address.length > 0){
            address = person.address[0];
        }

        $("input[type='hidden'][name='cell_phone']").val(cell_phone ? cell_phone : null);
        $("input[type='hidden'][name='cpf']").val(person.cpf ? person.cpf : null);
        // $("input[type='hidden'][name='price']").val(price);
        $("input[type='hidden'][name='event_id']").val(inscribe.event_id ? inscribe.event_id : null);
        $("input[type='hidden'][name='event_name']").val(inscribe.event.name ? inscribe.event.name : null);
        $("input[type='hidden'][name='inscribe_id']").val(inscribe.id ? inscribe.id : null);
        $("input[type='hidden'][name='created_at']").val(inscribe.created_at ? inscribe.created_at : null);
        $("input[type='hidden'][name='first_name']").val(person.first_name ? person.first_name : null);
        $("input[type='hidden'][name='email']").val(person.email ? person.email : null);
        $("input[type='hidden'][name='unity']").val(inscribe.unity ? inscribe.unity : null);
        $("input[type='hidden'][name='course']").val(inscribe.course ? inscribe.course : null);

        if (address) {
            $("input[type='hidden'][name='street']").val(address.street ? address.street : null);
            $("input[type='hidden'][name='number']").val(address.number ? address.number : null);
            $("input[type='hidden'][name='district']").val(address.district ? address.district : null);
            $("input[type='hidden'][name='cep']").val(address.cep ? address.cep : null);
            $("input[type='hidden'][name='city']").val(address.city ? address.city : null);
            $("input[type='hidden'][name='state']").val(address.state ? address.state : null);
        }
    }
</script>

{{-- VESTIBULAR --}}
<script>
    $('#unity').change(function () {
        $('#course-it, #course-ni, #course-void').hide();
        $('#course-it select, #course-ni select, #course-void select').val('').removeClass('required');
        $('#course-it select, #course-ni select, #course-void select').attr('name', 'no_course');
        switch ($(this).val()) {
            case 'IT':
                $('#course-it').show();
                $('#course-it select').addClass('required');
                $('#course-it select').attr('name', 'course');
                break;

            case 'NI':
                $('#course-ni').show();
                $('#course-ni select').addClass('required');
                $('#course-ni select').attr('name', 'course');
                break;

            default:
                $('#course-void').show();
                $('#course-void select').addClass('required');
                $('#course-void select').attr('name', 'course');
        }
    });
</script>

<script>
    //PESQUISA DE CPF NA API EXTERNA
    $("#cep").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Preenche os campos com "..." enquanto consulta webservice.
            $("#rua").val("...");
            $("#bairro").val("...");
            $("#cidade").val("...");
            $("#uf").val("...");
            $("#ibge").val("...");

            //Consulta o webservice viacep.com.br/
            $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                if (!("erro" in dados)) {
                    //Atualiza os campos com os valores da consulta.
                    $("#street").val(dados.logradouro);
                    $("#district").val(dados.bairro);
                    $("#city").val(dados.localidade);
                    $("#state").val(dados.uf);
                    $("#complement").val(dados.complemento);
                }
                else {
                    alert("CEP não encontrado.");
                }
            });
        }
    });
</script>


