<?php

namespace App\Models\Configuration;

use App\Observers\Logs\InterceptCrudObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PaymentInscribes extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'payment_inscribes';

    protected $fillable = [
        'inscribe_id',
        'type',
        'name',
        'value',
        'inscribe_date',
        'payment_date',
        'expiry_date',
        'link_boleto',
        'code',
        'status',
        'email_confirmed',
        'manual_payment_reason'
    ];

    public static function boot()
    {
        parent::boot();

        static::observe(new InterceptCrudObserver);
    }

    public function inscribe()
    {
        return $this->hasMany(\App\Models\Configuration\Inscribes::class, 'id', 'inscribe_id');
    }

    //---------------------------------------------------------------------------------------------
    public function create_user()
    {
        return $this->hasOne(\App\User::class, 'id', 'create_user_id');
    }
    public function update_user()
    {
        return $this->hasOne(\App\User::class, 'id', 'update_user_id');
    }
    public function delete_user()
    {
        return $this->hasOne(\App\User::class, 'id', 'delete_user_id');
    }
    //---------------------------------------------------------------------------------------------

}
