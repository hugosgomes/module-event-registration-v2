<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="{{ route('Panel.Main.index') }}" class="brand-link">
        <img src="{{ asset('PanelP/img/logoDefault.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">{{ env('APP_NAME') }}</span>
    </a>
    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ auth()->user()->avatar ? asset("PanelP/dist/img/".auth()->user()->avatar) : asset('PanelP/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ isset(auth()->user()->name) ? auth()->user()->name : 'Usuário do Sistema' }}</a>
            </div>
        </div>
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                @can('Panel.Main.update')

                    <li class="nav-item has-treeview">

                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-cogs"></i>
                            <p>
                                Sistema
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>

                        <ul class="nav nav-treeview">

                            @can('Panel.Users.index')
                                <li class="nav-item">
                                    <a href="{{ route('Panel.Users.index') }}" class="nav-link">
                                        <i class="fa fa-users nav-icon"></i>
                                        <p>Usuários</p>
                                    </a>
                                </li>
                            @endcan

                            @can('Panel.Roles.index')
                                <li class="nav-item">
                                    <a href="{{ route('Panel.Roles.index') }}" class="nav-link">
                                        <i class="fa fa-user-check nav-icon"></i>
                                        <p>Funções</p>
                                    </a>
                                </li>
                            @endcan

                            @can('Panel.Permissions.index')
                                <li class="nav-item">
                                    <a href="{{ route('Panel.Permissions.index') }}" class="nav-link">
                                        <i class="fa fa-lock nav-icon"></i>
                                        <p>Permissões</p>
                                    </a>
                                </li>
                            @endcan

                            @can('Panel.Errors.index')
                                <li class="nav-item">
                                    <a href="{{ route('Panel.Errors.index') }}" class="nav-link">
                                        <i class="fa fa-bug nav-icon"></i>
                                        <p>Erros</p>
                                    </a>
                                </li>
                            @endcan

                        </ul>

                    </li>

                @endcan

                @can('Panel.CmsPages.index')

                    <li class="nav-item has-treeview">

                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-edit"></i>
                            <p>
                                CMS das Páginas
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>

                        <ul class="nav nav-treeview">

                            @can('Panel.CmsPages.index')
                                <li class="nav-item">
                                    <a href="{{ route('Panel.CmsPages.index') }}" class="nav-link">
                                        <i class="fa fa-edit nav-icon"></i>
                                        <p>CMS das Páginas</p>
                                    </a>
                                </li>
                            @endcan

                        </ul>

                    </li>

                @endcan

                @can('Panel.Inscribes.index')

                    <li class="nav-item has-treeview">

                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-edit"></i>
                            <p>
                                Inscrições
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>

                        <ul class="nav nav-treeview">

                            @can('Panel.Inscribes.index')
                                <li class="nav-item">
                                    <a href="{{ route('Panel.Inscribes.index') }}" class="nav-link">
                                        <i class="fa fa-edit nav-icon"></i>
                                        <p>Ver Inscrições</p>
                                    </a>
                                </li>
                            @endcan

                            @can('Panel.Inscribes.report')
                                <li class="nav-item">
                                    <a href="{{ route('Panel.Inscribes.report') }}" class="nav-link">
                                        <i class="fa fa-edit nav-icon"></i>
                                        <p>Relatório</p>
                                    </a>
                                </li>
                            @endcan

                        </ul>

                    </li>

                @endcan

            </ul>
        </nav>
    </div>
</aside>
