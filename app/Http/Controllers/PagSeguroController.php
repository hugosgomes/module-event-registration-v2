<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Configuration\PaymentInscribes;
use App\PagSeguro\PagSeguro;
use Illuminate\Http\Request;

class PagSeguroController extends Controller
{
    protected                          $configurations;
    protected                          $paymentInscribes;

    public function __construct
    (
        Request                         $request,
        PagSeguro                       $pagseguro,
        PaymentInscribes                $paymentInscribes
    )
    {
        $this->request          =       $request;
        $this->pagseguro        =       $pagseguro;
        $this->paymentInscribes =       $paymentInscribes;
    }

    public function notification(Request $request){
        $this->pagseguro->notification($request);
    }

    public function boleto(Request $request){
        $this->setPrice($request);
        $data = $request->all();
        $boleto = $this->pagseguro->boleto($request);
        if ($boleto == 'error') {
            $error = "Não foi possível gerar o boleto";
            return back()->with('error', $error)->withErrors('error', $error);
        }
        $code = (string)$boleto->code;
        $link = (string)$boleto->paymentLink;
        $this->paymentInscribes::where('inscribe_id', $data['inscribe_id'])->delete();
        //-----------------------------------------------------------------------
        $payment = $this->paymentInscribes::create([
            'inscribe_id'      => $data['inscribe_id'],
            'type'             => 'boleto',
            'name'             => $data['event_name'],
            'value'            => $request['price'],
            'inscribe_date'    => $data['created_at'],
            'expiry_date'      => now()->addDays(3),
            'link_boleto'      => $link,
            'code'             => $code,
            'status'           => 1,
            'email_confirmed'  => 0
        ]);

        return redirect($payment->link_boleto);
    }

    public function setPrice(Request $request){
        if($request['price'] == null){
            $price_1 = isset($request['professional_category'])  ? $request['professional_category'] : null;
            if($price_1  ==   null      ? $price_2  ='20.00' : '');
            if($price_1  ==   'ag_20'   ? $price_2  ='20.00' : '');
            if($price_1  ==   'apl_20'  ? $price_2  ='30.00' : '');
            if($price_1  ==   'aps_50'  ? $price_2  ='50.00' : '');
            if($price_1  ==   'pp_60'   ? $price_2  ='60.00' : '');
        }else{
            $price_2 = $request['price'];
        }
        $request['price'] = $price_2;
    }
}
