<?php

use App\Events\Users\UserRegister;
use App\Models\Configuration\CmsPages;
use App\Models\Configuration\Permissions;
use App\Models\Configuration\Roles;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    //------------------------------------------------------------------------------------------------------------------
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(CmsPagesTableSeeder::class);
    }
}
