<section class="page-content container-fluid">
    <div class="row">
        <div class="col">
            <div class="card">
                <h5 class="card-header">Editando {{ $crudName }}</h5>
                <form class="form-horizontal" id="validate-form" method="POST" action="{{ route("$route[0].$route[1].update") }}" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    <input type="hidden" name="id" value="{{ $item['id'] }}">

                    <div class="card-body">

                        <div class="form-body">
                            <div class="form-group row">
                                <label class="control-label text-right col-md-3">Nome Completo</label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="Digite o Nome Completo" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $item['name'] }}">
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label text-right col-md-3">E-mail</label>
                                <div class="col-md-5">
                                    <input type="email" placeholder="Digite o e-mail" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $item['email'] }}">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <hr class="dashed ">
                            <div class="form-group row">
                                <label class="control-label text-right col-md-3">Digite a enha</label>
                                <div class="col-md-5">
                                    <input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Digite a Senha" name="password" value="{{ $item['password'] }}" minlength="8">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <hr class="dashed">

                            <div class="form-group row">
                                <label class="control-label text-right col-md-3">Função</label>
                                <div class="col-md-5">
                                    <select class="form-control select2 {{ $errors->has('role_id') ? ' is-invalid' : '' }}" name="role_id" required>
                                        <option value="{{ $item->roles[0]['id'] }}" selected>{{ $item->roles[0]['name'] }}</option>

                                        @foreach($roles as $role)
                                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                                        @endforeach

                                    </select>
                                    @if ($errors->has('role_id'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('role_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label text-right col-md-3">Visualização de Evento</label>
                                <div class="col-md-5">
                                    <select class="form-control select2 {{ $errors->has('profile_event') ? ' is-invalid' : '' }}" name="profile_event" required>
                                        <option value="">Todos os Eventos</option>

                                        @foreach($events as $event)
                                            <option value="{{ $event['id'] }}" {{$event['id'] == $item->profile_event ? 'selected' : ''}}>{{ $event['name'] }}</option>
                                        @endforeach

                                    </select>
                                    @if ($errors->has('profile_event'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('profile_event') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer bg-light">
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="offset-sm-3 col-md-5">
                                            <button type="submit" class="btn btn-primary btn-rounded submit">Atualizar</button>
                                            <a href="{{ route("$route[0].$route[1].index") }}" class="btn btn-secondary clear-form btn-rounded btn-outline">Cancelar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
