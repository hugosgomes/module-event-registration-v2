<?php

namespace App\Validations\Panel\Users;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ValidatorUsers
{
    //Variáveis Globais

    protected                     $request;
    protected                     $model;

    //Funções Padrões

    public function __construct
    (
        Request                   $request,
        User                      $model
    )
    {
        $this->request =          $request;
        $this->model =            $model;
    }


    //Funções Principais

    public function index(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function show(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function create(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name'                       => ['required', 'string', 'max:191'],
                'email'                      => ['required', 'string', 'email', 'max:191', 'unique:users'],
                'password'                   => ['required', 'string', 'min:4', 'confirmed'],
                'role_id'                    => ['required'],
            ]
        );
        return $validator;
    }

    public function edit(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function update(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'id'                         => [
                    'required',
                    Rule::exists('users')
                        ->where('id', $request->get('id'))
                        ->whereNull('deleted_at'),
                ],
                'name'                       => ['required', 'string', 'max:191'],
                'password'                   => ['required', 'string', 'min:8'],
                'role_id'                    => ['required'],
            ]
        );
        return $validator;
    }

    public function delete(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'id' => [
                    'required',
                    Rule::exists('users')
                        ->where('id', $request->get('id'))
                        ->whereNull('deleted_at'),
                ],
            ]
        );
        return $validator;
    }

    public function restore(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }
}
