<!-- COMMON SCRIPTS -->
<script src="{{ asset('HomeP/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('HomeP/js/common_scripts.min.js') }}"></script>
<script src="{{ asset('HomeP/js/velocity.min.js') }}"></script>
<script src="{{ asset('HomeP/js/functions.js') }}"></script>

<!-- SPECIFIC SCRIPTS -->
<script src="{{ asset('HomeP/js/parallax.min.js') }}"></script>
<script src="{{ asset('HomeP/js/owl-carousel.js') }}"></script>
<script>
    "use strict";
    $(".team-carousel").owlCarousel({
        items: 1,
        loop: false,
        margin: 10,
        autoplay: false,
        smartSpeed: 300,
        responsiveClass: false,
        responsive: {
            320: {
                items: 1,
            },
            768: {
                items: 2,
            },
            1000: {
                items: 3,
            }
        }
    });
</script>
<!-- ================== PAGE LEVEL SCRIPTS ==================-->
@includeIf("$route[0].$route[1].Organizar.javascriptLocal")
