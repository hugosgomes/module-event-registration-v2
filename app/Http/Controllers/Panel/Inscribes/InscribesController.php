<?php

namespace App\Http\Controllers\Panel\Inscribes;

use App\Http\Controllers\Controller;
use App\Models\Configuration\Configurations;
use App\Response\Response;
use App\Services\Panel\Inscribes\ServiceInscribes;
use Illuminate\Http\Request;
use Gate;

class InscribesController extends Controller
{
    protected                          $configurations;
    protected                          $request;
    protected                          $routeArray;
    protected                          $service;

    public function __construct
    (
        Configurations                 $configurations,
        Request                        $request,
        ServiceInscribes                $service
    )
    {
        $this->middleware('auth');
        $this->configurations =        $configurations;
        $this->request =               $request;
        $this->routeArray =            $this->configurations->getRouteName($request);
        $this->service =               $service;
    }

    /*
     *
     * Funções Principais
     *
     */
    public function index(Request $request)
    {
        $index = $this->service->index($request);
        if($index['data']!=false)
        {
            return view($index['data']['route']['view'], $index['data']['compact']);
        }
        else
        {
            return back()->with('error', 'Acesso negado')->withErrors('error', 'Acesso negado');
        }
    }

    public function show(Request $request)
    {
        $show = $this->service->show($request);
        if($show['data']!=false)
        {
            return view($show['data']['route']['view'], $show['data']['compact']);
        }
        else
        {
            return back()->with('error', 'Acesso negado')->withErrors('error', 'Acesso negado');
        }
    }

    public function create(Request $request)
    {
        $create = $this->service->create($request);
        if($create['data']!=false)
        {
            return view($create['data']['route']['view'], $create['data']['compact']);
        }
        else
        {
            return back()->with('error', 'Acesso negado')->withErrors('error', 'Acesso negado');
        }
    }

    public function store(Request $request)
    {
        $store = $this->service->store($request);
        if($store['data']!=false)
        {
            if($store['status']=='success')
            {
                return redirect($store['data']['compact']['redirect'])
                    ->with($store['status'], $store['msg'])
                    ->with($store['status'], $store['msg']);
            }
            return back()->with($store['status'], $store['msg'])
                ->with('request', $store['request']->all())
                ->withErrors($store['data']);
        }
        else
        {
            return back()->with('error', 'Acesso negado')->withErrors('error', 'Acesso negado');
        }
    }

    public function edit(Request $request, $id)
    {
        $request['id'] = $id;
        $edit = $this->service->edit($request);
        if($edit['data']!=false)
        {
            return view($edit['data']['route']['view'], $edit['data']['compact']);
        }
        else
        {
            return back()->with('error', 'Acesso negado')->withErrors('error', 'Acesso negado');
        }
    }

    public function update(Request $request)
    {
        $update = $this->service->update($request);
        if($update['data']!=false)
        {
            if($update['status']=='success')
            {
                return back()->with($update['status'], $update['msg'])->with($update['status'], $update['msg']);
            }
            return back()->with($update['status'], $update['msg'])
                ->with('request', $update['request']->all())
                ->withErrors($update['data']);
        }
        else
        {
            return back()->with('error', 'Acesso negado')->withErrors('error', 'Acesso negado');
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->service->delete($request);
        if($delete['data']!=false)
        {
            if($delete['status']=='success')
            {
                return back()->with($delete['status'], $delete['msg'])
                    ->with($delete['status'], $delete['msg']);
            }
            return back()->with($delete['status'], $delete['msg'])
                ->with('request', $delete['request']->all())
                ->withErrors($delete['data']);
        }
        else
        {
            return back()->with('error', 'Acesso negado')->withErrors('error', 'Acesso negado');
        }
    }

    public function restore(Request $request)
    {
        $restore = $this->service->restore($request);
        if($restore['data']!=false)
        {
            if($restore['status']=='success')
            {
                return back()->with($restore['status'], $restore['msg'])
                    ->with($restore['status'], $restore['msg']);
            }
            return back()->with($restore['status'], $restore['msg'])
                ->with('request', $restore['request']->all())
                ->withErrors($restore['data']);
        }
        else
        {
            return back()->with('error', 'Acesso negado')->withErrors('error', 'Acesso negado');
        }
    }

    public function report(Request $request)
    {
        $report = $this->service->report($request);
        if($report['data']!=false)
        {
            return view($report['data']['route']['view'], $report['data']['compact'])->with(['request' => $request]);
        }
        else
        {
            return back()->with('error', 'Acesso negado')->withErrors('error', 'Acesso negado');
        }
    }

    public function import(Request $request)
    {
        $import = $this->service->import($request);
        if($import['data']!=false)
        {
            if($import['status']=='success')
            {
                return back()
                    ->with($import['status'], $import['msg'])
                    ->with($import['status'], $import['msg']);
            }
            return back()->with($import['status'], $import['msg'])
                ->with('request', $import['request']->all())
                ->withErrors($import['data']);
        }
        else
        {
            return back()->with('error', 'Acesso negado')->withErrors('error', 'Acesso negado');
        }
    }

    public function export(Request $request)
    {
        $export = $this->service->export($request);
        $file = public_path($export['data']['compact']['model']);
        $headers = [];
        if($export['data']!=false)
        {
            if($export['status']=='success')
            {
                return response()->download($file, 'Relatório_Inscritos_' . date("Y-m-d") . ".csv", $headers)->deleteFileAfterSend(true);
            }
            return back()->with($export['status'], $export['msg'])
                ->with('request', $export['request']->all())
                ->withErrors($export['data']);
        }
        else
        {
            return back()->with('error', 'Acesso negado')->withErrors('error', 'Acesso negado');
        }
    }
}
