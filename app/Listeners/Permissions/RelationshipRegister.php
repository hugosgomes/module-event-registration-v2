<?php

namespace App\Listeners\Permissions;

use App\Events\Permissions\PermissionsRegister;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RelationshipRegister
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PermissionsRegister  $event
     * @return void
     */
    public function handle(PermissionsRegister $event)
    {
        $event->permission->roles()->sync($event->roles_ids);
    }
}
