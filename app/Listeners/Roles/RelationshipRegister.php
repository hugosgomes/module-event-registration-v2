<?php

namespace App\Listeners\Roles;

use App\Events\Roles\RolesRegister;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RelationshipRegister
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RolesRegister  $event
     * @return void
     */
    public function handle(RolesRegister $event)
    {
        $event->role->permissions()->sync($event->permissions_ids);
    }
}
