<?php

namespace App\Models\Configuration;

use App\Observers\Logs\InterceptCrudObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Inscribes extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'inscribes';

    protected $fillable = [
        'registration_id',
        'person_id',
        'event_id',
        'status',
        'kit',
        'unity',
        'course',
    ];

    public static function boot()
    {
        parent::boot();

        static::observe(new InterceptCrudObserver);
    }

    public function payments()
    {
        return $this->hasMany(\App\Models\Configuration\PaymentInscribes::class, 'inscribe_id', 'id');
    }

    //---------------------------------------------------------------------------------------------
    public function create_user()
    {
        return $this->hasOne(\App\User::class, 'id', 'create_user_id');
    }
    public function update_user()
    {
        return $this->hasOne(\App\User::class, 'id', 'update_user_id');
    }
    public function delete_user()
    {
        return $this->hasOne(\App\User::class, 'id', 'delete_user_id');
    }
    //---------------------------------------------------------------------------------------------

}
