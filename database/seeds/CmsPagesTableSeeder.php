<?php

use App\Events\Users\UserRegister;
use App\Models\Configuration\CmsPages;
use App\Models\Configuration\Permissions;
use App\Models\Configuration\Roles;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CmsPagesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('cms_pages')->delete();
        $cms1 = CmsPages::create([
            'page_name'                 =>'Home Principal',
            'logo1'                     =>'default',
            'facebook'                  =>'facebook',
            'twitter'                   =>'twitter',
            'session_1_img_background'  =>'default',
            'session_1_title'           =>'Primeiro Título',
            'session_1_subtitle'        =>'Usu habeo equidem sanctus no ex melius labitur conceptam eos',
            'session_2_img_right'       =>'default',
            'session_2_title'           =>'Vim ridens eleifend referrentur eu',
            'session_2_content'         =>'Ex graeco nostrud theophrastus nam, cum tibique reprimique ad. Mea omittam electram te, eu cum fastidii sapientem delicatissimi.',
            'session_3_title'           =>'MEET WILIO TEAM',
            'session_3_subtitle'        =>'Usu habeo equidem sanctus no. Suas summo id sed, erat erant oporteat cu pri. In eum omnes molestie. Sed ad debet scaevola, ne mel lorem legendos.',
        ]);
    }
}
