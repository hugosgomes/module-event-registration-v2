<?php

namespace App\Scopes\ListAll;

use App\Managers\ManagerUser;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class ListAllScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $user = app(ManagerUser::class)->getUserIdentify();
        $ListAll = app(ManagerUser::class)->getUserListAllIdentify();

        $builder->where(function ($query) use ($user, $ListAll){

            if($user->roles[0]->name=='Administrator' || $ListAll==true)
                $query->where('id', '!=', 0);

            if($user->roles[0]->name!='Administrator' && $ListAll==false)
                $query->where('create_user_id', $user->id);

        });

    }
}
