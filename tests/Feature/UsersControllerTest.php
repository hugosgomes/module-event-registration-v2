<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class UsersControllerTest extends TestCase
{

    use DatabaseTransactions;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    // public function testUsersInfor()
    // {
    //     Método não existente no controller
    // }

    public function testUsersIndex()
    {
        $user = User::find(1);
        $response = $this->actingAs($user)->get(route('Panel.Users.index'));
        $response->assertSuccessful();
    }

    // public function testUsersShow()
    // {
    //     View não existe
    // }

    public function testUsersCreate()
    {
        $user = User::find(1);
        $response = $this->actingAs($user)->get(route('Panel.Users.create'));
        $response->assertSuccessful();
    }

    public function testUsersEdit()
    {
        $user = User::find(1);
        $userEdit = factory(User::class)->create();
        $userEdit->save();
        $userEdit->roles()->sync([1]);
        $response = $this->actingAs($user)->get(route('Panel.Users.edit', ['id' => $userEdit->id]));
        $response->assertSuccessful();
    }

    public function testUsersStore()
    {
        $user = User::find(1);
        $userStore = [
            'name' => 'Test User',
            'email' => 'test@user',
            'password' => '12345678',
            'password_confirmation' => '12345678',
            'role_id' => 1,
        ];
        $response = $this->actingAs($user)->post(route('Panel.Users.store', $userStore));
        $response->assertSessionHas('success');
        $this->assertDatabaseHas('users', ['email' => $userStore['email']]);
    }

    public function testUsersUpdate()
    {
        $user = User::find(1);
        $userUpdate = [
            'id' => 2,
            'name' => 'Test User',
            'email' => 'test@user',
            'password' => '12345678',
            'password_confirmation' => '12345678',
            'role_id' => 1,
        ];
        $response = $this->actingAs($user)->post(route('Panel.Users.update', $userUpdate));
        $response->assertSessionHas('success');
        $this->assertDatabaseHas('users', ['name' => $userUpdate['name']]);
    }

    public function testUsersDelete()
    {
        $user = User::find(1);
        $userDelete = factory(User::class)->create();
        $userDelete->save();
        $response = $this->actingAs($user)->get(route('Panel.Users.delete', $userDelete->toArray()));
        $response->assertSessionHas('success');
        $this->assertSoftDeleted('users', ['id' => $userDelete->id]);
    }

    // public function testUsersTrashed()
    // {
    //      Método não existente no controller
    // }

    public function testUsersRestore()
    {
        $user = User::find(1);
        $userRestore = factory(User::class)->create();
        $userRestore->save();
        $userRestore->delete();
        $response = $this->actingAs($user)->get(route('Panel.Users.restore', $userRestore->toArray()));
        $response->assertSessionHas('success');
        $this->assertDatabaseHas('users', [
            'id' => $userRestore->id,
            'deleted_at' => null
        ]);
    }
}
