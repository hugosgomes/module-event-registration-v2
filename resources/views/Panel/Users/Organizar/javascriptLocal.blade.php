<script src="{{ asset('PanelP/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('PanelP/plugins/datatables/dataTables.bootstrap4.js') }}"></script>
<!--Data Tables js-->
<script src="{{ asset('PanelP/assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('PanelP/assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('PanelP/assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('PanelP/assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('PanelP/assets/plugins/bootstrap-datatable/js/jszip.min.js') }}"></script>
<script src="{{ asset('PanelP/assets/plugins/bootstrap-datatable/js/pdfmake.min.js') }}"></script>
<script src="{{ asset('PanelP/assets/plugins/bootstrap-datatable/js/vfs_fonts.js') }}"></script>
<script src="{{ asset('PanelP/assets/plugins/bootstrap-datatable/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('PanelP/assets/plugins/bootstrap-datatable/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('PanelP/assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js') }}"></script>
<script>
    $(function () {
        $('#example2').DataTable({
            dom: 'Bfrtip',
            buttons:
                [
                    {
                        extend:    'copy',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-file"> | Copiar</i>',
                        titleAttr: 'Copiar para Área de Transferência',
                    },
                    {
                        extend:    'excel',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-file-excel"> | Exp. Excel</i>',
                        titleAttr: 'Exportar para Arquivo  de Excel'
                    },
                    {
                        extend:    'pdf',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-file-pdf"> | Exp. PDF</i>',
                        titleAttr: 'Exportar para Arquivo  de PDF'
                    },
                    {
                        extend:    'csv',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-file-csv"> | Exp. CSV</i>',
                        titleAttr: 'Exportar para Arquivo  de CSV'
                    },
                    {
                        extend:    'print',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-print"> | Imprimir</i>',
                        titleAttr: 'Imprimir esta tabela'
                    },
                    @can("$route[0].$route[1].create")
                    {
                        className: 'btn btn-primary',
                        text: '<i class="fa fa-plus"> | Criar Novo</i>',
                        action: function ( e, dt, node, config ) {
                            window.location='{{ route("$route[0].$route[1].create") }}';
                        },
                        titleAttr: "Cadastrar {{ $crudName }}"
                    },
                    @endcan
                    {
                        extend:    'colvis',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-cogs"> | Config. Tab.</i>',
                        titleAttr: 'Selecionar Colunas'
                    },
                ],
            stateSave: true,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "pagingType": "full_numbers",
            "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            }
        });
    });
</script>


<script>
    $(function () {
        $('#example3').DataTable({
            dom: 'Bfrtip',
            buttons:
                [
                    {
                        extend:    'copy',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-file"> | Copiar</i>',
                        titleAttr: 'Copiar para Área de Transferência',
                    },
                    {
                        extend:    'excel',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-file-excel"> | Exp. Excel</i>',
                        titleAttr: 'Exportar para Arquivo  de Excel'
                    },
                    {
                        extend:    'pdf',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-file-pdf"> | Exp. PDF</i>',
                        titleAttr: 'Exportar para Arquivo  de PDF'
                    },
                    {
                        extend:    'csv',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-file-csv"> | Exp. CSV</i>',
                        titleAttr: 'Exportar para Arquivo  de CSV'
                    },
                    {
                        extend:    'print',
                        className: 'btn btn-primary',
                        text:      '<i class="fa fa-print"> | Imprimir</i>',
                        titleAttr: 'Imprimir esta tabela'
                    },
                ],
            stateSave: true,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "pagingType": "full_numbers",
            "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            }
        });
    });
</script>
