<!-- Modal terms -->
<div class="modal fade" id="policy-txt" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
    <div class="modal-dialog modal-xx">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="termsLabel">Política de Privacidade</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <p>Lorem ipsum dolor sit amet, in porro albucius qui, in <strong>nec quod novum accumsan</strong>, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt sensibus.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="policy">Aceito</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Não Aceito</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal terms -->
<div class="modal fade" id="terms-txt" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
    <div class="modal-dialog modal-xx">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="termsLabel">Termos de Serviço</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                    <p>O candidato(a), ao inscrever-se no Processo de Seleção da UNIG, deve declarar se concorda ou não com as condições exigidas para efetuar a inscrição, conforme descritas abaixo:</p>
                    <p>Declaro ter pleno conhecimento das normas e disposições contidas no Edital e Manual do Candidato, que estão disponíveis no site (www.unig.br) e nos campi da UNIG, para o ingresso em um dos cursos superiores ofertados pela UNIG em seus campi.</p>

                    <p>Declaro estar ciente do que determina a Portaria MEC nº 391 (de 07/02/2002), a qual determina que os candidatos aos processos seletivos para o acesso ao ensino superior, devem ter concluído o Ensino Médio (antigo 2º grau) ou curso equivalente.</p>

                    <p>Declaro estar ciente de que em caso de aprovação no Processo de Seleção, fico obrigado(a) a apresentar nos prazos de matrícula estabelecidos no Manual do Candidato, todos os documentos exigidos para efetivação da mesma, sob pena de perder o direito a matrícula em função do não atendimento da apresentação da documentação comprobatória exigida pela legislação educacional.</p>

                    <p>Declaro estar ciente de que a inscrição só será confirmada mediante o pagamento da taxa de inscrição estipulada no Edital.</p>

                    <p>Observação: Os casos omissos, e não previstos no Edital e no Manual do Candidato, serão deliberados pela COMSE (Comissão de Exame de Seleção) e Reitoria da UNIG.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="terms">Aceito</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Não Aceito</button>
            </div>
            <br><br>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
