<?php

namespace App;

use App\Models\Configuration\Permissions;
use App\Observers\Logs\InterceptCrudObserver;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\PasswordReset;

class User extends Authenticatable
{
    use Notifiable;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'users';

    protected $fillable = [
        'name', 'email', 'password', 'system', 'profile_event'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function boot()
    {
        parent::boot();

        static::observe(new InterceptCrudObserver);
    }

    /*
     *
     * Reset Password
     *
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordReset($token));
    }

    /*
     *
     * Funções auxiliares
     *
    */
    public function hasPermission(Permissions $permission)
    {
        return $this->hasAnyRoles($permission->roles);
    }
    public function hasAnyRoles($roles)
    {
        if( is_array($roles) || is_object($roles) ) {
            return !! $roles->intersect($this->roles)->count();
        }

        return $this->roles->contains('name', $roles);
    }

    /*
     *
     * Relacionamentos
     *
    */
    public function roles()
    {
        return $this->belongsToMany(\App\Models\Configuration\Roles::class, 'role_user', 'user_id', 'role_id')->withoutGlobalScopes();
    }

    //---------------------------------------------------------------------------------------------
    public function create_user()
    {
        return $this->hasOne(\App\User::class, 'id', 'create_user_id');
    }
    public function update_user()
    {
        return $this->hasOne(\App\User::class, 'id', 'update_user_id');
    }
    public function delete_user()
    {
        return $this->hasOne(\App\User::class, 'id', 'delete_user_id');
    }
    //---------------------------------------------------------------------------------------------

}
