<section class="content">
    <form action="{{route('Panel.Inscribes.report')}}" method="get">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title"><h3>Relatório de {{ $crudName }}</h3></div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-2">
                                <label class="control-label col-md-4">Evento</label>
                                <select class="form-control select2" id="event_id" name="event_id" required {{$disabled}}>
                                <option value="">Todos</option>
                                    @foreach ($events as $event)
                                        <option value="{{$event['id']}}" {{ $request['event_id'] == $event['id'] ? 'selected' : '' }}>{{$event['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @if ($statistics)
                                <div class="col-2">
                                    <label class="control-label col-md-4">Unidade</label>
                                    <select class="form-control select2" id="unity" name="unity" required {{$disabled}}>
                                        <option value="IT" {{$request['unity'] == 'IT' || !$request['unity'] ? 'selected' : ''}}>ITAPERUNA</option>
                                        <option value="NI" {{$request['unity'] == 'NI' ? 'selected' : ''}}>NOVA IGUAÇU</option>
                                    </select>
                                </div>
                            @endif
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-3 col-6">
                                <!-- small box -->
                                <div class="small-box bg-info">
                                    <div class="inner">
                                        <h3>{{ $model['totalInscribes'] }}</h3>
                                        <p>Total de Inscritos</p>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-bag"></i>
                                    </div>
                                    <a href="{{route('Panel.Inscribes.index')}}" class="small-box-footer">Mais Informações <i class="fas fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box bg-success">
                                <div class="inner">
                                    <h3>{{ $model['totalInscribesPaidOut'] }}</h3>
                                    <p>Total de Inscritos Pagos</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-stats-bars"></i>
                                </div>
                                <a href="{{route('Panel.Inscribes.index', ['status' => 3])}}" class="small-box-footer">Mais Informações <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box bg-warning">
                                <div class="inner">
                                    <h3>{{ $model['totalInscribesNoPaidOut'] }}</h3>

                                    <p>Total de Inscritos Não Pagos</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="{{route('Panel.Inscribes.index', ['status' => 1])}}" class="small-box-footer">Mais Informações <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if ($statistics)
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <h3>Relatório Estatístico - Vestibular</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Cursos/Turno</th>
                                            <th>Pagos</th>
                                            <th>Não Pagos</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($statistics as $statistic)
                                            <tr>
                                                <td>{{$statistic->course}}</td>
                                                <td>{{$statistic->pagos}}</td>
                                                <td>{{$statistic->nao_pagos}}</td>
                                                <td>{{$statistic->inscritos}}</td>
                                            </tr>
                                        @endforeach
                                        <tfoot>
                                            <tr>
                                                <td>Totais</td>
                                                <td>{{$statisticsSum->pagos}}</td>
                                                <td>{{$statisticsSum->nao_pagos}}</td>
                                                <td>{{$statisticsSum->inscritos}}</td>
                                            </tr>
                                        </tfoot>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </form>
</section>
