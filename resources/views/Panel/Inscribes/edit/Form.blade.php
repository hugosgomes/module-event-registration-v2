<section class="page-content container-fluid">
    <div class="row">
        <div class="col">
            <div class="card">
                <h5 class="card-header">Editando {{ $crudName }}</h5>
                {{-- {{dd($model->person['address'])}} --}}
                <form class="form-horizontal" id="validate-form" method="POST" action="{{ route("$route[0].$route[1].update") }}" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    <input type="hidden" name="id" value="{{ $model->id }}">
                    <input type="hidden" name="person_id" value="{{ $model->person['id'] }}">

                    <div class="card-body">

                        <div class="form-body">
                            <div class="form-body">

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Evento</label>
                                    <div class="col-md-5">
                                        <select class="form-control select2 {{ $errors->has('event_id') ? ' is-invalid' : '' }}" name="event_id" required readonly>
                                            @foreach($events as $event)
                                                <option value="{{ $event['id'] }}" {{$event['id'] == $model->event_id ? 'selected' : ''}}>{{ $event['name'] }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('event_id'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('event_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Status</label>
                                    <div class="col-md-5">
                                        <select class="form-control select2 name="status" required readonly>
                                            <option value="3" {{$model['status'] == 3 ? 'selected' : ''}}>Pago</option>
                                            <option value="1" {{$model['status'] == 1 ? 'selected' : ''}}>Pendente</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Kit Entregue</label>
                                    <div class="col-md-5">
                                        <select class="form-control select2" name="kit" required>
                                            <option value="1" {{$model['kit'] == 1 ? 'selected' : ''}}>Sim</option>
                                            <option value="0" {{$model['kit'] == 0 ? 'selected' : ''}}>Não</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Nome Completo</label>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" autofocus name="first_name" value="{{ $item->person['first_name'] }}" required>
                                        @if ($errors->has('first_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Matrícula</label>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control {{ $errors->has('number_registration') ? ' is-invalid' : '' }}" autofocus name="number_registration" value="{{ $item->person['registrations'][0]['number_registration'] ?? null }}" readonly>
                                        @if ($errors->has('number_registration'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('number_registration') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">CPF</label>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control cpf {{ $errors->has('cpf') ? ' is-invalid' : '' }}" autofocus name="cpf" value="{{ $item->person['cpf'] }}" required>
                                        @if ($errors->has('cpf'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('cpf') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Email</label>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" autofocus name="email" value="{{ $item->person['email'] }}" required>
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Data de Nascimento</label>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control birth{{ $errors->has('birth') ? ' is-invalid' : '' }}" autofocus name="birth" value="{{ $item->person['birth'] }}" required>
                                        @if ($errors->has('birth'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('birth') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Telefone Celular</label>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control cell_phone{{ $errors->has('cell_phone') ? ' is-invalid' : '' }}" autofocus name="cell_phone" value="{{ $item->cell_phone }}" required>
                                        @if ($errors->has('cell_phone'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('cell_phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Telefone Residencial</label>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control home_phone{{ $errors->has('home_phone') ? ' is-invalid' : '' }}" autofocus name="home_phone" value="{{ $item->home_phone }}">
                                        @if ($errors->has('home_phone'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('home_phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Gênero</label>
                                    <div class="col-md-5">
                                        <select class="form-control select2" name="gender">
                                            <option value="" {{$model->person['gender'] == null ? 'selected' : ''}}></option>
                                            <option value="Feminino" {{$model->person['gender'] == 'Feminino' ? 'selected' : ''}}>Feminino</option>
                                            <option value="Masculino" {{$model->person['gender'] == 'Masculino' ? 'selected' : ''}}>Masculino</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Nome Social</label>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control {{ $errors->has('social_name') ? ' is-invalid' : '' }}" autofocus name="social_name" value="{{ $item->person['social_name'] }}">
                                        @if ($errors->has('social_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('social_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">RG</label>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control {{ $errors->has('rg') ? ' is-invalid' : '' }}" autofocus name="rg" value="{{ $item->person['rg'] }}">
                                        @if ($errors->has('rg'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('rg') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Órgão Emissor</label>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control {{ $errors->has('issuing_body') ? ' is-invalid' : '' }}" autofocus name="issuing_body" value="{{ $item->person['issuing_body'] }}">
                                        @if ($errors->has('issuing_body'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('issuing_body') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Estado Emissor</label>
                                    <div class="col-md-5">
                                        <select class="form-control select2" name="issuing_uf">
                                            <option value="" {{$model->person['issuing_uf'] == null ? 'selected' : ''}}></option>
                                            <option value="RJ" {{$model->person['issuing_uf'] == 'RJ' ? 'selected' : ''}}>RJ</option>
                                            <option value="SP" {{$model->person['issuing_uf'] == 'SP' ? 'selected' : ''}}>SP</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Nome da Mãe</label>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control {{ $errors->has('name_mother') ? ' is-invalid' : '' }}" autofocus name="name_mother" value="{{ $item->person['name_mother'] }}">
                                        @if ($errors->has('name_mother'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name_mother') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Nome do Pai</label>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control {{ $errors->has('name_father') ? ' is-invalid' : '' }}" autofocus name="name_father" value="{{ $item->person['name_father'] }}">
                                        @if ($errors->has('name_father'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name_father') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Nacionalidade</label>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control {{ $errors->has('nationality') ? ' is-invalid' : '' }}" autofocus name="nationality" value="{{ $item->person['nationality'] }}">
                                        @if ($errors->has('nationality'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('nationality') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Estado Civil</label>
                                    <div class="col-md-5">
                                        <select class="form-control select2" name="marital_status">
                                            <option value="" {{$model->person['marital_status'] == null ? 'selected' : ''}}></option>
                                            <option value="Solteiro(a)" {{$model->person['marital_status'] == 'Solteiro(a)' ? 'selected' : ''}}>Solteiro(a)</option>
                                            <option value="Casado(a)" {{$model->person['marital_status'] == 'Casado(a)' ? 'selected' : ''}}>Casado(a)</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Portador de Deficiência</label>
                                    <div class="col-md-5">
                                        <select class="form-control select2" name="disabled_person">
                                            <option value="" {{$model['disabled_person'] == null ? 'selected' : ''}}></option>
                                            <option value="1" {{$model['disabled_person'] == 1 ? 'selected' : ''}}>Sim</option>
                                            <option value="0" {{$model['disabled_person'] == 0 ? 'selected' : ''}}>Não</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Instituição Afiliada(Não estudante)</label>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control {{ $errors->has('no_student_institution') ? ' is-invalid' : '' }}" autofocus name="no_student_institution" value="{{ $item->person['no_student_institution'] }}">
                                        @if ($errors->has('no_student_institution'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('no_student_institution') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Curso (Não estudante)</label>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control {{ $errors->has('no_student_course') ? ' is-invalid' : '' }}" autofocus name="no_student_course" value="{{ $item->person['no_student_course'] }}">
                                        @if ($errors->has('no_student_course'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('no_student_course') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Período (Não estudante)</label>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control {{ $errors->has('no_student_period') ? ' is-invalid' : '' }}" autofocus name="no_student_period" value="{{ $item->person['no_student_period'] }}">
                                        @if ($errors->has('no_student_period'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('no_student_period') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Cep</label>
                                    <div class="col-md-5">
                                        <input id="cep" type="text" class="form-control {{ $errors->has('no_student_period') ? ' is-invalid' : '' }}" autofocus name="cep" value="{{ $item->person['address'][0]['cep'] ?? null }}">
                                        @if ($errors->has('no_student_period'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('no_student_period') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Rua</label>
                                    <div class="col-md-5">
                                        <input id="street" type="text" class="form-control {{ $errors->has('no_student_period') ? ' is-invalid' : '' }}" autofocus name="street" value="{{ $item->person['address'][0]['street']  ?? null}}">
                                        @if ($errors->has('no_student_period'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('no_student_period') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Número</label>
                                    <div class="col-md-5">
                                        <input id="number" type="text" class="form-control {{ $errors->has('no_student_period') ? ' is-invalid' : '' }}" autofocus name="number" value="{{ $item->person['address'][0]['number'] ?? null}}">
                                        @if ($errors->has('no_student_period'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('no_student_period') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Complemento</label>
                                    <div class="col-md-5">
                                        <input id="complement" type="text" class="form-control {{ $errors->has('no_student_period') ? ' is-invalid' : '' }}" autofocus name="complement" value="{{ $item->person['address'][0]['complement']  ?? null}}">
                                        @if ($errors->has('no_student_period'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('no_student_period') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Bairro</label>
                                    <div class="col-md-5">
                                        <input id="district" type="text" class="form-control {{ $errors->has('no_student_period') ? ' is-invalid' : '' }}" autofocus name="district" value="{{ $item->person['address'][0]['district']  ?? null}}">
                                        @if ($errors->has('no_student_period'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('no_student_period') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Município</label>
                                    <div class="col-md-5">
                                        <input id="city" type="text" class="form-control {{ $errors->has('no_student_period') ? ' is-invalid' : '' }}" autofocus name="city" value="{{ $item->person['address'][0]['city']  ?? null}}">
                                        @if ($errors->has('no_student_period'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('no_student_period') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Estado</label>
                                    <div class="col-md-5">
                                        <input id="state" type="text" class="form-control {{ $errors->has('no_student_period') ? ' is-invalid' : '' }}" autofocus name="state" value="{{ $item->person['address'][0]['state']  ?? null}}">
                                        @if ($errors->has('no_student_period'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('no_student_period') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer bg-light">
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="offset-sm-3 col-md-5">
                                            <button type="submit" class="btn btn-primary btn-rounded submit">Atualizar</button>
                                            <a href="{{ route("$route[0].$route[1].index") }}" class="btn btn-secondary clear-form btn-rounded btn-outline">Cancelar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
