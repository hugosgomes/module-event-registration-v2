<?php

namespace App\Repositories\Panel\Users;

use App\Events\Users\UserRegister;
use App\Events\Users\UserUpdate;
use App\Models\Configuration\Configurations;
use App\Models\Configuration\Roles;
use App\Repositories\EventsApiRepository;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Gate;


class RepositoryUsers
{
    //Variáveis Globais

    protected                     $request;
    protected                     $model;
    protected                     $routeArray;
    protected                     $eventsApi;

    //Funções Padrões

    public function __construct
    (
        Request                   $request,
        User                      $model,
        Configurations            $configurations,
        EventsApiRepository       $eventsApi
    )
    {
        $this->request          = $request;
        $this->model            = $model;
        $this->configurations   = $configurations;
        $this->routeArray       = $this->configurations->getRouteName($request);
        $this->crudName         = 'Usuários';
        $this->eventsApi        = $eventsApi;
    }


    //Main Functions

    public function index(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            if(isset(auth()->user()->roles[0]) && auth()->user()->roles[0]->name=='Administrator')
            {
                $model = $this->model->where('id', '!=', 0)->get();
            }
            else
            {
                $model = $this->model->where('id', '!=', 0)->where('name', '!=', 'Administrator')->where('name', '!=', 'Development1')->get();
            }

            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'crudName'   => $this->crudName,
                            'model'      => $model,
                            'trashed'    => $this->model->where('id', '!=', 0)->onlyTrashed()->get(),
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function show(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model = $this->model->find($request['id']);
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $model->id,
                            'route'      => $this->routeArray,
                            'crudName'   => $this->crudName,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function create(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model = $this->model;
            $events = $this->eventsApi->index($request);
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'       => $this->crudName,
                            'route'       => $this->routeArray,
                            'crudName'    => $this->crudName,
                            'roles'       => Roles::all(),
                            'model'       => $model,
                            'events'       => $events,
                        ],
                    'request'             => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function store(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            //-----------------------------------------------------
            $model =  $this->model->create([
                'name'          => $request['name'],
                'email'         => $request['email'],
                'password'      => Hash::make($request['password']),
                'profile_event' => $request['profile_event'],
            ]);
            event(new UserRegister($model, $request->role_id));
            //-----------------------------------------------------
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                            'redirect'   => $this->routeArray[0].'/'.$this->routeArray[1].'/index'
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function edit(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model = $this->model->find($request['id']);
            $events = $this->eventsApi->index($request);
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'       => $this->crudName,
                            'route'       => $this->routeArray,
                            'model'       => $model,
                            'item'        => $model,
                            'crudName'    => $this->crudName,
                            'roles'       => Roles::all(),
                            'events'      => $events,
                        ],
                    'request'             => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function update(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]) && $this->configurations->CrudSystemNotModified($request, $this->model)==false)
        {
            //-----------------------------------------------------
            $model = $this->model->find($request->id);
            if($request->password != $model->password)
            {
                $model->name            = $request->name;
                $model->email           = $request->email;
                $model->password        = Hash::make($request['password']);
                $model->profile_event   = $request->profile_event;
                $model->save();
            }
            else
            {
                $model->name            = $request->name;
                $model->email           = $request->email;
                $model->profile_event   = $request->profile_event;
                $model->save();
            }
            event(new UserUpdate($model, $request->role_id));
            //-----------------------------------------------------
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function delete(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]) && $this->configurations->CrudSystemNotModified($request, $this->model)==false)
        {
            $model = $this->model->destroy($request['id']);
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

    public function restore(Request $request)
    {
        if(Gate::allows($this->routeArray[0].'.'.$this->routeArray[1].'.'.$this->routeArray[2]))
        {
            $model = $this->model->withTrashed()->where('id', $request['id'])->restore();
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }
        else
        {
            return false;
        }
    }

}
